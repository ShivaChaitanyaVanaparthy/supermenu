package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.igrand.supermenu.Activities.APIError;
import com.igrand.supermenu.Activities.BottonNavigationUser;
import com.igrand.supermenu.Activities.ForgetPasswordRestuarant;
import com.igrand.supermenu.Activities.ForgetPasswordUser;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.Registration;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.ForgetPasswordResponse;
import com.igrand.supermenu.Response.LoginResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class CustomBottomSheetDialogFragmentRegister extends BottomSheetDialogFragment {

    Button submitbutton;
    String emailid;
    EditText email;
    ApiInterface apiInterface;
    ForgetPasswordResponse.StatusBean statusBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_register, container, false);

        submitbutton=v.findViewById(R.id.submitbutton);
        email=v.findViewById(R.id.email);

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                emailid=email.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<ForgetPasswordResponse> call = apiInterface.forgetpasswordresponse(emailid);
                call.enqueue(new Callback<ForgetPasswordResponse>() {
                    @Override
                    public void onResponse(Call<ForgetPasswordResponse> call, Response<ForgetPasswordResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            Toast.makeText(getContext(), "Password Sent Successfully...", Toast.LENGTH_SHORT).show();


                            Intent intent=new Intent(getActivity(), Login.class);
                            intent.putExtra("Type","user");
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        } else if(response.code()!=200){
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                        }

                    }

                    @Override
                    public void onFailure(Call<ForgetPasswordResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(getActivity(),
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });

            }
        });

        return v;
    }
}
