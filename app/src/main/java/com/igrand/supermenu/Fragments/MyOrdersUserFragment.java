package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.BottonNavigationUser;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.Orderdetails;
import com.igrand.supermenu.Activities.PrefManager;
import com.igrand.supermenu.Activities.RateIt;
import com.igrand.supermenu.Activities.ViewDetails;
import com.igrand.supermenu.Adapters.RecyclerUserOrdersAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.MyOrdersResponse;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersUserFragment extends Fragment {

    TextView couponcode,rateit;
    LinearLayout rating,viewdetails;
    ImageView back;
    PrefManager prefManager;
    String user_id;
    ApiInterface apiInterface;
    MyOrdersResponse.StatusBean statusBean;
    RecyclerUserOrdersAdapter recycler_userofferadapter;
    RecyclerView recycler_userorder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_userorders, container, false);

        couponcode=v.findViewById(R.id.couponcode);
        /*rateit=v.findViewById(R.id.rateit);
        rating=v.findViewById(R.id.rating);
        back=v.findViewById(R.id.back);*/
        viewdetails=v.findViewById(R.id.viewdetails);
        recycler_userorder=v.findViewById(R.id.recycler_userorder);

        prefManager=new PrefManager(getContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");



        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MyOrdersResponse> call = apiInterface.myorderresponse(user_id);
        call.enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();

                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                        List<MyOrdersResponse.DataBean> dataBean = response.body().getData();

                        recycler_userorder.setLayoutManager(new LinearLayoutManager(getContext()));
                        recycler_userofferadapter=new RecyclerUserOrdersAdapter(getActivity(),dataBean);
                        recycler_userorder.setAdapter(recycler_userofferadapter);

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "No data found...", Toast.LENGTH_SHORT).show();

                    }

                }


            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        return v;
    }

    }
