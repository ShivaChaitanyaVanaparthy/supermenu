package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.igrand.supermenu.Activities.LocationChange;
import com.igrand.supermenu.Adapters.PlacesAutoCompleteAdapter;
import com.igrand.supermenu.Adapters.RecyclerUserOfferAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.UserHomeResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersUserFragment1 extends Fragment implements PlacesAutoCompleteAdapter.ClickListener  {

    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    UserHomeResponse.StatusBean statusBean;
    Double latitude, longitude;
    ApiInterface apiInterface;
    RecyclerUserOfferAdapter recycler_userofferadapter;
    RecyclerView recycler_useroffers;
    ImageView location;
    TextView txtAddress;
    Context context;
    LinearLayout linear,linearadd;
    String city_name,address,Lat,Longg;



    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_useroffers, container, false);


        recycler_useroffers = v.findViewById(R.id.recycler_useroffers);
        location = v.findViewById(R.id.location);
        txtAddress = v.findViewById(R.id.txtAddress);
        linear = v.findViewById(R.id.linear);
        linearadd = v.findViewById(R.id.linearadd);




        if(getArguments()!=null){

            address = getArguments().getString("address");
            city_name = getArguments().getString("cityname");
            txtAddress.setText(address);

            getCityName(city_name);
        }


        linearadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Lat=String.valueOf(latitude);
                Longg=String.valueOf(longitude);*/
                Intent intent=new Intent(getContext(), LocationChange.class);
                Bundle b = new Bundle();
               /* b.putDouble("lat", latitude);
                b.putDouble("long", longitude);*/
                intent.putExtras(b);
                startActivity(intent);
            }
        });




        Places.initialize(getContext(), getResources().getString(R.string.google_maps_key));


        recyclerView = (RecyclerView)v. findViewById(R.id.places_recycler_view);
        ((TextView) v.findViewById(R.id.txtAddress)).addTextChangedListener(filterTextWatcher);
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getContext(),latitude,longitude);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();



        return v;
    }




    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    @Override
    public void click(Place place) {
        //Toast.makeText(getContext(), place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude, Toast.LENGTH_SHORT).show();


        txtAddress.setText(place.getAddress());
        recyclerView.setVisibility(View.GONE);


        latitude=place.getLatLng().latitude;
        longitude=place.getLatLng().longitude;


        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            getCityName(city_name);

            //txtAddress.setText(city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private void getCityName(String city_name) {






            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UserHomeResponse> call = apiInterface.userhomeresponse(city_name);
            call.enqueue(new Callback<UserHomeResponse>() {
                @Override
                public void onResponse(Call<UserHomeResponse> call, Response<UserHomeResponse> response) {


                    UserHomeResponse statusResponse = response.body();

                    if (statusResponse != null) {


                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        if (statusBean.getCode() == 200) {

                            progressDialog.dismiss();
                            recycler_useroffers.setVisibility(View.VISIBLE);

                            List<UserHomeResponse.DataBean> dataBean = response.body().getData();

                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
                            /*recycler_useroffers.setLayoutManager(mLayoutManager);
                            recycler_userofferadapter = new RecyclerUserOfferAdapter(getActivity(), dataBean);
                            recycler_useroffers.setAdapter(recycler_userofferadapter);*/

                        } else if (statusBean.getCode() == 409) {
                            progressDialog.dismiss();
                            recycler_useroffers.setVisibility(View.GONE);
                            Toast.makeText(getContext(), statusBean.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        progressDialog.dismiss();
                        recycler_useroffers.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<UserHomeResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(getContext(),
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();

                }
            });

    }



}
