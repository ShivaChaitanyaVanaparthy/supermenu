package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.APIError;
import com.igrand.supermenu.Activities.BottonNavigationUser;
import com.igrand.supermenu.Activities.BusinessDoc;
import com.igrand.supermenu.Activities.ChangePassword;
import com.igrand.supermenu.Activities.ChangePasswordRestuarent;
import com.igrand.supermenu.Activities.EditProfile;
import com.igrand.supermenu.Activities.Geolocations;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.LoginRestuarent;
import com.igrand.supermenu.Activities.MainActivity;
import com.igrand.supermenu.Activities.Notifications;
import com.igrand.supermenu.Activities.OfferEdit;
import com.igrand.supermenu.Activities.PrefManager;
import com.igrand.supermenu.Activities.PrefManagerRestuarent;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.RestuarentProfileResponse;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    ImageView editprofile,image,rock;
    TextView changepassword,count,restuarent_name,status,location,email,phone,address,geolocation,businessdoc;
    LinearLayout logout;
    PrefManagerRestuarent prefManager;
    ApiInterface apiInterface;
    RestuarentProfileResponse.StatusBean statusBean;
    String user_id,location1,email1,phone1,address1,RestuarentName,RestuarentAddress;
    LinearLayout linear;
    RadioGroup rg;
    RadioButton prepaid,postpaid;
    LinearLayout notifications;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_profile, container, false);

        editprofile=v.findViewById(R.id.editprofile);
        changepassword=v.findViewById(R.id.changepassword);
        notifications=v.findViewById(R.id.notifications);
        logout=v.findViewById(R.id.logout);
        restuarent_name=v.findViewById(R.id.restuarent_name);
        status=v.findViewById(R.id.status);
        location=v.findViewById(R.id.location);
        email=v.findViewById(R.id.email);
        phone=v.findViewById(R.id.phone);
        address=v.findViewById(R.id.address);
        geolocation=v.findViewById(R.id.geolocation);
        image=v.findViewById(R.id.image);
        rock=v.findViewById(R.id.rock);
        linear=v.findViewById(R.id.linear);
        businessdoc=v.findViewById(R.id.businessdoc);
        prepaid=v.findViewById(R.id.prepaid);
        postpaid=v.findViewById(R.id.postpaid);
        count=v.findViewById(R.id.count);







        prefManager=new PrefManagerRestuarent(getActivity());
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManager.clearSession();
                Intent intent = new Intent(getActivity(), Login.class);
                intent.putExtra("Home",false);
                intent.putExtra("Type","restaurant");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });






        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RestuarentProfileResponse> call = apiInterface.restuarentprofileresponse(user_id);
        call.enqueue(new Callback<RestuarentProfileResponse>() {
            @Override
            public void onResponse(Call<RestuarentProfileResponse> call, Response<RestuarentProfileResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    final List<RestuarentProfileResponse.DataBean> dataBean = response.body().getData();


                    RestuarentName=dataBean.get(0).getRestaurant_name();
                    RestuarentAddress=dataBean.get(0).getGoogleloc();
                    location.setText(dataBean.get(0).getGoogleloc());
                    email.setText(dataBean.get(0).getEmail());
                    phone.setText(dataBean.get(0).getPhone());
                    address.setText(dataBean.get(0).getGoogleloc());
                    restuarent_name.setText(dataBean.get(0).getRestaurant_name());
                    count.setText(String.valueOf(dataBean.get(0).get_$Count_of_offersNotifications328()));
                    //geolocation.setText(dataBean.get(0).getGoogleloc());
                    //businessdoc.setText(dataBean.get(0).getBusinessdoc());
                    Picasso.get().load(dataBean.get(0).getRestaurant_image()).placeholder(R.drawable.loading).error(R.drawable.logoo).into(image);
                    Picasso.get().load(dataBean.get(0).getRestaurant_image()).placeholder(R.drawable.loading).error(R.drawable.logoo).into(rock);

                    geolocation.setText(dataBean.get(0).getGoogleloc());

                  /*  geolocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent=new Intent(getActivity(), Geolocations.class);
                            intent.putExtra("Iframe",dataBean.get(0).getGoogleloc());
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    });*/

                    businessdoc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent=new Intent(getActivity(), BusinessDoc.class);
                            intent.putExtra("BusinessDoc",dataBean.get(0).getBusinessdoc());
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        }
                    });

                    String status1=dataBean.get(0).getStatus();

                    if(status1.equals("1")) {

                        status.setText("OPEN");
                        status.setBackgroundResource(R.drawable.textgreenbackground);
                        prepaid.setChecked(true);

                    } else if(status1.equals("0")) {

                        status.setText("CLOSED");
                        status.setBackgroundResource(R.drawable.textbackgroundpurple);
                        prepaid.setChecked(false);
                    }


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<RestuarentProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), Notifications.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), ChangePasswordRestuarent.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(getActivity(), EditProfile.class);
                intent.putExtra("Resturanet_name",RestuarentName);
                intent.putExtra("Resturanet_Address",RestuarentAddress);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        return v;
    }
}
