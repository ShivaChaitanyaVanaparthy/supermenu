package com.igrand.supermenu.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.igrand.supermenu.Activities.APIError;
import com.igrand.supermenu.Activities.BottonNavigationUser;
import com.igrand.supermenu.Activities.LocationChange;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.PrefManager;
import com.igrand.supermenu.Activities.RateIt;
import com.igrand.supermenu.Adapters.PlacesAutoCompleteAdapter;
import com.igrand.supermenu.Adapters.RecyclerUserOfferAdapter;
import com.igrand.supermenu.Adapters.RecyclerUserOrdersAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.MyOrdersResponse;
import com.igrand.supermenu.Response.RatingResponse;
import com.igrand.supermenu.Response.ReviewCancel;
import com.igrand.supermenu.Response.ReviewsResponse;
import com.igrand.supermenu.Response.UserHomeResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class OffersUserFragment extends Fragment implements LocationListener, PlacesAutoCompleteAdapter.ClickListener  {

    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    UserHomeResponse.StatusBean statusBean;
    Double latitude, longitude;
    ApiInterface apiInterface;
    RecyclerUserOfferAdapter recycler_userofferadapter;
    RecyclerView recycler_useroffers;
    ImageView location,nooffer;
    TextView txtAddress;
    Context context;
    LinearLayout linear,linearadd;
    String city_name,address;
    int mDistanceInMeters=5500;
    RecyclerUserOrdersAdapter recycler_userofferadapter1;




    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    String Lat,Longg,user_id;
    PrefManager prefManager;
    MyOrdersResponse.StatusBean statusBean1;
    TextView nooffertext;





    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_useroffers, container, false);


        recycler_useroffers = v.findViewById(R.id.recycler_useroffers);
        location = v.findViewById(R.id.location);
        txtAddress = v.findViewById(R.id.txtAddress);
        linear = v.findViewById(R.id.linear);
        linearadd = v.findViewById(R.id.linearadd);
        nooffer = v.findViewById(R.id.nooffer);
        nooffertext = v.findViewById(R.id.nooffertext);

        prefManager=new PrefManager(getContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");


        if(this.getArguments()!=null){

            address = this.getArguments().getString("address");
            city_name = this.getArguments().getString("cityname");
            txtAddress.setText(address);
            getCityName(city_name);

        } else {
            locationManager = (LocationManager) getActivity().getSystemService(Service.LOCATION_SERVICE);
            isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissionsToRequest = findUnAskedPermissions(permissions);

            if (!isGPS && !isNetwork) {
                Log.d(TAG, "Connection off");
                showSettingsAlert();
                getLastLocation();
            } else {
                Log.d(TAG, "Connection on");
                // check permissions
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (permissionsToRequest.size() > 0) {
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                                ALL_PERMISSIONS_RESULT);
                        Log.d(TAG, "Permission requests");
                        canGetLocation = false;
                    }
                }

                // get location
                getLocation();
            }
        }




            /*Places.initialize(getContext(), getResources().getString(R.string.google_maps_key));

            recyclerView = (RecyclerView)v. findViewById(R.id.places_recycler_view);
            ((TextView) v.findViewById(R.id.txtAddress)).addTextChangedListener(filterTextWatcher);


            double latRadian = Math.toRadians(loc.getLatitude());

            double degLatKm = 110.574235;
            double degLongKm = 110.572833 * Math.cos(latRadian);
            double deltaLat = mDistanceInMeters / 1000.0 / degLatKm;
            double deltaLong = mDistanceInMeters / 1000.0 / degLongKm;

            double minLat = loc.getLatitude() - deltaLat;
            double minLong = loc.getLongitude() - deltaLong;
            double maxLat = loc.getLatitude() + deltaLat;
            double maxLong = loc.getLongitude() + deltaLong;

            Log.d(TAG,"Min: "+Double.toString(minLat)+","+Double.toString(minLong));
            Log.d(TAG,"Max: "+Double.toString(maxLat)+","+Double.toString(maxLong));


            mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getContext(), latitude, longitude);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            mAutoCompleteAdapter.setClickListener(this);
            recyclerView.setAdapter(mAutoCompleteAdapter);
            mAutoCompleteAdapter.notifyDataSetChanged();*/




        linearadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                locationManager = (LocationManager) getActivity().getSystemService(Service.LOCATION_SERVICE);
                isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                permissionsToRequest = findUnAskedPermissions(permissions);

                if (!isGPS && !isNetwork) {
                    Log.d(TAG, "Connection off");
                    showSettingsAlert();
                    getLastLocation();
                } else {
                    Log.d(TAG, "Connection on");
                    // check permissions
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (permissionsToRequest.size() > 0) {
                            requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                                    ALL_PERMISSIONS_RESULT);
                            Log.d(TAG, "Permission requests");
                            canGetLocation = false;
                        }
                    }

                    // get location
                    getLocation1();
                }

                Places.initialize(getContext(), getResources().getString(R.string.google_maps_key));

                recyclerView = (RecyclerView)v. findViewById(R.id.places_recycler_view);
                ((TextView) v.findViewById(R.id.txtAddress)).addTextChangedListener(filterTextWatcher);


                double latRadian = Math.toRadians(loc.getLatitude());

                double degLatKm = 110.574235;
                double degLongKm = 110.572833 * Math.cos(latRadian);
                double deltaLat = mDistanceInMeters / 1000.0 / degLatKm;
                double deltaLong = mDistanceInMeters / 1000.0 / degLongKm;

                double minLat = loc.getLatitude() - deltaLat;
                double minLong = loc.getLongitude() - deltaLong;
                double maxLat = loc.getLatitude() + deltaLat;
                double maxLong = loc.getLongitude() + deltaLong;

                Log.d(TAG,"Min: "+Double.toString(minLat)+","+Double.toString(minLong));
                Log.d(TAG,"Max: "+Double.toString(maxLat)+","+Double.toString(maxLong));


                mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getContext(), latitude, longitude);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                //mAutoCompleteAdapter.setClickListener(this);
                recyclerView.setAdapter(mAutoCompleteAdapter);
                mAutoCompleteAdapter.notifyDataSetChanged();



                Lat=String.valueOf(latitude);
                Longg=String.valueOf(longitude);
                Intent intent=new Intent(getContext(), LocationChange.class);
                Bundle b = new Bundle();
                b.putDouble("lat", latitude);
                b.putDouble("long", longitude);
                intent.putExtras(b);
                startActivity(intent);
            }
        });



        return v;
    }


    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
          /*  if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }*/
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    @Override
    public void click(Place place) {
        //Toast.makeText(getContext(), place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude, Toast.LENGTH_SHORT).show();


        txtAddress.setText(place.getAddress());
        recyclerView.setVisibility(View.GONE);


        latitude=place.getLatLng().latitude;
        longitude=place.getLatLng().longitude;


        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            getCityName(city_name);

            //txtAddress.setText(city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }

                 if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                   /* loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);*/
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    private void getLocation1() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI1(loc);
                    }
                }

                if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI1(loc);
                    }
                } else {
                   /* loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);*/
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "No LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(final Location loc) {

        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());

        // txtAddress.setText(Double.toString(latitude));
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

          // txtAddress.setText(city_name + state + country + postalCode);
            txtAddress.setText(address);
            getCityName(city_name);

            //
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void updateUI1(final Location loc) {

        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());
    }

    private void getCityName(final String city_name) {


    final ProgressDialog progressDialog = new ProgressDialog(getContext());
    progressDialog.setMessage("Loading.....");
    progressDialog.show();
    apiInterface = ApiClient.getClient().create(ApiInterface.class);
    Call<UserHomeResponse> call = apiInterface.userhomeresponse(city_name);
    call.enqueue(new Callback<UserHomeResponse>() {
        @Override
        public void onResponse(Call<UserHomeResponse> call, Response<UserHomeResponse> response) {


            if (response.code() == 200) {

                progressDialog.dismiss();
                statusBean = response.body() != null ? response.body().getStatus() : null;
                recycler_useroffers.setVisibility(View.VISIBLE);

                final List<UserHomeResponse.DataBean> dataBean = response.body().getData();

                if(dataBean.size()<=0){
                    //nooffer.setVisibility(View.VISIBLE);
                    nooffertext.setVisibility(View.VISIBLE);
                }

                RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
                recycler_useroffers.setLayoutManager(mLayoutManager);
                recycler_userofferadapter = new RecyclerUserOfferAdapter(getActivity(), dataBean);
                recycler_useroffers.setAdapter(recycler_userofferadapter);



            } else if (response.code() != 200) {
                progressDialog.dismiss();
                recycler_useroffers.setVisibility(View.GONE);
                Converter<ResponseBody, APIError> converter =
                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                APIError error;
                try {
                    error = converter.convert(response.errorBody());
                    APIError.StatusBean status=error.getStatus();
                    //Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();
                    if(status.getMessage().equals("No Data Found")){
                       // nooffer.setVisibility(View.VISIBLE);
                        nooffertext.setVisibility(View.VISIBLE);
                    }
                } catch (IOException e) { e.printStackTrace(); }
            }
/*
                    } else {
                        progressDialog.dismiss();
                        recycler_useroffers.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();
                    }*/
        }

        @Override
        public void onFailure(Call<UserHomeResponse> call, Throwable t) {
            progressDialog.dismiss();
            recycler_useroffers.setVisibility(View.GONE);
            //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            Toast toast = Toast.makeText(getContext(),
                    t.getMessage(), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
            toast.show();

        }
    });

}

    @Override
    public void onResume() {

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MyOrdersResponse> call = apiInterface.myorderresponse(user_id);
        call.enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {

                if (response.code() == 200) {

                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    final List<MyOrdersResponse.DataBean> dataBean = response.body().getData();


                    for (int i=0;i<dataBean.size();i++){

                       if(dataBean.get(i).getRating_status()!=null){

                           final String OrderId=dataBean.get(i).getOrder_id();

                           final Dialog dialog;
                           dialog = new Dialog(getContext());
                           dialog.setContentView(R.layout.dialogboxrating);
                           dialog.show();
                           dialog.setCancelable(false);
                           dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                           Window window = dialog.getWindow();
                           window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

                           final TextView restuarent,reviews;
                           Button submit,cancel;
                           final RatingBar ratingBar = dialog.findViewById(R.id.rating);
                           restuarent=dialog.findViewById(R.id.restuarent);
                           submit=dialog.findViewById(R.id.submit);
                           cancel=dialog.findViewById(R.id.cancel);
                           reviews=dialog.findViewById(R.id.reviews);
                           restuarent.setText(dataBean.get(i).getRestaurant_name());


                           cancel.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {



                                   apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                   Call<ReviewCancel> call = apiInterface.reviewCancelresponse(OrderId,"2");
                                   call.enqueue(new Callback<ReviewCancel>() {
                                       @Override
                                       public void onResponse(Call<ReviewCancel> call, Response<ReviewCancel> response) {
                                           if (response.code() == 200) {
                                               ReviewCancel.StatusBean statusBean = response.body() != null ? response.body().getStatus() : null;

                                               dialog.dismiss();

                                           } else if (response.code() != 200) {

                                               Converter<ResponseBody, APIError> converter =
                                                       ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                               APIError error;
                                               try {
                                                   error = converter.convert(response.errorBody());
                                                   APIError.StatusBean status=error.getStatus();
                                                   Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();

                                               } catch (IOException e) { e.printStackTrace(); }
                                           }
/*
                    } else {
                        progressDialog.dismiss();
                        recycler_useroffers.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "No Data Found...", Toast.LENGTH_SHORT).show();
                    }*/
                                       }

                                       @Override
                                       public void onFailure(Call<ReviewCancel> call, Throwable t) {
                                           //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                           Toast toast = Toast.makeText(getContext(),
                                                   t.getMessage(), Toast.LENGTH_SHORT);
                                           toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                           toast.show();

                                       }
                                   });



                               }
                           });

                           submit.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   Float rating = ratingBar.getRating();
                                   String Rating=String.valueOf(rating);
                                   String Comments=reviews.getText().toString();


                                   if(Rating.equals("")||Rating==null){
                                       Toast.makeText(getContext(), "Please rate the restaurant..", Toast.LENGTH_SHORT).show();
                                   }
                                   else {
                                       final ProgressDialog progressDialog = new ProgressDialog(getContext());
                                       progressDialog.setMessage("Loading.....");
                                       progressDialog.show();
                                       apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                       Call<ReviewsResponse> call = apiInterface.reviewresponse(OrderId,Rating,Comments);
                                       call.enqueue(new Callback<ReviewsResponse>() {
                                           @Override
                                           public void onResponse(Call<ReviewsResponse> call, Response<ReviewsResponse> response) {
                                               if (response.code() == 200) {
                                                   progressDialog.dismiss();
                                                  /* Intent intent=new Intent(getContext(), BottonNavigationUser.class);
                                                   startActivity(intent);*/

                                                   //getFragmentManager().beginTransaction().replace(R.id.frame_layout, new MyOrdersUserFragment(),"homee").commit();
                                                   dialog.dismiss();
                                                   Toast.makeText(getContext(), "Reviews/Ratings added sucessfully", Toast.LENGTH_SHORT).show();

                                               } else if (response.code() != 200) {
                                                   progressDialog.dismiss();

                                                   Converter<ResponseBody, APIError> converter =
                                                           ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                                   APIError error;
                                                   try {
                                                       error = converter.convert(response.errorBody());
                                                       APIError.StatusBean status=error.getStatus();
                                                       Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();
                                                   } catch (IOException e) { e.printStackTrace(); }
                                               }

                                           }

                                           @Override
                                           public void onFailure(Call<ReviewsResponse> call, Throwable t) {
                                               progressDialog.dismiss();
                                               //Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                                               Toast toast = Toast.makeText(getContext(),
                                                       t.getMessage(), Toast.LENGTH_SHORT);
                                               toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                               toast.show();

                                           }
                                       });


                                   }

                               }
                           });


                       }
                    }

                } else if (response.code() != 200) {

                   // Toast.makeText(getContext(), "No data found...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {

                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();
            }
        });
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }

    }

}
