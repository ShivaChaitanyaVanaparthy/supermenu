package com.igrand.supermenu.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.ChangePassword;
import com.igrand.supermenu.Activities.EditProfileUser;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.MainActivity;
import com.igrand.supermenu.Activities.PrefManager;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.UserProfileResponse;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileUserFragment extends Fragment {

    LinearLayout yourorders,changepassword;
    ImageView editprofile,image,image1;
    TextView email,phone,firstname,lastname,count;
    ApiInterface apiInterface;
    UserProfileResponse.StatusBean statusBean;
    PrefManager prefManager;
    String user_id,user_image,first_name,last_name,email11,phone11;
    int order_count;
    FrameLayout userprofile;
    LinearLayout logout;
    String password;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    final Fragment fragment1 = new OffersUserFragment();
    Fragment active = fragment1;
    String Phone,Fname,Lname;
    LinearLayout linear;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_userprofile, container, false);

        yourorders=v.findViewById(R.id.yourorders);
        editprofile=v.findViewById(R.id.editprofile);
        changepassword=v.findViewById(R.id.changepassword);
        image=v.findViewById(R.id.image);
        image1=v.findViewById(R.id.image1);
        email=v.findViewById(R.id.email);
        phone=v.findViewById(R.id.phone);
        firstname=v.findViewById(R.id.firstname);
        lastname=v.findViewById(R.id.lastname);
        userprofile=v.findViewById(R.id.userprofile);
        count=v.findViewById(R.id.count);
        logout=v.findViewById(R.id.logout);
        linear=v.findViewById(R.id.linear);


        prefManager=new PrefManager(getActivity());
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");
        password=profile.get("password");



        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                prefManager.clearSession();
                Intent intent = new Intent(getActivity(), Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<UserProfileResponse> call = apiInterface.userprofileresponse(user_id);
        call.enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    userprofile.setVisibility(View.VISIBLE);
                    linear.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<UserProfileResponse.DataBean> dataBean = response.body().getData();

                     user_image=dataBean.get(0).getUser_image();
                     first_name=dataBean.get(0).getFirst_name();
                     last_name=dataBean.get(0).getLast_name();
                     email11=dataBean.get(0).getEmail();
                     phone11=dataBean.get(0).getPhone();
                     order_count=dataBean.get(0).getOrders_count();


                    Picasso.get().load(user_image).placeholder(R.drawable.loading).error(R.drawable.logoo).into(image);
                    Picasso.get().load(user_image).placeholder(R.drawable.loading).error(R.drawable.logoo).into(image1);
                    email.setText(email11);
                    Phone=dataBean.get(0).getPhone();
                    Fname=dataBean.get(0).getFirst_name();
                    Lname=dataBean.get(0).getLast_name();
                    phone.setText(phone11);
                    firstname.setText(first_name);
                    lastname.setText(last_name);
                    count.setText(String.valueOf(order_count));



                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "No data found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



























        yourorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



              /*  fragmentManager = getChildFragmentManager());
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, new MyOrdersUserFragment(), "homee").hide(active);
                fragmentTransaction.commit();
                active = fragment1;

               */


                getFragmentManager().beginTransaction().replace(R.id.frame_layout, new MyOrdersUserFragment(),"homee").commit();


/*
               MyOrdersUserFragment fragment = new MyOrdersUserFragment();
                FragmentManager fragmentManager = getChildFragmentManager();
              fragmentManager.beginTransaction().replace(R.id.frame_layout,fragment,"homee").commit();
              */



                /*MyOrdersUserFragment nextFrag= new MyOrdersUserFragment();
                fragmentManager = getChildFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_layout, nextFrag, "homee")
                        .commit();*/
            }
        });

        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), ChangePassword.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), EditProfileUser.class);
                intent.putExtra(Phone,"phone");
                intent.putExtra(Fname,"fname");
                intent.putExtra(Lname,"lname");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        return v;
    }
}
