package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.APIError;
import com.igrand.supermenu.Activities.Payments;
import com.igrand.supermenu.Activities.PrefManagerRestuarent;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.SalesResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class SalesFragment extends Fragment {

    RecyclerView recycler_sales;
    LinearLayout payment;
    ImageView paymentss;
    ApiInterface apiInterface;
    SalesResponse.StatusBean statusBean;
    PrefManagerRestuarent prefManagerRestuarent;
    String user_id;
    TextView hourcount,daycount;
    RelativeLayout relative;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_sales, container, false);

        payment=v.findViewById(R.id.payment);
        paymentss=v.findViewById(R.id.paymentss);
        hourcount=v.findViewById(R.id.hourcount);
        daycount=v.findViewById(R.id.todaycount);
        relative=v.findViewById(R.id.relative);

        int color = Color.parseColor("#FFFFFF");
        paymentss.setColorFilter(color);

        prefManagerRestuarent=new PrefManagerRestuarent(getActivity());
        HashMap<String, String> profile=prefManagerRestuarent.getUserDetails();
        user_id=profile.get("Userid");


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<SalesResponse> call = apiInterface.salesresponse(user_id);
        call.enqueue(new Callback<SalesResponse>() {
            @Override
            public void onResponse(Call<SalesResponse> call, Response<SalesResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    List<SalesResponse.DataBean> dataBean = response.body().getData();


                    daycount.setText(String.valueOf(dataBean.get(0).getDay_count()));
                    hourcount.setText(String.valueOf(dataBean.get(0).getHour_count()));



                }
                else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(getContext(),""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }


            @Override
            public void onFailure(Call<SalesResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), Payments.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
        return v;


    }
}