package com.igrand.supermenu.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.AddOffers;
import com.igrand.supermenu.Activities.BottonNavigationUser;
import com.igrand.supermenu.Activities.ChangePassword;
import com.igrand.supermenu.Activities.Login;
import com.igrand.supermenu.Activities.PrefManager;
import com.igrand.supermenu.Activities.PrefManagerRestuarent;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Adapters.RecyclerOfferAdapter;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.OffersRestuarentResponse;
import com.igrand.supermenu.Response.UserHomeResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersFragment extends Fragment {

    RecyclerView recycler_offers;
    RecyclerOfferAdapter recyclerOrderAdapter;
    ImageView addoffer;
    ApiInterface apiInterface;
    OffersRestuarentResponse.StatusBean statusBean;
    String restaurent_id;
    PrefManagerRestuarent prefManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_offers, container, false);

        recycler_offers=v.findViewById(R.id.recycler_offers);
        addoffer=v.findViewById(R.id.addoffer);

        prefManager=new PrefManagerRestuarent(getContext());
        HashMap<String, String> profile=prefManager.getUserDetails();
        restaurent_id=profile.get("Userid");


        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OffersRestuarentResponse> call = apiInterface.offerrestuarentresponse(restaurent_id);
        call.enqueue(new Callback<OffersRestuarentResponse>() {
            @Override
            public void onResponse(Call<OffersRestuarentResponse> call, Response<OffersRestuarentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OffersRestuarentResponse.DataBean> dataBean = response.body().getData();

                    recycler_offers.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerOrderAdapter = new RecyclerOfferAdapter(getContext(),dataBean);
                    recycler_offers.setAdapter(recyclerOrderAdapter);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "No Offers found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<OffersRestuarentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(getContext(),
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        addoffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), AddOffers.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        return v;
    }

}
