package com.igrand.supermenu.Activities;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.model.Place;
import com.igrand.supermenu.Adapters.PlacesAutoCompleteAdapter;
import com.igrand.supermenu.Fragments.OffersUserFragment;
import com.igrand.supermenu.Fragments.OffersUserFragment1;
import com.igrand.supermenu.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationChange extends FragmentActivity implements PlacesAutoCompleteAdapter.ClickListener{

    EditText glocation;
    ImageView back;
    Double latitude,longitude;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    String city_name;
    String lat,longg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_change);


        glocation=findViewById(R.id.glocation);
        back=findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



            Bundle b = getIntent().getExtras();
            latitude = b.getDouble("lat");
            longitude = b.getDouble("long");






        recyclerView = (RecyclerView) findViewById(R.id.places_recycler_view);
        ((EditText)findViewById(R.id.glocation)).addTextChangedListener(filterTextWatcher);

        recyclerView.setLayoutManager(new LinearLayoutManager(LocationChange.this));
        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(LocationChange.this, latitude, longitude);
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();

    }


    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    @Override
    public void click(Place place) {
        //Toast.makeText(getContext(), place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude, Toast.LENGTH_SHORT).show();


       // txtAddress.setText(place.getAddress());
        recyclerView.setVisibility(View.GONE);




        latitude=place.getLatLng().latitude;
        longitude=place.getLatLng().longitude;


        Geocoder geocoder = new Geocoder(LocationChange.this, Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

           // getCityName(city_name);

            //txtAddress.setText(city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Intent i = new Intent(getApplicationContext(), BottonNavigationUser.class);
        i.putExtra("address",place.getAddress());
        i.putExtra("cityname",city_name);
        i.putExtra("key","key");
        startActivity(i);

        /*Bundle bundle = new Bundle();
        bundle.putString("address", place.getAddress());
        bundle.putString("cityname", city_name);
// set Fragmentclass Arguments
        OffersUserFragment fragobj = new OffersUserFragment();
        fragobj.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contaner, fragobj);
        transaction.commit();*/

    }

}
