package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.supermenu.Adapters.RecyclerNotificationsAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.NotificationResponse;

import java.util.HashMap;
import java.util.List;

public class Notifications extends BaseActivity {

    RecyclerView recycler_notifications;
    ImageView backpurple;
    RecyclerNotificationsAdapter recyclerNotificationsAdapter;
    ApiInterface apiInterface;
    NotificationResponse.StatusBean statusBean;
    PrefManagerRestuarent prefManager;
    String restaurant_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        recycler_notifications=findViewById(R.id.recycler_notifications);
        backpurple=findViewById(R.id.backpurple);

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        prefManager=new PrefManagerRestuarent(Notifications.this);
        HashMap<String, String> profile=prefManager.getUserDetails();
        restaurant_id=profile.get("Userid");


        final ProgressDialog progressDialog = new ProgressDialog(Notifications.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.notificationsresponse(restaurant_id);
        call.enqueue(new Callback<NotificationResponse>() {

            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<NotificationResponse.DataBean> dataBean = response.body().getData();

                    recycler_notifications.setLayoutManager(new LinearLayoutManager(Notifications.this));
                    recyclerNotificationsAdapter = new RecyclerNotificationsAdapter(getApplicationContext(),dataBean);
                    recycler_notifications.setAdapter(recyclerNotificationsAdapter);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(Notifications.this, "No data found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(Notifications.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
