package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.igrand.supermenu.R;
import com.squareup.picasso.Picasso;

public class BusinessDoc extends BaseActivity {

    String businessdoc;
    ImageView doc,backpurple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_doc);

        doc=findViewById(R.id.doc);
        backpurple=findViewById(R.id.backpurple);

        if(getIntent()!=null) {

            businessdoc=getIntent().getStringExtra("BusinessDoc");

        }

        Picasso.get().load(businessdoc).placeholder(R.drawable.loading).error(R.drawable.logoo).into(doc);

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
