package com.igrand.supermenu.Activities;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Adapters.RecyclerUserOfferAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.OfferClaimResponse;
import com.igrand.supermenu.Response.OfferDetailResponse;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class OfferDetailsUser extends BaseActivity {

    ImageView backwhite,offer_image;
    CardView card;
    Button claim;
    PrefManager prefManager;
    String user_id,offer_id,restaurant_name1;
    ApiInterface apiInterface;
    OfferDetailResponse.StatusBean statusBean;
    TextView restaurant_name,offer_details,offer_name,food_type,item_type,offer_details1,offer_name1,discount_price1,actual_price1,item_count1,txt1,txt2,minus,plus,cuisinetype,actual_price00,rating;
    RelativeLayout relative;
    String Restuarent_name,Offer_details,Offer_details1,Offer_name,Offer_name1,Food_type,Image,actual_price,discount_price,restaurant_id,offer_id1,item_count,
            dining,takeaway,copoun_code1,cuisine_type,Rating;
    Integer size,Copoun_code,Price,Value;
    OfferClaimResponse.StatusBean statusBean1;
    int sum;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details_user);

        backwhite=findViewById(R.id.backwhite);
        card=findViewById(R.id.card);
        claim=findViewById(R.id.claim);


        offer_image=findViewById(R.id.offer_image);
        restaurant_name=findViewById(R.id.restuarentname);
        offer_details=findViewById(R.id.offerdetails);
        offer_details1=findViewById(R.id.offerdetails1);
        offer_name=findViewById(R.id.offername);
        offer_name1=findViewById(R.id.offername1);
        food_type=findViewById(R.id.foodtype);
        relative=findViewById(R.id.relative);
        discount_price1=findViewById(R.id.discount_price);
        actual_price1=findViewById(R.id.actual_price);
        item_count1=findViewById(R.id.item_count);
        txt1=findViewById(R.id.txt1);
        txt2=findViewById(R.id.txt2);
        minus=findViewById(R.id.minus);
        plus=findViewById(R.id.plus);
        cuisinetype=findViewById(R.id.cuisine_type);
        actual_price00=findViewById(R.id.actual_price1);
        rating=findViewById(R.id.rating);






        txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               dining="Dinein";
               txt1.setBackgroundResource(R.drawable.textviewlightpurple);
                txt2.setBackgroundResource(R.drawable.textviewlightash);
                Toast.makeText(OfferDetailsUser.this, "Dining is selected", Toast.LENGTH_SHORT).show();
            }
        });

        txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dining="Parcel";
                txt1.setBackgroundResource(R.drawable.textviewlightash);
                txt2.setBackgroundResource(R.drawable.textviewlightpurple);
                Toast.makeText(OfferDetailsUser.this, "Parcel is selected", Toast.LENGTH_SHORT).show();
            }
        });


        if(getIntent()!=null) {
            offer_id=getIntent().getStringExtra("Offer_Id");
            actual_price=getIntent().getStringExtra("Actual_price");
            discount_price=getIntent().getStringExtra("Discount_price");
            cuisine_type=getIntent().getStringExtra("CuisineType");
            Rating=getIntent().getStringExtra("Rating");
           //actual_price00.setText(discount_price);
            cuisinetype.setText(cuisine_type);
            discount_price1.setText(discount_price);
            actual_price1.setText(actual_price);
            actual_price1.setPaintFlags(actual_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            //Price=Integer.parseInt(actual_price00.getText().toString());
        }

        //Value=sum*Price;

        //actual_price00.setText(String.valueOf(Value));



        double number = Double.valueOf(Rating) ;
        double roundedNumber = RecyclerUserOfferAdapter.DecimalUtils.round(number, 1);
        rating.setText(String.valueOf(roundedNumber));




        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        restaurant_name1 = profile.get("Name");
        user_id = profile.get("Userid");




        final ProgressDialog progressDialog = new ProgressDialog(OfferDetailsUser.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OfferDetailResponse> call = apiInterface.offetdetailresponse(offer_id);
        call.enqueue(new Callback<OfferDetailResponse>() {
            @Override
            public void onResponse(Call<OfferDetailResponse> call, Response<OfferDetailResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    relative.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OfferDetailResponse.DataBean> dataBean = response.body().getData();

                    Picasso.get().load(dataBean.get(0).getOffer_image()).error(R.drawable.logoo).placeholder(R.drawable.loading).into(offer_image);

                    Restuarent_name=dataBean.get(0).getRestaurant_name();
                    Offer_details=dataBean.get(0).getOffer_details();
                    Offer_details1=dataBean.get(0).getOffer_details();
                    Offer_name=dataBean.get(0).getOffer_name();
                    Offer_name1=dataBean.get(0).getOffer_name();
                    Food_type=dataBean.get(0).getFood_type();
                    Image=dataBean.get(0).getOffer_image();
                    restaurant_id=dataBean.get(0).getRestaurant_id();
                    offer_id1=dataBean.get(0).getOffer_id();
                    size=dataBean.size();




                    restaurant_name.setText(dataBean.get(0).getRestaurant_name());
                    offer_details.setText(dataBean.get(0).getOffer_details());
                    offer_details1.setText(dataBean.get(0).getOffer_details());
                    offer_name.setText(dataBean.get(0).getOffer_name());
                    offer_name1.setText(dataBean.get(0).getOffer_name());
                    food_type.setText(dataBean.get(0).getFood_type());
                    discount_price1.setText(dataBean.get(0).getDiscount_price());
                    actual_price1.setText(dataBean.get(0).getActual_price());
                    cuisinetype.setText(dataBean.get(0).getCuisine_type());
                    actual_price00.setText(dataBean.get(0).getDiscount_price());



                    Value=Integer.parseInt(dataBean.get(0).getDiscount_price());



                    plus.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {

                            sum = Integer.parseInt(item_count1.getText().toString());
                            sum = sum + 1;
                            Log.e("Quantity value 2 ", "" + sum);
                            if (sum > 0) {
                                item_count1.setVisibility(View.VISIBLE);
                                minus.setVisibility(View.VISIBLE);
                                plus.setVisibility(View.VISIBLE);
                            }

                            item_count1.setText(String.valueOf(sum));

                            sum=Integer.parseInt(item_count1.getText().toString());
                            if(sum==5){

                                plus.setVisibility(View.INVISIBLE);

                            }

                            Price=Integer.parseInt(actual_price00.getText().toString());
                            actual_price00.setText(String.valueOf(Value * sum));
                            //listener.itemClick(view, getAdapterPosition(), sum);

               /*
                Value=sum*Price;

                actual_price00.setText(String.valueOf(Value));
*/

                        }
                    });

                    minus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            sum = Integer.parseInt(item_count1.getText().toString());

                            if (sum > 0) {
                                sum = sum - 1;
                                if (sum == 0) {
                                    item_count1.setVisibility(View.INVISIBLE);
                                    minus.setVisibility(View.INVISIBLE);
                                    plus.setVisibility(View.VISIBLE);

                                }
                                item_count1.setText(String.valueOf(sum));
                                actual_price00.setText(String.valueOf(Value * sum));
                                //listener.itemClick(view, getAdapterPosition(), sum);

                   /* Price=Integer.parseInt(actual_price00.getText().toString());
                    Value=sum-Price;

                    actual_price00.setText(String.valueOf(Value));*/

                            }
                        }
                    });







                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(OfferDetailsUser.this, "No data found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<OfferDetailResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
                Toast toast= Toast.makeText(OfferDetailsUser.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



        claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                item_count=item_count1.getText().toString();

                if(dining==null){
                    Toast.makeText(OfferDetailsUser.this, "Please Select Service Type", Toast.LENGTH_SHORT).show();
                } else {

                    final ProgressDialog progressDialog = new ProgressDialog(OfferDetailsUser.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();

                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<OfferClaimResponse> call = apiInterface.offerclaimresponse(offer_id,item_count,dining,user_id);
                    call.enqueue(new Callback<OfferClaimResponse>() {
                        @Override
                        public void onResponse(Call<OfferClaimResponse> call, Response<OfferClaimResponse> response) {


                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean1 = response.body() != null ? response.body().getStatus() : null;
                                Copoun_code=response.body().get_$CouponCode28();
                                final Dialog dialog = new Dialog(OfferDetailsUser.this);
                                dialog.setContentView(R.layout.couponapproved);
                                dialog.setCancelable(false);
                                dialog.show();
                                TextView code;
                                ImageView close;
                                code=dialog.findViewById(R.id.code);
                                close=dialog.findViewById(R.id.close);
                                code.setText(String.valueOf(Copoun_code));
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                       Intent intent=new Intent(OfferDetailsUser.this,BottonNavigationUser.class);
                                       startActivity(intent);
                                    }
                                });
                                //dialog.setCancelable(false);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                                Window window = dialog.getWindow();
                                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);


                            } else if (response.code() != 200) {


                                progressDialog.dismiss();
                                Converter<ResponseBody, APIError> converter =
                                        ApiClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                                APIError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    APIError.StatusBean status = error.getStatus();
                                    Toast.makeText(OfferDetailsUser.this, "" + status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                            }
                        }

                        @Override
                        public void onFailure(Call<OfferClaimResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(OfferDetailsUser.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });
                }





            }
        });

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(OfferDetailsUser.this,OfferDetailsUser1.class);
                intent.putExtra("Restuarent_name",Restuarent_name);
                intent.putExtra("Offer_details",Offer_details);
                intent.putExtra("Offer_name",Offer_name);
                intent.putExtra("Food_type",Food_type);
                intent.putExtra("Image",Image);
                intent.putExtra("Size",size);
                intent.putExtra("ActualPrice",actual_price);
                intent.putExtra("DiscountPrice",discount_price);
                intent.putExtra("Restuarent_id",restaurant_id);
                intent.putExtra("Cuisine_type",cuisine_type);
                intent.putExtra("Rating",Rating);
                startActivity(intent);
            }
        });

        backwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}
