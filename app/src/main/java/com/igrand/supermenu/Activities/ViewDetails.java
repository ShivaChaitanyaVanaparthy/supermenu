package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.CopounResponse;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.OrderdetailsResponse;

import java.util.HashMap;
import java.util.List;

public class ViewDetails extends BaseActivity {

    ImageView backpurple;
    String time,order_id,Count,Reviews,Ratings,email,restuarent_name,status,value,location1,month,time1,offer_name,delivery_type,status0,status11,Copoun,Delivery,user_id,Code;
    TextView date,comment,yourrating,yourcomment,order_id1,count,restuarent_name1,status1,location,discount_price,time11,time2,offername,deliverytype,name1;
    EditText couponcode;
    ApiInterface apiInterface;
    OrderdetailsResponse.StatusBean statusBean;
    LinearLayout linear,linear1;
    Button submit;
    PrefManagerRestuarent prefManager;
    RatingBar rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);


        backpurple=findViewById(R.id.backpurple);
        date=findViewById(R.id.date);
        order_id1=findViewById(R.id.order_id);
        restuarent_name1=findViewById(R.id.restuarent_name);
        status1=findViewById(R.id.status);
        location=findViewById(R.id.location);
        discount_price=findViewById(R.id.discount_price);
        time11=findViewById(R.id.time);
        time2=findViewById(R.id.time1);
        offername=findViewById(R.id.offername);
        deliverytype=findViewById(R.id.deliverytype);
        name1=findViewById(R.id.name1);
        couponcode=findViewById(R.id.couponcode);
        linear=findViewById(R.id.linear);
        submit=findViewById(R.id.submit);
        linear1=findViewById(R.id.linear1);
        count=findViewById(R.id.count);
        comment=findViewById(R.id.comment);
        rating=findViewById(R.id.rating);
        yourcomment=findViewById(R.id.yourcomment);
        yourrating=findViewById(R.id.yourrating);


        prefManager=new PrefManagerRestuarent(ViewDetails.this);
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");


        if(getIntent()!=null) {

            time=getIntent().getStringExtra("Time");
            order_id=getIntent().getStringExtra("Order_Id");
            restuarent_name=getIntent().getStringExtra("RestuarentName");
            status=getIntent().getStringExtra("Status");
            status0=getIntent().getStringExtra("Status0");
            status11=getIntent().getStringExtra("Status1");
            value=getIntent().getStringExtra("Value");
            location1=getIntent().getStringExtra("Location");
            month=getIntent().getStringExtra("Month");
            time1=getIntent().getStringExtra("Time1");
            offer_name=getIntent().getStringExtra("OfferName");
            delivery_type=getIntent().getStringExtra("Delivery_type");
            email=getIntent().getStringExtra("email");
            Count=getIntent().getStringExtra("count");
            Reviews = getIntent().getStringExtra("Reviews");
            Ratings = getIntent().getStringExtra("Ratings");

        }

        if(status!=null){

            if(status.equals("CLAIMED")) {

                date.setText(time);
                order_id1.setText(order_id);
                restuarent_name1.setText(email);
                status1.setText(status);
                location.setText(location1);
                count.setText(Count);
                discount_price.setText(value);
                time11.setText(time1);
                time2.setText(time1);
                offername.setText(offer_name);
                name1.setText(restuarent_name);
                //deliverytype.setText(delivery_type);
                status1.setBackgroundResource(R.drawable.textbackgroundpurple);
                deliverytype.setBackgroundResource(R.drawable.textviewlightpurple);
                linear1.setVisibility(View.VISIBLE);
                submit.setVisibility(View.VISIBLE);

            }

        }

        if(status0!=null) {


             if(status0.equals("SUCCESS")) {

                date.setText(time);
                order_id1.setText(order_id);
                restuarent_name1.setText(email);
                status1.setText(status0);
                location.setText(location1);
                discount_price.setText(value);
                time11.setText(time1);
                 count.setText(Count);
                time2.setText(time1);
                offername.setText(offer_name);
                name1.setText(restuarent_name);
                //deliverytype.setText(delivery_type);
                status1.setBackgroundResource(R.drawable.textgreenbackground);
                deliverytype.setBackgroundResource(R.drawable.textviewlightpurple);
                linear1.setVisibility(View.GONE);
                 submit.setVisibility(View.GONE);
                 comment.setVisibility(View.VISIBLE);
                 rating.setVisibility(View.VISIBLE);
                 yourrating.setVisibility(View.VISIBLE);
                 yourcomment.setVisibility(View.VISIBLE);
                 comment.setText(Reviews);
                 if(Ratings.equals("")){
                 }else {
                     rating.setRating(Float.parseFloat(Ratings));
                 }





            }
        }


        if(status11!=null) {

            if(status11.equals("EXPIRED")) {

                date.setText(time);
                order_id1.setText(order_id);
                restuarent_name1.setText(email);
                status1.setText(status11);
                location.setText(location1);
                discount_price.setText(value);
                count.setText(Count);
                time11.setText(time1);
                time2.setText(time1);
                offername.setText(offer_name);
                name1.setText(restuarent_name);
                //deliverytype.setText(delivery_type);
                status1.setBackgroundResource(R.drawable.textviewpink);
                deliverytype.setBackgroundResource(R.drawable.textviewlightpurple);
                linear1.setVisibility(View.GONE);
                submit.setVisibility(View.GONE);

            }
        }




        final ProgressDialog progressDialog = new ProgressDialog(ViewDetails.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderdetailsResponse> call = apiInterface.viewDetails(order_id);
        call.enqueue(new Callback<OrderdetailsResponse>() {
            @Override
            public void onResponse(Call<OrderdetailsResponse> call, Response<OrderdetailsResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OrderdetailsResponse.DataBean> dataBean = response.body().getData();


                    Copoun=dataBean.get(0).getCoupon_number();
                    Delivery=dataBean.get(0).getDelivery_type();

                   // couponcode.setText(Copoun);
                    deliverytype.setText(Delivery);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(ViewDetails.this, "InValid Credentials...", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<OrderdetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(ViewDetails.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Code=couponcode.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(ViewDetails.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<CopounResponse> call = apiInterface.copoun(order_id,Code);
                call.enqueue(new Callback<CopounResponse>() {
                    @Override
                    public void onResponse(Call<CopounResponse> call, Response<CopounResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            Intent intent=new Intent(ViewDetails.this,BottomNavigation.class);
                            startActivity(intent);

                            Toast.makeText(ViewDetails.this, "CopounCode Claimed...", Toast.LENGTH_SHORT).show();

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            Toast.makeText(ViewDetails.this, "InValid CopounCode...", Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<CopounResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(ViewDetails.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();

                    }
                });

            }
        });
    }
}
