package com.igrand.supermenu.Activities;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class PrefManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "SuperMenuUser";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String KEY_USERID = "userid";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_FIRSTNAME = "first_name";
    private static final String KEY_LASTNAME = "last_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PHONE = "phone";

    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void createLogin(String user_id, String email, String first_name, String last_name, String phone, String password1) {

        editor.putString(KEY_USERID,user_id);
        editor.putString(KEY_PASSWORD,password1);
        editor.putString(KEY_FIRSTNAME,first_name);
        editor.putString(KEY_LASTNAME,last_name);
        editor.putString(KEY_EMAIL,email);
        editor.putString(KEY_PHONE,phone);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }



    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();

        profile.put("Userid",pref.getString(KEY_USERID,null));
        profile.put("password",pref.getString(KEY_PASSWORD,null));

        profile.put("first_name",pref.getString(KEY_FIRSTNAME,null));
        profile.put("last_name",pref.getString(KEY_LASTNAME,null));
        profile.put("email",pref.getString(KEY_EMAIL,null));
        profile.put("phone",pref.getString(KEY_PHONE,null));

        return profile;
    }


}
