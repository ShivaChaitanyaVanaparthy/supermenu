package com.igrand.supermenu.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.RegisterResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Registration extends BaseActivity {

    ImageView backregister;
    Button registerbutton;
    RadioGroup rg;
    EditText firstname,lastname,email,mobilenumber,password,confirmpassword;
    String first_name,last_name,email1,mobile_number,password1,confirmpassword1;
    String type;
    RadioButton restuarent,user;
    String Type,user_id,user_name,phone;
    ApiInterface apiInterface;
    RegisterResponse.StatusBean statusBean;
    PrefManager prefManager;
    CheckBox checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        backregister = findViewById(R.id.backregister);
        registerbutton = findViewById(R.id.registerbutton);
        rg = findViewById(R.id.rg);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        mobilenumber = findViewById(R.id.mobile);
        password = findViewById(R.id.passkey);
        confirmpassword = findViewById(R.id.confirmpasskey);
        checkbox = findViewById(R.id.checkbox);

        if(getIntent()!=null) {
            Type = getIntent().getStringExtra("Type");
        }


        registerbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    first_name = firstname.getText().toString();
                    last_name = lastname.getText().toString();
                    email1 = email.getText().toString();
                    phone = mobilenumber.getText().toString();
                    password1 = password.getText().toString();
                    confirmpassword1 = confirmpassword.getText().toString();

                    if (first_name.equals("")) {
                        Toast.makeText(Registration.this, "Please enter FirstName...", Toast.LENGTH_SHORT).show();
                    } else if (last_name.equals("")) {

                        Toast.makeText(Registration.this, "Please enter LastName...", Toast.LENGTH_SHORT).show();
                    } else if (phone.equals("")) {
                        Toast.makeText(Registration.this, "Please enter MobileNumber...", Toast.LENGTH_SHORT).show();
                    } else if (email1.equals("")) {
                        Toast.makeText(Registration.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
                    } else if (password1.equals("")) {
                        Toast.makeText(Registration.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
                    } else if (confirmpassword1.equals("")) {
                        Toast.makeText(Registration.this, "Please Confirm Password...", Toast.LENGTH_SHORT).show();
                    } else if (checkbox.isChecked()==false) {
                        Toast.makeText(Registration.this, "Please Accept Terms & Conditions...", Toast.LENGTH_SHORT).show();
                    }
                    else if (
                            (!first_name.isEmpty() || !first_name.equals("")) &&
                                    (!last_name.isEmpty() || !last_name.equals("")) &&
                                    (!email1.isEmpty() || !email1.equals("")) &&
                                    (!phone.isEmpty() || !phone.equals("")) &&
                                    (!password1.isEmpty() || !password1.equals("")) &&
                                    (!confirmpassword1.isEmpty() || !confirmpassword1.equals("")) &&
                                (!checkbox.isChecked()==false )
                    ) {


                        if(password1.equals(confirmpassword1)) {
                            if(checkbox.isChecked()) {


                                final ProgressDialog progressDialog = new ProgressDialog(Registration.this);
                                progressDialog.setMessage("Loading.....");
                                progressDialog.setCancelable(false);
                                progressDialog.show();


                                if (password1.equals(confirmpassword1)) {
                                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                    Call<RegisterResponse> call = apiInterface.registerresponse(Type, first_name, last_name, phone, email1, password1);
                                    call.enqueue(new Callback<RegisterResponse>() {
                                        @Override
                                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {

                                            if (response.code() == 200) {
                                                progressDialog.dismiss();
                                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                                //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                                                List<RegisterResponse.DataBean> dataBean = response.body().getData();

                                                user_id = dataBean.get(0).getUser_id();
                                                user_name = dataBean.get(0).getUser_name();
                                                phone = dataBean.get(0).getPhone();


                                                Intent intent = new Intent(Registration.this, Login.class);
                                                intent.putExtra("UserId", user_id);
                                                intent.putExtra("UserName", user_name);
                                                intent.putExtra("Phone", phone);
                                                intent.putExtra("Type", Type);
                                                startActivity(intent);
                                                // prefManager.createLogin1(first_name,last_name,email1,phone);

                                                Toast.makeText(Registration.this, "Successfully Registered...", Toast.LENGTH_SHORT).show();


                                            }  else if(response.code()!=200)
                                            {
                                                progressDialog.dismiss();
                                                Converter<ResponseBody, APIError> converter =
                                                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                                APIError error;
                                                try {
                                                    error = converter.convert(response.errorBody());
                                                    APIError.StatusBean status=error.getStatus();
                                                    Toast.makeText(Registration.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                                } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                                            }


                                        }


                                        @Override
                                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                                            progressDialog.dismiss();
                                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                            finish();
                                            Toast toast = Toast.makeText(Registration.this,
                                                    t.getMessage(), Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                                            toast.show();


                                        }
                                    });

                                }

                            } else {

                                Toast.makeText(Registration.this, "Please Accept Terms & Conditions", Toast.LENGTH_SHORT).show();
                            }

                        } else{

                            Toast.makeText(Registration.this, "Please Confirm Password Correctly...", Toast.LENGTH_SHORT).show();
                        }


                    }

                }
            });


        backregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
