package com.igrand.supermenu.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.igrand.supermenu.Fragments.MyOrdersFragment;
import com.igrand.supermenu.Fragments.OffersFragment;
import com.igrand.supermenu.Fragments.ProfileFragment;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Fragments.SalesFragment;

public class BottomNavigation extends AppCompatActivity {

    final Fragment fragment1 = new OffersFragment();
    final Fragment fragment2 = new MyOrdersFragment();
    final Fragment fragment3 = new SalesFragment();
    final Fragment fragment4 = new ProfileFragment();
    BottomNavigationView bottomNavigationView;
    public FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment active = fragment1;
    Fragment active2 = fragment2;
    Fragment active3 = fragment3;
    Fragment active4 = fragment4;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);


        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new MyOrdersFragment(), "homee");
        fragmentTransaction.commit();


        bottomNavigationView = findViewById(R.id.the_bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new MyOrdersFragment(), "homee").hide(active);
                    fragmentTransaction.commit();
                    active = fragment1;
                    return true;


                case R.id.navigation_digital:
                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new OffersFragment(), "").hide(active2);
                    fragmentTransaction.commit();
                    active2 = fragment2;
                    return true;

                case R.id.navigation_sale:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new SalesFragment(), "").hide(active3);
                    fragmentTransaction.commit();
                    active3 = fragment3;
                    return true;


                case R.id.navigation_wallet:

                    fragmentManager = getSupportFragmentManager();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, new ProfileFragment(), "").hide(active4);
                    fragmentTransaction.commit();
                    active4 = fragment4;
                    return true;


            }

            return false;
        }

    };

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }

    }
}
