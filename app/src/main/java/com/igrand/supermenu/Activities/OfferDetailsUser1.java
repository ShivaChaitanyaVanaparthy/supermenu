package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Adapters.RecyclerUserOffer1Adapter;
import com.igrand.supermenu.Adapters.RecyclerUserOfferAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.RestuarentDetailsResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfferDetailsUser1 extends BaseActivity {

    RecyclerView recycler_useroffers1;
    RecyclerUserOffer1Adapter recycler_userofferadapter;
    ImageView backwhite,image1;
    String restuarent_name,Rating,offer_details,offer_name,food_type,image,actual_price,discount_price,restuarent_id,pic,name,place,cuisine_type;
    TextView restuarent_name1,website,rating,food_type1,offer_name1,location;
    Integer size;
    ApiInterface apiInterface;
    RestuarentDetailsResponse.StatusBean statusBean;
    List<RestuarentDetailsResponse.DataBean.OfferDetailsBean> offer_details2;
    LinearLayout linear1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details_user1);

        recycler_useroffers1=findViewById(R.id.recycler_useroffers1);
        backwhite=findViewById(R.id.backwhite);
        image1=findViewById(R.id.offer_image);
        restuarent_name1=findViewById(R.id.restuarent_name);
        food_type1=findViewById(R.id.food_type);
        location=findViewById(R.id.location);
        linear1=findViewById(R.id.linear1);
        rating=findViewById(R.id.rating);
        website=findViewById(R.id.website);


        if(getIntent()!=null) {

            restuarent_name=getIntent().getStringExtra("Restuarent_name");
            offer_details=getIntent().getStringExtra("Offer_details");
            offer_name=getIntent().getStringExtra("Offer_name");
            food_type=getIntent().getStringExtra("Food_type");
            image=getIntent().getStringExtra("Image");
            actual_price=getIntent().getStringExtra("ActualPrice");
            discount_price=getIntent().getStringExtra("DiscountPrice");
            restuarent_id=getIntent().getStringExtra("Restuarent_id");
            cuisine_type=getIntent().getStringExtra("Cuisine_type");
            Rating=getIntent().getStringExtra("Rating");
            size=getIntent().getIntExtra("Size",0);
            Picasso.get().load(image).error(R.drawable.logoo).placeholder(R.drawable.loading).into(image1);
            restuarent_name1.setText(restuarent_name);


        }


        double number = Double.valueOf(Rating) ;
        double roundedNumber = RecyclerUserOfferAdapter.DecimalUtils.round(number, 1);
        rating.setText(String.valueOf(roundedNumber));

        final ProgressDialog progressDialog = new ProgressDialog(OfferDetailsUser1.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<RestuarentDetailsResponse> call = apiInterface.restuarentdetailresponse(restuarent_id);
        call.enqueue(new Callback<RestuarentDetailsResponse>() {
            @Override
            public void onResponse(Call<RestuarentDetailsResponse> call, Response<RestuarentDetailsResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear1.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<RestuarentDetailsResponse.DataBean> dataBean = response.body().getData();

                    food_type1.setText(dataBean.get(0).get_$Restaurant_detailsCuisine_type187());
                    website.setText(dataBean.get(0).getWebsite());

                    for(int i=0;i<=dataBean.size();i++) {
                            offer_details2= dataBean.get(i).getOffer_details();
                        break;
                    }

                    place=dataBean.get(0).getGoogleloc();

                    location.setText(place);
                    recycler_useroffers1.setLayoutManager(new LinearLayoutManager(OfferDetailsUser1.this));
                    recycler_userofferadapter=new RecyclerUserOffer1Adapter(getApplicationContext(),offer_details,offer_name,size,actual_price,discount_price,dataBean,offer_details2,cuisine_type);
                    recycler_useroffers1.setAdapter(recycler_userofferadapter);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(OfferDetailsUser1.this, "No data found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<RestuarentDetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                finish();
                Toast toast= Toast.makeText(OfferDetailsUser1.this,t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();

            }
        });


        backwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
