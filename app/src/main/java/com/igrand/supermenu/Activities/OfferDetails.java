package com.igrand.supermenu.Activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Adapters.RecyclerOfferDetailsAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.OfferDetailRestuarantResponse;
import com.igrand.supermenu.Response.OrderDetailRestuarantResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfferDetails extends BaseActivity {

    RecyclerView recycler_offerdetails;
    ImageView backwhite;
    RecyclerOfferDetailsAdapter recyclerOfferdetaisAdapter;
    ApiInterface apiInterface;
    OfferDetailRestuarantResponse.StatusBean statusBean;
    OrderDetailRestuarantResponse.StatusBean statusBean1;
    String offer_id;
    ImageView image;
    TextView txt,offername,offerdetails,actualprice,offerprice;
    LinearLayout linearlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        backwhite=findViewById(R.id.backwhite);
        recycler_offerdetails=findViewById(R.id.recycler_offerdetails);

        image=findViewById(R.id.image);
        txt=findViewById(R.id.txt);
        offername=findViewById(R.id.offername);
        offerdetails=findViewById(R.id.offerdetails);
        actualprice=findViewById(R.id.actual_price);
        offerprice=findViewById(R.id.offer_price);
        linearlayout=findViewById(R.id.linearlayout);


        if(getIntent()!=null) {

            offer_id=getIntent().getStringExtra("OFFERID");
        }

        backwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        final ProgressDialog progressDialog = new ProgressDialog(OfferDetails.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OfferDetailRestuarantResponse> call = apiInterface.restuarantofferresponse(offer_id);
        call.enqueue(new Callback<OfferDetailRestuarantResponse>() {
            @Override
            public void onResponse(Call<OfferDetailRestuarantResponse> call, Response<OfferDetailRestuarantResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linearlayout.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OfferDetailRestuarantResponse.DataBean> dataBean = response.body().getData();


                    txt.setText(dataBean.get(0).getOffer_id());
                    offername.setText(dataBean.get(0).getOffer_name());
                    offerdetails.setText(dataBean.get(0).getOffer_details());
                    actualprice.setText(dataBean.get(0).getActual_price());
                    offerprice.setText(dataBean.get(0).getDiscount_price());

                    Picasso.get().load(dataBean.get(0).getOffer_image()).error(R.drawable.logoo).placeholder(R.drawable.loading).into(image);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(OfferDetails.this, "No Order's Found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<OfferDetailRestuarantResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(OfferDetails.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });





        final ProgressDialog progressDialog1 = new ProgressDialog(OfferDetails.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderDetailRestuarantResponse> call1 = apiInterface.restuarantorderresponse(offer_id);
        call1.enqueue(new Callback<OrderDetailRestuarantResponse>() {
            @Override
            public void onResponse(Call<OrderDetailRestuarantResponse> call, Response<OrderDetailRestuarantResponse> response) {

                if (response.code() == 200) {
                    progressDialog1.dismiss();
                    linearlayout.setVisibility(View.VISIBLE);
                    statusBean1 = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OrderDetailRestuarantResponse.DataBean> dataBean1 = response.body().getData();

                    recycler_offerdetails.setLayoutManager(new LinearLayoutManager(OfferDetails.this));
                    recyclerOfferdetaisAdapter = new RecyclerOfferDetailsAdapter(getApplicationContext(),dataBean1);
                    recycler_offerdetails.setAdapter(recyclerOfferdetaisAdapter);


                } else if (response.code() != 200) {
                    progressDialog1.dismiss();
                    Toast.makeText(OfferDetails.this, "No Order's Found...", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<OrderDetailRestuarantResponse> call, Throwable t) {
                progressDialog1.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(OfferDetails.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }
}
