package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.EditProfileRestuarentResponse;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.UpdateProfileRestuarentResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class EditProfile extends BaseActivity {

    ImageView backwhite, image1, rock;
    EditText firstname, lastname, mobile;
    Button updatebutton;
    String first_name, last_name, mobile_number, user_id,fname,lname,phone,pic,RestuarentName,RestuarentAddress;
    PrefManagerRestuarent prefManagerRestuarent;
    ApiInterface apiInterface;
    EditProfileRestuarentResponse.StatusBean statusBean;
    UpdateProfileRestuarentResponse.StatusBean statusBean1;
    TextView restuarent_name,location;
    LinearLayout linear;




    File image = null;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic,picturePath;
    String emptydocument="";
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        backwhite = findViewById(R.id.backwhite);
        updatebutton = findViewById(R.id.updatebutton);

        image1 = findViewById(R.id.image);
        rock = findViewById(R.id.rock);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        mobile = findViewById(R.id.mobile);
        restuarent_name = findViewById(R.id.restuarent_name);
        location = findViewById(R.id.location);
        linear = findViewById(R.id.linear);

        if(getIntent()!=null) {

            RestuarentName=getIntent().getStringExtra("Resturanet_name");
            RestuarentAddress=getIntent().getStringExtra("Resturanet_Address");
            restuarent_name.setText(RestuarentName);
            location.setText(RestuarentAddress);

        }


        prefManagerRestuarent = new PrefManagerRestuarent(EditProfile.this);
        HashMap<String, String> profile = prefManagerRestuarent.getUserDetails();
        user_id = profile.get("Userid");


        final ProgressDialog progressDialog = new ProgressDialog(EditProfile.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditProfileRestuarentResponse> call = apiInterface.editprofilerestuarentresponse(user_id);
        call.enqueue(new Callback<EditProfileRestuarentResponse>() {
            @Override
            public void onResponse(Call<EditProfileRestuarentResponse> call, Response<EditProfileRestuarentResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    EditProfileRestuarentResponse editProfileRestuarentResponse=response.body();
                    List<EditProfileRestuarentResponse.DataBean> dataBeans=editProfileRestuarentResponse.getData();
                    statusBean = response.body() != null ? response.body().getStatus() : null;


                    for (int i=0;i<=dataBeans.size();i++) {

                        fname=dataBeans.get(i).getFname();
                        lname=dataBeans.get(i).getLname();
                        phone=dataBeans.get(i).getPhone();
                        pic=dataBeans.get(i).getLogo();
                        break;
                    }

                    firstname.setText(fname);
                    lastname.setText(lname);
                    mobile.setText(phone);
                    Picasso.get().load(pic).placeholder(R.drawable.loading).error(R.drawable.logoo).into(rock);
                    Picasso.get().load(pic).placeholder(R.drawable.loading).error(R.drawable.logoo).into(image1);


                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();


                   // Toast.makeText(EditProfile.this, "User Data Updated Successfully", Toast.LENGTH_SHORT).show();


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(EditProfile.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<EditProfileRestuarentResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(EditProfile.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });





        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(EditProfile.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfile.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(EditProfile.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(EditProfile.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(EditProfile.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfile.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(EditProfile.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(EditProfile.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);


            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(rock);

            bitmap = ((BitmapDrawable) rock.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }










        updatebutton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {


                                                first_name = firstname.getText().toString();
                                                last_name = lastname.getText().toString();
                                                mobile_number = mobile.getText().toString();
                                                updateprofile(user_id,first_name,last_name,mobile_number);

                                            }
                                        });


        backwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageUri=selectedImage;
                converetdImage = getResizedBitmap(bitmap, 500);
                rock.setImageBitmap(converetdImage);
                rock.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    private void updateprofile(String user_id, String first_name, String last_name, String mobile_number) {


        final ProgressDialog progressDialog = new ProgressDialog(EditProfile.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        MultipartBody.Part body = null;
        if (imageUri!=null)
        {
            body= prepareFilePart("image",imageUri);
        }

       /* MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
        }*/
            RequestBody UserId = RequestBody.create(MediaType.parse("multipart/form-data"), user_id);
            RequestBody FirstName = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
            RequestBody LastName = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
            RequestBody Phone = RequestBody.create(MediaType.parse("multipart/form-data"), mobile_number);
            // RequestBody Email = RequestBody.create(MediaType.parse("multipart/form-data"), email1);


            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UpdateProfileRestuarentResponse> call = apiInterface.updateprofilerestuarentresponse(UserId,FirstName,LastName,Phone,body);
            call.enqueue(new Callback<UpdateProfileRestuarentResponse>() {
                @Override
                public void onResponse(Call<UpdateProfileRestuarentResponse> call, Response<UpdateProfileRestuarentResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean1 = response.body() != null ? response.body().getStatus() : null;
                        //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();


                        Intent intent=new Intent(EditProfile.this,BottomNavigation.class);
                        startActivity(intent);
                        Toast.makeText(EditProfile.this, "Restaurant Data Updated Successfully", Toast.LENGTH_SHORT).show();


                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Toast.makeText(EditProfile.this, "Updated Restaurant Data Failed ! Please Try Again", Toast.LENGTH_SHORT).show();

                    }

                }


                @Override
                public void onFailure(Call<UpdateProfileRestuarentResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(EditProfile.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });




    }

    private MultipartBody.Part prepareFilePart(String image, Uri imageUri) {
        String path = RealPathUtil.getRealPath(EditProfile.this,imageUri); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(image, file.getName(), requestFile);
    }

}
