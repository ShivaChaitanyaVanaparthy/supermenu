package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.EditProfileRestuarentResponse;
import com.igrand.supermenu.Response.EditProfileUserResponse;
import com.igrand.supermenu.Response.EditProfileUserResponse1;
import com.igrand.supermenu.Response.LoginResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class EditProfileUser extends BaseActivity {

    ImageView backwhite,profilepic;
    Button updatebutton;
    PrefManager prefManager;
    String user_id,password,first_name,last_name,email,phone,first_name1,last_name1,email1,phone1,fname,lname,phone11,pic,email0;
    TextView firstname,lastname,mobile,emailid,email11;
    ApiInterface apiInterface;
    EditProfileUserResponse1.StatusBean statusBean;
    EditText firstname1, lastname1, mobile1;
    LinearLayout  linear;

    File image = null;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Bitmap bitmap;
    Bitmap converetdImage;
    String imagePic,picturePath;
    Uri imageUri;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_user);


        backwhite=findViewById(R.id.backpurple);
        updatebutton=findViewById(R.id.updatebutton);
        profilepic=findViewById(R.id.rock);
        firstname=findViewById(R.id.firstname);
        lastname=findViewById(R.id.lastname);
        mobile=findViewById(R.id.mobile);
        emailid=findViewById(R.id.email);
        firstname1=findViewById(R.id.firstname);
        lastname1=findViewById(R.id.lastname);
        mobile1=findViewById(R.id.mobile);
        email11=findViewById(R.id.email);
        linear=findViewById(R.id.linear);

        prefManager=new PrefManager(EditProfileUser.this);
        HashMap<String, String> profile=prefManager.getUserDetails();
        password=profile.get("password");


        user_id=profile.get("Userid");
        email=profile.get("email");





        final ProgressDialog progressDialog = new ProgressDialog(EditProfileUser.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditProfileUserResponse1> call = apiInterface.editprofileuserrrsponse1(user_id);
        call.enqueue(new Callback<EditProfileUserResponse1>() {
            @Override
            public void onResponse(Call<EditProfileUserResponse1> call, Response<EditProfileUserResponse1> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    // statusBean = response.body() != null ? response.body().getStatus() : null;
                    EditProfileUserResponse1 editProfileRestuarentResponse=response.body();
                    List<EditProfileUserResponse1.DataBean> dataBeans=editProfileRestuarentResponse.getData();
                    statusBean = response.body() != null ? response.body().getStatus() : null;


                    for (int i=0;i<=dataBeans.size();i++) {

                        fname=dataBeans.get(i).getFirst_name();
                        lname=dataBeans.get(i).getLast_name();
                        phone11=dataBeans.get(i).getPhone();
                        pic=dataBeans.get(i).getProfile();
                        email0=dataBeans.get(i).getEmail();
                        break;
                    }

                    firstname.setText(fname);
                    lastname.setText(lname);
                    mobile1.setText(phone11);
                    email11.setText(email0);
                    Picasso.get().load(pic).placeholder(R.drawable.loading).error(R.drawable.logoo).into(profilepic);


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfileUser.this, "Updated User Data Failed ! Please Try Again", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<EditProfileUserResponse1> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(EditProfileUser.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(EditProfileUser.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileUser.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(EditProfileUser.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(EditProfileUser.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(EditProfileUser.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileUser.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileUser.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(EditProfileUser.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);


            }
        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(profilepic);

            bitmap = ((BitmapDrawable) profilepic.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }


        backwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        updatebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                first_name1 = firstname.getText().toString();
                last_name1 = lastname.getText().toString();
                phone1 = mobile.getText().toString();
                email1 = emailid.getText().toString();


                updateprofile(user_id, first_name1, last_name1, phone1, email1);

            }
        });
    }


    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageUri=selectedImage;
                converetdImage = getResizedBitmap(bitmap, 500);
                profilepic.setImageBitmap(converetdImage);
                profilepic.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    private void updateprofile(String user_id, String first_name1, String last_name1, String phone1, String email1) {


        final ProgressDialog progressDialog = new ProgressDialog(EditProfileUser.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


       /* MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);

        }*/

        MultipartBody.Part body = null;
        if (imageUri!=null)
        {
            body= prepareFilePart("image",imageUri);
        }

        RequestBody UserId = RequestBody.create(MediaType.parse("multipart/form-data"), user_id);
        RequestBody FirstName = RequestBody.create(MediaType.parse("multipart/form-data"), first_name1);
        RequestBody LastName = RequestBody.create(MediaType.parse("multipart/form-data"), last_name1);
        RequestBody Phone = RequestBody.create(MediaType.parse("multipart/form-data"), phone1);
        RequestBody Email = RequestBody.create(MediaType.parse("multipart/form-data"), email1);



        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditProfileUserResponse> call = apiInterface.editprofileuserrrsponse(UserId,FirstName,LastName,Phone,Email,body);
        call.enqueue(new Callback<EditProfileUserResponse>() {
            @Override
            public void onResponse(Call<EditProfileUserResponse> call, Response<EditProfileUserResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();


                    Intent intent=new Intent(EditProfileUser.this,BottonNavigationUser.class);
                    startActivity(intent);
                    Toast.makeText(EditProfileUser.this, "User Data Updated Successfully", Toast.LENGTH_SHORT).show();


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfileUser.this, "Updated User Data Failed ! Please Try Again", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<EditProfileUserResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(EditProfileUser.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

    }

    private MultipartBody.Part prepareFilePart(String image, Uri imageUri) {
        String path = RealPathUtil.getRealPath(EditProfileUser.this,imageUri); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(image, file.getName(), requestFile);
    }
}
