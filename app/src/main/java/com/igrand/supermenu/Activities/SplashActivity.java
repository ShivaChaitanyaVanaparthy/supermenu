package com.igrand.supermenu.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.supermenu.R;

import static android.os.Build.VERSION_CODES.M;

public class SplashActivity extends Activity {

    PrefManager prefManager;
    PrefManagerRestuarent prefManager1;
    private static final String TAG = "user";
    private static final int MY_PERMISSIONS_REQUEST = 1;
    Dialog dialog;
    RadioGroup rg;
    RadioButton restuarent, user;
    Button registerbutton;
    private static int splashscreentimeout=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_slide1);

        prefManager = new PrefManager(SplashActivity.this);
        prefManager1 = new PrefManagerRestuarent(SplashActivity.this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {




                    if (checkPermissions()) {


                        if (prefManager.isLoggedIn()) {
                            //prefManager1.clearSession();
                            Log.e(TAG, "user" + prefManager.isLoggedIn());
                            Intent intent = new Intent(SplashActivity.this, BottonNavigationUser.class);
                            startActivity(intent);

                        }
                       else if (prefManager1.isLoggedIn()) {
                            //  prefManager.clearSession();
                            Log.e(TAG, "restaurant" + prefManager1.isLoggedIn());
                            Intent intent = new Intent(SplashActivity.this, BottomNavigation.class);
                            startActivity(intent);
                        } else {

                            Intent intent=new Intent(SplashActivity.this,Login.class);
                            startActivity(intent);
                           // launchHomeScreen();
                            // setContentView(R.layout.welcome_slide1);
                        }

                    } else {

                        Toast.makeText(getApplicationContext(), "Please take the permissions...", Toast.LENGTH_SHORT).show();


                    }


            }},splashscreentimeout);






       /* prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }*/

    }

    private void launchHomeScreen() {
        dialog = new Dialog(SplashActivity.this);
        dialog.setContentView(R.layout.dialogboxlogin);
        dialog.show();
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        rg = dialog.findViewById(R.id.rg);
        restuarent = dialog.findViewById(R.id.restuarent);
        user = dialog.findViewById(R.id.user);
        registerbutton = dialog.findViewById(R.id.registerbutton);
        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (restuarent.isChecked()) {

                    dialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), LoginRestuarent.class);
                    intent.putExtra("Type", "restaurant");
                    startActivity(intent);

                } else if (user.isChecked()) {

                    dialog.dismiss();
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.putExtra("Type", "user");
                    startActivity(intent);
                } else {

                    Toast.makeText(SplashActivity.this, "Please select User/Restaurant...", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private boolean checkPermissions() {

        final Context context = SplashActivity.this;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                //ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            //ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
            // ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
        )
        {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                int i = 0;
                if (grantResults != null && grantResults.length > 0) {
                    for (i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            break;
                        }
                    }
                    if (i == grantResults.length) {
                        Intent intent = new Intent(getApplication(), Login.class);
                        //  intent.putExtra("array", itemList);
                        startActivity(intent);
                    } else {
                        checkPermissions();
                    }
                }
                break;
        }
    }
}
