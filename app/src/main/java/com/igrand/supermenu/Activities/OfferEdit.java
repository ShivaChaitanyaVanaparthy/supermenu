package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.AddOfferResponse;
import com.igrand.supermenu.Response.EditOfferResponse;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.UpdateOfferResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

public class OfferEdit extends BaseActivity {

    ImageView backpurple,imageupload;
    Button loginbutton;
    ApiInterface apiInterface;
    EditOfferResponse.StatusBean statusBean;
    String offer_id,imagePic,picturePath,offer_name1,expirytime1,actual_price1,offer_price1,description1,image1,type,type1,Image1;
    Bitmap converetdImage;
    EditText offername,expirytime,actual_price,discount_price,description;
    RadioGroup rg,rg1;
    RadioButton prepaid,postpaid,prepaid1,postpaid1;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    File image = null;
    private Bitmap bitmap;
    TextView uploadimage;
    String emptyDocument="";
    Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_edit);

        backpurple=findViewById(R.id.backpurple);
        loginbutton=findViewById(R.id.loginbutton);

        offername=findViewById(R.id.offername);
        expirytime=findViewById(R.id.expirytime);
        actual_price=findViewById(R.id.actual_price);
        discount_price=findViewById(R.id.discount_price);
        description=findViewById(R.id.description);
        uploadimage=findViewById(R.id.uploadimage);
        imageupload=findViewById(R.id.imageupload);

        rg=findViewById(R.id.rg);
        rg1=findViewById(R.id.rg1);
        prepaid=findViewById(R.id.prepaid);
        postpaid=findViewById(R.id.postpaid);
        prepaid1=findViewById(R.id.prepaid1);
        postpaid1=findViewById(R.id.postpaid1);

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null) {

            offer_id=getIntent().getStringExtra("OFFERID");
        }


        final ProgressDialog progressDialog = new ProgressDialog(OfferEdit.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<EditOfferResponse> call = apiInterface.editofferresponse(offer_id);
        call.enqueue(new Callback<EditOfferResponse>() {
            @Override
            public void onResponse(Call<EditOfferResponse> call, Response<EditOfferResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<EditOfferResponse.DataBean> dataBean = response.body().getData();


                    offername.setText(dataBean.get(0).getMenuitem());
                    expirytime.setText(dataBean.get(0).getValid());
                    actual_price.setText(dataBean.get(0).getActualoffercost());
                    discount_price.setText(dataBean.get(0).getOffercost());
                    description.setText(dataBean.get(0).getDescription());
                    Image1=dataBean.get(0).getImage();
                    //Picasso.get().load(Image1).into(imageupload);
                   // uploadimage.setText(dataBean.get(0).getImage());


                    if(dataBean.get(0).getFtype().equals("Veg")) {

                        prepaid.setChecked(true);

                    } else if(dataBean.get(0).getFtype().equals("NonVeg")) {


                        postpaid.setChecked(true);
                    }


                    if(dataBean.get(0).getMtype().equals("Food")) {

                        prepaid1.setChecked(true);

                    } else if(dataBean.get(0).getMtype().equals("Beverages")) {


                        postpaid1.setChecked(true);
                    }




                }
                else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(OfferEdit.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<EditOfferResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(OfferEdit.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });



        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid){
                    type="Veg";
                }
                else {
                    type="NonVeg";
                }
            }
        });


        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid1) {
                    type1 = "Beverages";
                }
                else {
                    type1="Food";
                }
            }
        });


        uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(OfferEdit.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(OfferEdit.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(OfferEdit.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(OfferEdit.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(OfferEdit.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(OfferEdit.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(OfferEdit.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(OfferEdit.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }



                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }



        });






        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offer_name1=offername.getText().toString();
                expirytime1=expirytime.getText().toString();
                actual_price1=actual_price.getText().toString();
                offer_price1=discount_price.getText().toString();
                description1=description.getText().toString();
                image1=uploadimage.getText().toString();

                editoffer(offer_id,offer_name1,expirytime1,actual_price1,offer_price1,description1,type,type1);
            }
        });

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }


    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageUri=selectedImage;
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void editoffer(String restaurent_id, String offer_name1, String expirytime1, String actual_price1, String offer_price1, String description1, String type, String type1) {




        MultipartBody.Part body = null;
        if (imageUri!=null)
        {
            body= prepareFilePart("image",imageUri);
        }


     /*   MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
        }*/
       if(imageUri==null){
            Toast.makeText(this, "Please Upload image to continue", Toast.LENGTH_SHORT).show();
        } else {

            final ProgressDialog progressDialog=new ProgressDialog(OfferEdit.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            RequestBody OfferName = RequestBody.create(MediaType.parse("multipart/form-data"), offer_name1);
            RequestBody ExpiryTime = RequestBody.create(MediaType.parse("multipart/form-data"), expirytime1);
            RequestBody ActualPrice = RequestBody.create(MediaType.parse("multipart/form-data"), actual_price1);
            RequestBody OfferPrice = RequestBody.create(MediaType.parse("multipart/form-data"), offer_price1);
            RequestBody Description = RequestBody.create(MediaType.parse("multipart/form-data"), description1);
            RequestBody Id = RequestBody.create(MediaType.parse("multipart/form-data"), restaurent_id);
            RequestBody Ftype = RequestBody.create(MediaType.parse("multipart/form-data"), type);
            RequestBody Mtype = RequestBody.create(MediaType.parse("multipart/form-data"), type1);
            RequestBody Status = RequestBody.create(MediaType.parse("multipart/form-data"), "1");
            // RequestBody Mtype = RequestBody.create(MediaType.parse("multipart/form-data"), type1);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<UpdateOfferResponse> call = apiInterface.offersupdate(Id,OfferName,Ftype,ExpiryTime,ActualPrice,OfferPrice,Description,Mtype,Status,body);
            call.enqueue(new Callback<UpdateOfferResponse>() {
                @Override
                public void onResponse(Call<UpdateOfferResponse> call, Response<UpdateOfferResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        UpdateOfferResponse.StatusBean statusBean1 = response.body() != null ? response.body().getStatus() : null;
                        // List<UpdateOfferResponse.DataBean> dataBean = response.body().getData();


                        Intent intent=new Intent(OfferEdit.this,BottomNavigation.class);
                        startActivity(intent);
                    /*OffersFragment nextFrag= new OffersFragment();
                    AddOffers.this.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.userprofile, nextFrag, "homee")
                            .addToBackStack(null)
                            .commit();*/

                    } else if (response.code() != 200) {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status=error.getStatus();
                            Toast.makeText(OfferEdit.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }

                    }

                }


                @Override
                public void onFailure(Call<UpdateOfferResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast= Toast.makeText(OfferEdit.this,
                            t.getMessage() , Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });
        }



    }

    private MultipartBody.Part prepareFilePart(String image, Uri imageUri) {
        String path = RealPathUtil.getRealPath(OfferEdit.this,imageUri); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(image, file.getName(), requestFile);
    }

}
