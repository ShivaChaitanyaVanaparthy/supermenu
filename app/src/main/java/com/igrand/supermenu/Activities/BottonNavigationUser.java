package com.igrand.supermenu.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.igrand.supermenu.Fragments.MyOrdersUserFragment;
import com.igrand.supermenu.Fragments.OffersUserFragment;
import com.igrand.supermenu.Fragments.OffersUserFragment1;
import com.igrand.supermenu.Fragments.ProfileUserFragment;
import com.igrand.supermenu.R;

public class BottonNavigationUser extends AppCompatActivity {

    Fragment fragment1 = new OffersUserFragment();
    Fragment fragment2 = new MyOrdersUserFragment();
    Fragment fragment3 = new ProfileUserFragment();
    BottomNavigationView bottomNavigationView;
    public FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment active = fragment1;
    Fragment active2 = fragment2;
    Fragment active3 = fragment3;
    private Boolean exit = false;
    String address,city_name,key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botton_navigation_user);



        if(getIntent()!=null){
            address = getIntent().getStringExtra("address");
            city_name = getIntent().getStringExtra("cityname");
            key = getIntent().getStringExtra("key");
        }



        /*Bundle bundle = new Bundle();
        bundle.putString("address", address);
        bundle.putString("cityname", city_name);
        fragment1.setArguments(bundle);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, new OffersUserFragment(), "homee");
        fragmentTransaction.commit();*/

        if(key==null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, new OffersUserFragment(), "homee");
            fragmentTransaction.commit();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("address", address);
            bundle.putString("cityname", city_name);
// set Fragmentclass Arguments
            OffersUserFragment fragobj = new OffersUserFragment();
            fragobj.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragobj);
            transaction.commit();
        }



        bottomNavigationView = findViewById(R.id.the_bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.navigation_home:

                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, new MyOrdersUserFragment(), "homee").hide(active);
                        fragmentTransaction.commit();
                        active = fragment1;
                        return true;


                    case R.id.navigation_digital:

                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, new OffersUserFragment(), "").hide(active2);
                        fragmentTransaction.commit();
                        active2 = fragment2;
                        return true;


                    case R.id.navigation_wallet:

                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, new ProfileUserFragment(), "").hide(active3);
                        fragmentTransaction.commit();
                        active3 = fragment3;

                        return true;

                }

                return false;
            }

        };

    @Override
    public void onBackPressed() {
        if (exit) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        } else {
            this.exit = true;
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.the_bottom_navigation);
            bottomNavigationView.setSelectedItemId(R.id.navigation_digital);
            Toast.makeText(BottonNavigationUser.this, "Press Back again to Exit...", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

}

