package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.igrand.supermenu.Adapters.PlacesAutoCompleteAdapter;
import com.igrand.supermenu.Adapters.RecyclerAdapterCusineTypeList;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.CusineTypeResonse;
import com.igrand.supermenu.Response.RegistrationRestResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RegistrationRestaurantt extends BaseActivity implements LocationListener, PlacesAutoCompleteAdapter.ClickListener {

    Button registerbutton;
    ImageView backpurple,imageupload;
    String first_name,last_name,email,phone,password,confirm_password,restuarant_name,business_doc,city,area,
            address,Gloc,picturePath,imagePic;
    EditText City,Area,Address,glocation;
    TextView business,cuisinetype;
    ApiInterface apiInterface;
    Bitmap converetdImage;
    RegistrationRestResponse.StatusBean statusBean;
    File image = null;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Bitmap bitmap;
    RecyclerAdapterCusineTypeList recyclerUser;
    CusineTypeResonse.StatusBean statusBean1;


    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    Double latitude, longitude;
    RadioGroup radio;
    RadioButton radio1,radio2;
    String Cuisinetype;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    RecyclerView recyclerView;
    String city_name;
    String emptydocument="";
    Uri imageUri;


    private final int select_photo = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_restaurantt);

        business = findViewById(R.id.business);
        City = findViewById(R.id.City);
        Area = findViewById(R.id.Area);
        Address = findViewById(R.id.Address);
        glocation = findViewById(R.id.glocation);
        registerbutton = findViewById(R.id.registerbutton);
        backpurple = findViewById(R.id.backpurple);
        imageupload = findViewById(R.id.imageupload);
        radio = findViewById(R.id.radio);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        cuisinetype = findViewById(R.id.cuisinetype);

        if (getIntent() != null) {

            first_name = getIntent().getStringExtra("FNAME");
            last_name = getIntent().getStringExtra("LNAME");
            email = getIntent().getStringExtra("EMAIL");
            phone = getIntent().getStringExtra("PHONE");
            password = getIntent().getStringExtra("PASSWORD");
            confirm_password = getIntent().getStringExtra("CONFIRMPASSWORD");
            restuarant_name = getIntent().getStringExtra("Restaurant");

        }

        cuisinetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(RegistrationRestaurantt.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_cuisine);
                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;
                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);
                dialog.show();

                final ProgressDialog progressDialog = new ProgressDialog(RegistrationRestaurantt.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<CusineTypeResonse> call = apiInterface.admincusineList();
                call.enqueue(new Callback<CusineTypeResonse>() {
                    @Override
                    public void onResponse(Call<CusineTypeResonse> call, Response<CusineTypeResonse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                            // Toast.makeText(Registration1.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<CusineTypeResonse.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(RegistrationRestaurantt.this));
                            recyclerUser = new RecyclerAdapterCusineTypeList(RegistrationRestaurantt.this, dataBeans, RegistrationRestaurantt.this, cuisinetype, dialog,add);
                            recyclerView.setAdapter(recyclerUser);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                            // Toast.makeText(Registration1.this, "No City's...", Toast.LENGTH_SHORT).show();

                        }

                    }
                    @Override
                    public void onFailure(Call<CusineTypeResonse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(RegistrationRestaurantt.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });
        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(RegistrationRestaurantt.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(RegistrationRestaurantt.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(RegistrationRestaurantt.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(RegistrationRestaurantt.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(RegistrationRestaurantt.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(RegistrationRestaurantt.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(RegistrationRestaurantt.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(RegistrationRestaurantt.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                /*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);*/

              /*  Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                startActivityForResult(in, select_photo);*/

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }


        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }

        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //restuarant_name=restuarentname.getText().toString();
                business_doc=business.getText().toString();
                //city=City.getText().toString();
                area=Area.getText().toString();
                address=Address.getText().toString();
                //Gloc=glocation.getText().toString();

                ArrayList<String> stringList1=new ArrayList<String>();

                Cuisinetype=cuisinetype.getText().toString();

                stringList1.add(Cuisinetype);

                getData(first_name,last_name,email,phone,password,restuarant_name,stringList1,area,address,confirm_password);
            }
        });

        locationManager = (LocationManager) this.getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }



        Places.initialize(RegistrationRestaurantt.this, getResources().getString(R.string.google_maps_key));

        recyclerView = (RecyclerView) findViewById(R.id.places_recycler_view);
        ((EditText) findViewById(R.id.glocation)).addTextChangedListener(filterTextWatcher);

        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(RegistrationRestaurantt.this, latitude, longitude);
        recyclerView.setLayoutManager(new LinearLayoutManager(RegistrationRestaurantt.this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();


    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }
                if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "No LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @Override
    public void click(Place place) {
        glocation.setText(place.getAddress());
        Gloc=glocation.getText().toString();
        recyclerView.setVisibility(View.GONE);
        latitude=place.getLatLng().latitude;
        longitude=place.getLatLng().longitude;


        Geocoder geocoder = new Geocoder(RegistrationRestaurantt.this, Locale.getDefault());

        try {
            List<Address> addresses= geocoder.getFromLocation(latitude, longitude, 1);
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            glocation.setText(place.getAddress());
            //txtAddress.setText(city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(city_name==null||city_name.equals("")){
            city_name="";
        }

        recyclerView.setVisibility(View.GONE);
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case select_photo:
                if (resultCode == RESULT_OK) {
                    try {

                        Uri imageuri = data.getData();// Get intent
                        // data

                        /*uriPath.setText("URI Path: " + imageuri.toString());
                        // Get real path and show over text view

                        realPath.setText("Real Path: " + real_Path);

                        uriPath.setVisibility(View.VISIBLE);
                        realPath.setVisibility(View.VISIBLE);*/
                        picturePath = getRealPathFromUri(RegistrationRestaurantt.this,
                                imageuri);
                        image = new File(picturePath);
                        Bitmap bitmap = decodeUri(RegistrationRestaurantt.this, imageuri, 300);// call
                        // deocde
                        // uri
                        // method
                        // Check if bitmap is not null then set image else show
                        // toast
                        if (bitmap != null)
                            imageupload.setImageBitmap(bitmap);// Set image over
                            // bitmap

                        else
                            Toast.makeText(RegistrationRestaurantt.this,
                                    "Error while decoding image.",
                                    Toast.LENGTH_SHORT).show();
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                        Toast.makeText(RegistrationRestaurantt.this, "File not found.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
        }
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageUri=selectedImage;
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static Bitmap decodeUri(Context context, Uri uri,
                                   final int requiredSize) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(uri), null, o2);
    }

    // Get Original image path
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void getData(String first_name, String last_name, String email, String phone, String password, String restuarant_name, List cusisine_type, String area, String address,String confirm_password) {


/*
        if (first_name.equals("")){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter FirstName...", Toast.LENGTH_SHORT).show();
        }
        else if (last_name.equals("")) {

            Toast.makeText(RegistrationRestaurantt.this, "Please enter LastName...", Toast.LENGTH_SHORT).show();
        }
        else if (phone.equals("")){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter MobileNumber...", Toast.LENGTH_SHORT).show();
        }

        else if (email.equals("")){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
        }
        else if (password.equals("")) {

            Toast.makeText(RegistrationRestaurantt.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
        }
        else if (restuarant_name.equals("")){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter RestuarantName...", Toast.LENGTH_SHORT).show();
        }*/
         if (cusisine_type.size()<=0){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter CusineType...", Toast.LENGTH_SHORT).show();
        }/*else if(image==null){
             Toast.makeText(this, "Please Upload image to continue", Toast.LENGTH_SHORT).show();
         }*/ else if (Gloc==null||Gloc.equals("")){
            Toast.makeText(RegistrationRestaurantt.this, "Please enter Glocation...", Toast.LENGTH_SHORT).show();
        }

        MultipartBody.Part body = null;
        if (imageUri!=null)
        {
            body= prepareFilePart("businessdoc",imageUri);
        }



            final ProgressDialog progressDialog=new ProgressDialog(RegistrationRestaurantt.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            progressDialog.setCancelable(false);


            List<MultipartBody.Part> descriptionList = new ArrayList<>();

            RequestBody firstname1 = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
            RequestBody lastname1 = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
            RequestBody email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody phone1 = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
            RequestBody password1 = RequestBody.create(MediaType.parse("multipart/form-data"), password);
            RequestBody restuarantname1 = RequestBody.create(MediaType.parse("multipart/form-data"), restuarant_name);
            descriptionList.add(MultipartBody.Part.createFormData("cuisine_type[]", Cuisinetype ));
            RequestBody city1 = RequestBody.create(MediaType.parse("multipart/form-data"), city_name);
            RequestBody address1 = RequestBody.create(MediaType.parse("multipart/form-data"), address);
            RequestBody gloc1 = RequestBody.create(MediaType.parse("multipart/form-data"), Gloc);
            RequestBody restuarant1 = RequestBody.create(MediaType.parse("multipart/form-data"), "restaurant");




            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<RegistrationRestResponse> call = apiInterface.registerrestresponse(restuarant1,firstname1,lastname1,phone1,email1,password1,restuarantname1, descriptionList,city1,address1,gloc1,body);
            call.enqueue(new Callback<RegistrationRestResponse>() {
                @Override
                public void onResponse(Call<RegistrationRestResponse> call, Response<RegistrationRestResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                        List<RegistrationRestResponse.DataBean> dataBean = response.body().getData();

                        Toast.makeText(RegistrationRestaurantt.this, "Registered Successfully...", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(RegistrationRestaurantt.this,Login.class);
                        intent.putExtra("Type","restaurant");
                        startActivity(intent);

                    }
                    else if(response.code()!=200)
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIErrorRegister> converter =
                                ApiClient.getClient().responseBodyConverter(APIErrorRegister.class,new Annotation[0]);
                        APIErrorRegister error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIErrorRegister.StatusBean status=error.getStatus();
                            Toast.makeText(RegistrationRestaurantt.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }


                    }

                }

                @Override
                public void onFailure(Call<RegistrationRestResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast= Toast.makeText(RegistrationRestaurantt.this,
                            t.getMessage() , Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });
        }



    private MultipartBody.Part prepareFilePart(String image, Uri imageUri) {
        String path = RealPathUtil.getRealPath(RegistrationRestaurantt.this,imageUri); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(image, file.getName(), requestFile);
    }

    public void getcusine(ArrayList<String> list) {

        StringBuilder sb = new StringBuilder();
        for (String s : list)
        {
            sb.append(s);
            sb.append("\t");
        }
        cuisinetype.setText(sb.toString());

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegistrationRestaurantt.this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(final Location loc) {

        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());

        // txtAddress.setText(Double.toString(latitude));
        Geocoder geocoder = new Geocoder(RegistrationRestaurantt.this, Locale.getDefault());

        List<android.location.Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            //city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            // getCityName(city_name);

            //glocation.setText(city_name + state + country + postalCode);
            //Gloc=city_name + state + country + postalCode;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
