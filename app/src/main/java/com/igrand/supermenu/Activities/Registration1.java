package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.igrand.supermenu.Adapters.PlacesAutoCompleteAdapter;
import com.igrand.supermenu.Adapters.RecyclerAdapterCusineTypeList;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.CusineTypeResonse;
import com.igrand.supermenu.Response.RegistrationRestResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Registration1 extends BaseActivity implements LocationListener,PlacesAutoCompleteAdapter.ClickListener  {

    Button registerbutton;
    ImageView backpurple,imageupload;
    String first_name,last_name,email,phone,password,confirm_password,restuarant_name,business_doc,city,area,
            address,Gloc,picturePath,imagePic;
    EditText City,Area,Address,glocation;
    TextView business,cuisinetype;
    ApiInterface apiInterface;
    Bitmap converetdImage;
    RegistrationRestResponse.StatusBean statusBean;
    File image = null;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private Bitmap bitmap;
    RecyclerAdapterCusineTypeList recyclerUser;
    CusineTypeResonse.StatusBean statusBean1;


    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    Double latitude, longitude;
    RadioGroup radio;
    RadioButton radio1,radio2;
    String Cuisinetype;

    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    RecyclerView recyclerView;
    String city_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration1);

        business = findViewById(R.id.business);
        City = findViewById(R.id.City);
        Area = findViewById(R.id.Area);
        Address = findViewById(R.id.Address);
        glocation = findViewById(R.id.glocation);
        registerbutton = findViewById(R.id.registerbutton);
        backpurple = findViewById(R.id.backpurple);
        imageupload = findViewById(R.id.imageupload);
        radio = findViewById(R.id.radio);
        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        cuisinetype = findViewById(R.id.cuisinetype);



        if (getIntent() != null) {

            first_name = getIntent().getStringExtra("FNAME");
            last_name = getIntent().getStringExtra("LNAME");
            email = getIntent().getStringExtra("EMAIL");
            phone = getIntent().getStringExtra("PHONE");
            password = getIntent().getStringExtra("PASSWORD");
            confirm_password = getIntent().getStringExtra("CONFIRMPASSWORD");
            restuarant_name = getIntent().getStringExtra("Restaurant");

        }


        cuisinetype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(Registration1.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.radiobutton_dialog_cuisine);


                final RecyclerView recyclerView;
                final RelativeLayout linear;
                final Button add;

                recyclerView = dialog.findViewById(R.id.recyclerView);
                linear = dialog.findViewById(R.id.linear);
                add = dialog.findViewById(R.id.add);

                dialog.show();




                final ProgressDialog progressDialog = new ProgressDialog(Registration1.this);
                progressDialog.setMessage("Loading.....");
                progressDialog.show();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<CusineTypeResonse> call = apiInterface.admincusineList();
                call.enqueue(new Callback<CusineTypeResonse>() {
                    @Override
                    public void onResponse(Call<CusineTypeResonse> call, Response<CusineTypeResonse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            linear.setVisibility(View.VISIBLE);
                            statusBean1 = response.body() != null ? response.body().getStatus() : null;
                           // Toast.makeText(Registration1.this, "City's List......", Toast.LENGTH_SHORT).show();
                            List<CusineTypeResonse.DataBean> dataBeans = response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(Registration1.this));
                            recyclerUser = new RecyclerAdapterCusineTypeList(Registration1.this, dataBeans, Registration1.this, cuisinetype, dialog,add);
                            recyclerView.setAdapter(recyclerUser);

                        } else if (response.code() != 200) {
                            progressDialog.dismiss();
                           // Toast.makeText(Registration1.this, "No City's...", Toast.LENGTH_SHORT).show();

                        }

                    }


                    @Override
                    public void onFailure(Call<CusineTypeResonse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast = Toast.makeText(Registration1.this,
                                t.getMessage(), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });


            }
        });

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (ContextCompat.checkSelfPermission(Registration1.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(Registration1.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(Registration1.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(Registration1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(Registration1.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(Registration1.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(Registration1.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(Registration1.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }


        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }



        locationManager = (LocationManager) this.getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }


        registerbutton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            //restuarant_name=restuarentname.getText().toString();
            business_doc=business.getText().toString();
            //city=City.getText().toString();
            area=Area.getText().toString();
            address=Address.getText().toString();
            //Gloc=glocation.getText().toString();

            ArrayList<String> stringList1=new ArrayList<String>();

            Cuisinetype=cuisinetype.getText().toString();

            stringList1.add(Cuisinetype);

            getData(first_name,last_name,email,phone,password,restuarant_name,stringList1,area,address,confirm_password);
        }
    });


        Places.initialize(Registration1.this, getResources().getString(R.string.google_maps_key));

    recyclerView = (RecyclerView) findViewById(R.id.places_recycler_view);
        ((EditText) findViewById(R.id.glocation)).addTextChangedListener(filterTextWatcher);

    mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(Registration1.this, latitude, longitude);
        recyclerView.setLayoutManager(new LinearLayoutManager(Registration1.this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();


}

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE) {recyclerView.setVisibility(View.VISIBLE);}
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE) {recyclerView.setVisibility(View.GONE);}
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        public void onTextChanged(CharSequence s, int start, int before, int count) { }
    };

    @Override
    public void click(Place place) {
        //Toast.makeText(getContext(), place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude, Toast.LENGTH_SHORT).show();


        glocation.setText(place.getAddress());
        Gloc=glocation.getText().toString();
        recyclerView.setVisibility(View.GONE);


        latitude=place.getLatLng().latitude;
        longitude=place.getLatLng().longitude;


        Geocoder geocoder = new Geocoder(Registration1.this, Locale.getDefault());

        List<Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();


            glocation.setText(place.getAddress());


            //txtAddress.setText(city_name + state + country + postalCode);
        } catch (IOException e) {
            e.printStackTrace();
        }

        recyclerView.setVisibility(View.GONE);

    }





    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }
                if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "No LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (this.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Registration1.this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(final Location loc) {

        Log.d(TAG, "updateUI");

        latitude = (loc.getLatitude());
        longitude = (loc.getLongitude());

        // txtAddress.setText(Double.toString(latitude));
        Geocoder geocoder = new Geocoder(Registration1.this, Locale.getDefault());

        List<android.location.Address> addresses;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city_name = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

           // getCityName(city_name);

            //glocation.setText(city_name + state + country + postalCode);
            //Gloc=city_name + state + country + postalCode;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }





    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

//the image URI
            Uri selectedImage = data.getData();

            //     imagepath=selectedImage.getPath();


            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();


            if (picturePath != null && !picturePath.equals("")) {
                image = new File(picturePath);
            }

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getData(String first_name, String last_name, String email, String phone, String password, String restuarant_name, List cusisine_type, String area, String address,String confirm_password) {





        if (first_name.equals("")){
            Toast.makeText(Registration1.this, "Please enter FirstName...", Toast.LENGTH_SHORT).show();
        }
        else if (last_name.equals("")) {

            Toast.makeText(Registration1.this, "Please enter LastName...", Toast.LENGTH_SHORT).show();
        }
        else if (phone.equals("")){
            Toast.makeText(Registration1.this, "Please enter MobileNumber...", Toast.LENGTH_SHORT).show();
        }

        else if (email.equals("")){
            Toast.makeText(Registration1.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
        }
        else if (password.equals("")) {

            Toast.makeText(Registration1.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
        }
        else if (restuarant_name.equals("")){
            Toast.makeText(Registration1.this, "Please enter RestuarantName...", Toast.LENGTH_SHORT).show();
        }
        else if (cusisine_type.equals("")) {

            Toast.makeText(Registration1.this, "Please enter CusineType...", Toast.LENGTH_SHORT).show();
        }
        else if (imageupload.equals("")){
            Toast.makeText(Registration1.this, "Please select BusinessDoc...", Toast.LENGTH_SHORT).show();
        }

        else if (Gloc.equals("")){
            Toast.makeText(Registration1.this, "Please enter Glocation...", Toast.LENGTH_SHORT).show();
        }
        else if (confirm_password.equals("")) {

            Toast.makeText(Registration1.this, "Please ConfirmPassword...", Toast.LENGTH_SHORT).show();
        }





            final ProgressDialog progressDialog=new ProgressDialog(Registration1.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            MultipartBody.Part body = null;
            if (image != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                body = MultipartBody.Part.createFormData("businessdoc", image.getName(), requestFile);

            } else {
                Toast.makeText(this, "Image not selected...", Toast.LENGTH_SHORT).show();
            }

            List<MultipartBody.Part> descriptionList = new ArrayList<>();

            RequestBody firstname1 = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
            RequestBody lastname1 = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
            RequestBody email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody phone1 = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
            RequestBody password1 = RequestBody.create(MediaType.parse("multipart/form-data"), password);
            RequestBody restuarantname1 = RequestBody.create(MediaType.parse("multipart/form-data"), restuarant_name);
            descriptionList.add(MultipartBody.Part.createFormData("cuisine_type[]", Cuisinetype ));
           // RequestBody cusisine1 = RequestBody.create(MediaType.parse("multipart/form-data"), cusisine_type);
            RequestBody city1 = RequestBody.create(MediaType.parse("multipart/form-data"), city_name);
            RequestBody area1 = RequestBody.create(MediaType.parse("multipart/form-data"), area);
            RequestBody address1 = RequestBody.create(MediaType.parse("multipart/form-data"), address);
            RequestBody gloc1 = RequestBody.create(MediaType.parse("multipart/form-data"), Gloc);
            RequestBody restuarant1 = RequestBody.create(MediaType.parse("multipart/form-data"), "restaurant");




                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                Call<RegistrationRestResponse> call = apiInterface.registerrestresponse(restuarant1,firstname1,lastname1,phone1,email1,password1,restuarantname1, descriptionList,city1,address1,gloc1,body);
                call.enqueue(new Callback<RegistrationRestResponse>() {
                    @Override
                    public void onResponse(Call<RegistrationRestResponse> call, Response<RegistrationRestResponse> response) {

                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            statusBean = response.body() != null ? response.body().getStatus() : null;
                            //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                            List<RegistrationRestResponse.DataBean> dataBean = response.body().getData();

                            Toast.makeText(Registration1.this, "Registered Successfully...", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(Registration1.this,Login.class);
                            intent.putExtra("Type","restaurant");
                            startActivity(intent);

                        }
                        else if(response.code()!=200)
                        {
                            progressDialog.dismiss();
                            Converter<ResponseBody, APIError> converter =
                                    ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                            APIError error;
                            try {
                                error = converter.convert(response.errorBody());
                                APIError.StatusBean status=error.getStatus();
                                Toast.makeText(Registration1.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                        }

                    }


                    @Override
                    public void onFailure(Call<RegistrationRestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Toast toast= Toast.makeText(Registration1.this,
                                t.getMessage() , Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                        toast.show();


                    }
                });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }

    }


    public void getcusine(ArrayList<String> list) {

        StringBuilder sb = new StringBuilder();
        for (String s : list)
        {
            sb.append(s);
            sb.append("\t");
        }
        cuisinetype.setText(sb.toString());

    }
}
