package com.igrand.supermenu.Activities;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Fragments.CustomBottomSheetDialogFragmentRegister;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.LoginResponse1;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Login extends BaseActivity {

    Button loginbutton;
    TextView forgetpassword,register,password;
    EditText email1;
    TextView log;
    String emailid,password1,Type,user_id,first_name,last_name,email,phone,usertype,restaurant_id,restuarent_name;
    ApiInterface apiInterface;
    LoginResponse.StatusBean statusBean;
    PrefManager prefManager;
    String checking;
    private Boolean exit = false;
    TextInputLayout txt_password,txt_email;
    Dialog dialog;
    RadioGroup rg;
    RadioButton restuarent, user;
    Button registerbutton;
    PrefManagerRestuarent prefManager1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        forgetpassword=findViewById(R.id.forgetpassword);
        register=findViewById(R.id.register);
        email1=findViewById(R.id.email);
        password=findViewById(R.id.password);
        log=findViewById(R.id.log);
        txt_email=findViewById(R.id.txt_email);
        txt_password=findViewById(R.id.txt_passwrd);

//        checking=getIntent().getStringExtra("Home");
        prefManager=new PrefManager(Login.this);
        prefManager1=new PrefManagerRestuarent(Login.this);

        if(getIntent()!=null) {
            Type = getIntent().getStringExtra("Type");
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchScreen();
                /*Intent intent=new Intent(Login.this,Registration.class);
                intent.putExtra("Type",Type);
                startActivity(intent);*/
            }
        });
        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog dialog = new BottomSheetDialog(Login.this);
                dialog.setContentView(R.layout.activity_register);
                new CustomBottomSheetDialogFragmentRegister().show(getSupportFragmentManager(), "Dialog");
            }
        });
        loginbutton=findViewById(R.id.loginbutton);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                emailid=email1.getText().toString();
                password1=password.getText().toString();

/*

                if ((!isValidEmail(emailid))) { 
                    return; 
                }
*/

                if (emailid.equals("")){
                    Toast.makeText(Login.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
                }
                 else if (password1.equals("")) {

                    Toast.makeText(Login.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
                }

                else {

                    final ProgressDialog progressDialog = new ProgressDialog(Login.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();


                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse> call = apiInterface.loginresponse(emailid,password1);
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                List<LoginResponse.DataBean> dataBean = response.body().getData();


                                user_id=dataBean.get(0).getUser_id();
                                first_name=dataBean.get(0).getFirst_name();
                                last_name=dataBean.get(0).getLast_name();
                                phone=dataBean.get(0).getPhone();
                                email=dataBean.get(0).getEmail();
                                usertype=dataBean.get(0).getUser_type();
                                restaurant_id=dataBean.get(0).getRestaurant_id();
                                restuarent_name=dataBean.get(0).getRestaurant_name();


                                if(usertype.equals("user")){

                                    Intent intent=new Intent(Login.this,BottonNavigationUser.class);
                                    intent.putExtra("UserId",user_id);
                                    startActivity(intent);
                                    prefManager.createLogin(user_id,password1,first_name,last_name,phone,email);
                                } else if(usertype.equals("restaurant")){

                                    Intent intent=new Intent(Login.this,BottomNavigation.class);
                                    intent.putExtra("UserId",user_id);
                                    startActivity(intent);
                                    prefManager1.createLogin(restaurant_id,password1,first_name,last_name,phone,email,restuarent_name);

                                }



                                //Toast.makeText(Login.this, "Login Successfully done..", Toast.LENGTH_SHORT).show();

                            }


                            else if(response.code()!=200)
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, APIError> converter =
                                        ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                                APIError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    APIError.StatusBean status=error.getStatus();
                                    Toast.makeText(Login.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                            }

                        }


                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(Login.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });
                }


              /*  if((!emailid.isEmpty() || !emailid.equals("")) && (!password1.isEmpty() || password1.equals(""))) {




                } else {

                }*/


            }
        });
    }

    private void launchScreen() {
            dialog = new Dialog(Login.this);
            dialog.setContentView(R.layout.dialogboxlogin);
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = dialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            rg = dialog.findViewById(R.id.rg);
            restuarent = dialog.findViewById(R.id.restuarent);
            user = dialog.findViewById(R.id.user);
            registerbutton = dialog.findViewById(R.id.registerbutton);
            registerbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (restuarent.isChecked()) {

                        dialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), RegistrationRestuarent.class);
                        intent.putExtra("Type", "restaurant");
                        startActivity(intent);

                    } else if (user.isChecked()) {

                        dialog.dismiss();
                        Intent intent = new Intent(getApplicationContext(), Registration.class);
                        intent.putExtra("Type", "user");
                        startActivity(intent);
                    } else {

                        Toast.makeText(Login.this, "Please select User/Restaurant...", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }


}
