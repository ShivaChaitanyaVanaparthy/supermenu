package com.igrand.supermenu.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.CopounResponse;
import com.igrand.supermenu.Response.OrderdetailsResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ViewDetailsOrder extends BaseActivity {

    ImageView backpurple;
    String time,order_id,Reviews,Ratings,restuarent_name,status,value,location1,month,time1,offer_name,delivery_type,status0,status11,Copoun,Delivery,user_id,Code;
    TextView date,count,comment,order_id1,restuarent_name1,status1,location,discount_price,time11,time2,offername,deliverytype,name1;
    ApiInterface apiInterface;
    RatingBar rating;
    OrderdetailsResponse.StatusBean statusBean;
    LinearLayout linear;
    PrefManagerRestuarent prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details_order);


        backpurple = findViewById(R.id.backpurple);
        date = findViewById(R.id.date);
        order_id1 = findViewById(R.id.order_id);
        restuarent_name1 = findViewById(R.id.restuarent_name);
        status1 = findViewById(R.id.status);
        location = findViewById(R.id.location);
        discount_price = findViewById(R.id.discount_price);
        time11 = findViewById(R.id.time);
        time2 = findViewById(R.id.time1);
        offername = findViewById(R.id.offername);
        deliverytype = findViewById(R.id.deliverytype);
        name1 = findViewById(R.id.name1);
        linear = findViewById(R.id.linear);
        count = findViewById(R.id.count);
        comment = findViewById(R.id.comment);
        rating = findViewById(R.id.rating);


        prefManager = new PrefManagerRestuarent(ViewDetailsOrder.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        user_id = profile.get("Userid");


        if (getIntent() != null) {

            time = getIntent().getStringExtra("Time");
            order_id = getIntent().getStringExtra("Order_Id");
            restuarent_name = getIntent().getStringExtra("RestuarentName");
            status = getIntent().getStringExtra("Status");
            status0 = getIntent().getStringExtra("Status0");
            status11 = getIntent().getStringExtra("Status1");
            value = getIntent().getStringExtra("Value");
            location1 = getIntent().getStringExtra("Location");
            month = getIntent().getStringExtra("Month");
            time1 = getIntent().getStringExtra("Time1");
            offer_name = getIntent().getStringExtra("OfferName");
            delivery_type = getIntent().getStringExtra("Delivery_type");
            Reviews = getIntent().getStringExtra("Reviews");
            Ratings = getIntent().getStringExtra("Ratings");

        }



              /*  //date.setText(time);
                order_id1.setText(order_id);
                restuarent_name1.setText(restuarent_name);
                status1.setText(status);
                location.setText(location1);
                discount_price.setText(value);
                //time11.setText(time1);
                //time2.setText(time1);
                offername.setText(offer_name);
                name1.setText(restuarent_name);
                //deliverytype.setText(delivery_type);
                status1.setBackgroundResource(R.drawable.textbackgroundpurple);
                deliverytype.setBackgroundResource(R.drawable.textviewlightpurple);*/




        final ProgressDialog progressDialog = new ProgressDialog(ViewDetailsOrder.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<OrderdetailsResponse> call = apiInterface.viewDetails(order_id);
        call.enqueue(new Callback<OrderdetailsResponse>() {
            @Override
            public void onResponse(Call<OrderdetailsResponse> call, Response<OrderdetailsResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    linear.setVisibility(View.VISIBLE);
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<OrderdetailsResponse.DataBean> dataBean = response.body().getData();


                    Copoun = dataBean.get(0).getCoupon_number();
                    Delivery = dataBean.get(0).getDelivery_type();
                    time11.setText(dataBean.get(0).getCoupon_success_time());
                    time2.setText(dataBean.get(0).getOrder_date());
                    discount_price.setText(dataBean.get(0).getOffer_price());
                    name1.setText(dataBean.get(0).getRestaurant_name());
                    date.setText(dataBean.get(0).getOrder_date());
                    restuarent_name1.setText(dataBean.get(0).getRestaurant_name());
                    order_id1.setText(order_id);
                    count.setText(dataBean.get(0).getItem_count());
                    offername.setText(offer_name);

                    comment.setText(Reviews);
                    if(Ratings.equals("")){
                    }else {
                        rating.setRating(Float.parseFloat(Ratings));
                    }
                   /* if(Ratings!=null||!Ratings.equals("")){
                        rating.setRating(Float.parseFloat(Ratings));
                    }*/

                    // couponcode.setText(Copoun);
                    deliverytype.setText(dataBean.get(0).getDelivery_type());

                    if(dataBean.get(0).getOrder_status().equals("1")){
                        status1.setText("SUCCESS");
                        status1.setBackgroundResource(R.drawable.textgreenbackground);

                    }else if(dataBean.get(0).getOrder_status().equals("0")){
                        status1.setText("CLAIMED");
                        status1.setBackgroundResource(R.drawable.textbackgroundpurple);
                    }else if(dataBean.get(0).getOrder_status().equals("2")){
                        status1.setText("EXPIRED");
                        status1.setBackgroundResource(R.drawable.textviewpink);
                    }


                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(ViewDetailsOrder.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }


                }

            }

            @Override
            public void onFailure(Call<OrderdetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast = Toast.makeText(ViewDetailsOrder.this,
                        t.getMessage(), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });


        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}
