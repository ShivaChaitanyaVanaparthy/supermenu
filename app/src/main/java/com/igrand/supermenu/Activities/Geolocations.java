package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import com.igrand.supermenu.R;

public class Geolocations extends BaseActivity {

    WebView webView;
    String iframe;
    ImageView backpurple;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geolocations);

        webView=findViewById(R.id.webView);
        backpurple=findViewById(R.id.backpurple);


        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(getIntent()!=null) {

            iframe=getIntent().getStringExtra("Iframe");
        }

        WebSettings webViewSettings = webView.getSettings();
        webViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webViewSettings.setJavaScriptEnabled(true);
        //webViewSettings.setPluginsEnabled(true);
        webViewSettings.setBuiltInZoomControls(true);
        webViewSettings.setPluginState(WebSettings.PluginState.ON);
        //webView.loadData(iframe, "text/html; charset=UTF-8", null);
        webView.loadData(iframe, "text/html", null);
    }
}
