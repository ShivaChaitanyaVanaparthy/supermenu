package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Fragments.CustomBottomSheetDialogFragmentRegister;
import com.igrand.supermenu.Fragments.CustomBottomSheetDialogFragmentRegister1;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.LoginResponse1;

import java.util.List;

public class LoginRestuarent extends BaseActivity {

    Button loginbutton;
    TextView forgetpassword,register,password;
    EditText email1;
    TextView log;
    String emailid,password1,Type,user_id,first_name,last_name,email,phone,restuarent_name;
    ApiInterface apiInterface;
    LoginResponse1.StatusBean statusBean;
    PrefManagerRestuarent prefManager;
    String checking,restuarent_id;
    private Boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_restuarent);
        forgetpassword=findViewById(R.id.forgetpassword);
        register=findViewById(R.id.register);
        email1=findViewById(R.id.email);
        password=findViewById(R.id.password);
        log=findViewById(R.id.log);


        checking=getIntent().getStringExtra("Home");

        prefManager=new PrefManagerRestuarent(LoginRestuarent.this);

        if(getIntent()!=null) {

            Type = getIntent().getStringExtra("Type");

        }


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginRestuarent.this,RegistrationRestuarent.class);
                intent.putExtra("Type",Type);
                startActivity(intent);
            }
        });
        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialog dialog = new BottomSheetDialog(LoginRestuarent.this);
                dialog.setContentView(R.layout.activity_register);
                new CustomBottomSheetDialogFragmentRegister1().show(getSupportFragmentManager(), "Dialog");
            }
        });
        loginbutton=findViewById(R.id.loginbutton);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                emailid=email1.getText().toString();
                password1=password.getText().toString();

                if (emailid.equals("")){
                    Toast.makeText(LoginRestuarent.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
                }
                else if (password1.equals("")) {

                    Toast.makeText(LoginRestuarent.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
                }

                else
                    {

                    final ProgressDialog progressDialog = new ProgressDialog(LoginRestuarent.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();


                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<LoginResponse1> call = apiInterface.loginresponse1(emailid,password1,Type);
                    call.enqueue(new Callback<LoginResponse1>() {
                        @Override
                        public void onResponse(Call<LoginResponse1> call, Response<LoginResponse1> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;
                                //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                                List<LoginResponse1.DataBean> dataBean = response.body().getData();


                                restuarent_id=dataBean.get(0).getRestaurant_id();
                                restuarent_name=dataBean.get(0).getRestaurant_name();
                                first_name=dataBean.get(0).getFname();
                                last_name=dataBean.get(0).getLname();
                                phone=dataBean.get(0).getPhone();
                                email=dataBean.get(0).getEmail();

                                Intent intent=new Intent(LoginRestuarent.this,BottomNavigation.class);
                                intent.putExtra("UserId",user_id);
                                startActivity(intent);
                                prefManager.createLogin(restuarent_id,password1,first_name,last_name,phone,email,restuarent_name);

                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(LoginRestuarent.this, "Invalid Credentials...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<LoginResponse1> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(LoginRestuarent.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });

                }

            }
        });
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
