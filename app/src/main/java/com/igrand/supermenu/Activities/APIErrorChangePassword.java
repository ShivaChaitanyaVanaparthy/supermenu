package com.igrand.supermenu.Activities;

public class APIErrorChangePassword {


    /**
     * status : 0
     * message : Updating Password Failed ! Please Try Again
     */

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
