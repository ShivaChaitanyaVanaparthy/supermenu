package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.igrand.supermenu.R;

public class RegistrationRestuarent extends BaseActivity {

    Button registerbutton;
    ImageView backregister;
    EditText firstname,lastname,email,mobilenumber,password,confirmpassword,restaurantname;
    String first_name,last_name,email1,mobile_number,password1,confirmpassword1,restaurantname1;
    String type;
    RadioButton restuarent,user;
    String Type,user_id,user_name,phone;
    CheckBox checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_restuarent);

        backregister = findViewById(R.id.backregister);
        firstname = findViewById(R.id.firstname);
        lastname = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        mobilenumber = findViewById(R.id.mobile);
        password = findViewById(R.id.passkey);
        confirmpassword = findViewById(R.id.confirmpasskey);
        registerbutton=findViewById(R.id.registerbutton);
        checkbox=findViewById(R.id.checkbox);
        restaurantname=findViewById(R.id.restuarentname);

        backregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        registerbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                first_name=firstname.getText().toString();
                last_name=lastname.getText().toString();
                email1=email.getText().toString();
                phone=mobilenumber.getText().toString();
                password1=password.getText().toString();
                confirmpassword1=confirmpassword.getText().toString();
                restaurantname1=restaurantname.getText().toString();


                if (first_name.isEmpty()){
                    Toast.makeText(RegistrationRestuarent.this, "Please enter FirstName...", Toast.LENGTH_SHORT).show();
                }
                 if (last_name.equals("")) {

                    Toast.makeText(RegistrationRestuarent.this, "Please enter LastName...", Toast.LENGTH_SHORT).show();
                }
                 if (phone.equals("")){
                    Toast.makeText(RegistrationRestuarent.this, "Please enter MobileNumber...", Toast.LENGTH_SHORT).show();
                }

                 if (email1.equals("")){
                    Toast.makeText(RegistrationRestuarent.this, "Please enter Email...", Toast.LENGTH_SHORT).show();
                }
                 if (password1.equals("")) {

                    Toast.makeText(RegistrationRestuarent.this, "Please enter Password...", Toast.LENGTH_SHORT).show();
                }
                 if (confirmpassword1.equals("")){
                    Toast.makeText(RegistrationRestuarent.this, "Please Confirm Password...", Toast.LENGTH_SHORT).show();
                }if (restaurantname.equals("")){
                    Toast.makeText(RegistrationRestuarent.this, "Please Enter Restaurant Name..", Toast.LENGTH_SHORT).show();
                }

                else if (checkbox.isChecked()==false) {
                    Toast.makeText(RegistrationRestuarent.this, "Please Accept Terms & Conditions...", Toast.LENGTH_SHORT).show();
                }

                     if(password1.equals(confirmpassword1)) {


                         if(checkbox.isChecked() && !first_name.isEmpty() && !last_name.isEmpty() &&!phone.isEmpty() && !email1.isEmpty() && !password1.isEmpty() && !confirmpassword1.isEmpty()){

                             Intent intent = new Intent(RegistrationRestuarent.this, RegistrationRestaurantt.class);
                             intent.putExtra("FNAME", first_name);
                             intent.putExtra("LNAME", last_name);
                             intent.putExtra("EMAIL", email1);
                             intent.putExtra("PHONE", phone);
                             intent.putExtra("PASSWORD", password1);
                             intent.putExtra("CONFIRMPASSWORD", confirmpassword1);
                             intent.putExtra("Restaurant", restaurantname1);
                             startActivity(intent);

                         } else {

                             Toast.makeText(RegistrationRestuarent.this, "Please Enter all the fields", Toast.LENGTH_SHORT).show();
                         }

                     } else {

                         Toast.makeText(RegistrationRestuarent.this, "Please Check the Password", Toast.LENGTH_SHORT).show();
                     }








               /* if(password.equals(confirmpassword1)) {

                    {

                        Intent intent = new Intent(RegistrationRestuarent.this, Registration1.class);
                        intent.putExtra("FNAME", first_name);
                        intent.putExtra("LNAME", last_name);
                        intent.putExtra("EMAIL", email1);
                        intent.putExtra("PHONE", phone);
                        intent.putExtra("PASSWORD", password1);
                        intent.putExtra("CONFIRMPASSWORD", confirmpassword1);
                        startActivity(intent);

                    }
                }

                else {

                    Toast.makeText(RegistrationRestuarent.this, "Please check the Password...", Toast.LENGTH_SHORT).show();
                }*/


            }
        });
    }
}
