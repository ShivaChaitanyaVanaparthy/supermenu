package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.ForgetPasswordUpdateResponse;
import com.igrand.supermenu.Response.UserPasswordResponse;

import java.util.HashMap;

public class ForgetPasswordRestuarant extends AppCompatActivity {

    ImageView backpurple;
    Button submitbutton;
    String password,user_id;
    EditText oldpassword,newpassword,confirmpassword;
    String oldpassword1,newpassword1,confirmpassword1,email;
    ApiInterface apiInterface;
    ForgetPasswordUpdateResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_restuarant);

        backpurple=findViewById(R.id.backpurple);
        submitbutton=findViewById(R.id.submitbutton);
        oldpassword=findViewById(R.id.oldpassword);
        newpassword =findViewById(R.id.newpassword);
        confirmpassword=findViewById(R.id.confirmpassword);

        if(getIntent()!=null){

            email=getIntent().getStringExtra("Email");
        }


        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                oldpassword1=oldpassword.getText().toString();
                newpassword1=newpassword.getText().toString();
                confirmpassword1=confirmpassword.getText().toString();

                if(newpassword1.equals(confirmpassword1)) {

                    final ProgressDialog progressDialog = new ProgressDialog(ForgetPasswordRestuarant.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();


                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<ForgetPasswordUpdateResponse> call = apiInterface.restuarantpasswordupdateresponse(email,"restaurant",oldpassword1,newpassword1);
                    call.enqueue(new Callback<ForgetPasswordUpdateResponse>() {
                        @Override
                        public void onResponse(Call<ForgetPasswordUpdateResponse> call, Response<ForgetPasswordUpdateResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;

                                Intent intent = new Intent(ForgetPasswordRestuarant.this, LoginRestuarent.class);
                                intent.putExtra("Home",false);
                                intent.putExtra("Type","restaurant");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                Toast.makeText(ForgetPasswordRestuarant.this, "Password Changed Successfully...", Toast.LENGTH_SHORT).show();


                            } else if (response.code() != 200) {
                                progressDialog.dismiss();
                                Toast.makeText(ForgetPasswordRestuarant.this, "No data found...", Toast.LENGTH_SHORT).show();

                            }

                        }


                        @Override
                        public void onFailure(Call<ForgetPasswordUpdateResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(ForgetPasswordRestuarant.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });



                } else {
                    Toast.makeText(ForgetPasswordRestuarant.this, "Please confirm password...", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
