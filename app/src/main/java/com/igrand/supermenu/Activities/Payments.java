package com.igrand.supermenu.Activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.supermenu.Adapters.RecyclerPaymentAdapter;
import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.PaymentsResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class Payments extends BaseActivity {

    RecyclerView recycler_payments;
    RecyclerPaymentAdapter recyclerPaymentAdapter;
    ImageView backpurple;
    PrefManagerRestuarent prefManagerRestuarent;
    String user_id;
    ApiInterface apiInterface;
    PaymentsResponse.StatusBean statusBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        recycler_payments=findViewById(R.id.recycler_payments);
        backpurple=findViewById(R.id.backpurple);

        prefManagerRestuarent=new PrefManagerRestuarent(Payments.this);
        HashMap<String, String> profile=prefManagerRestuarent.getUserDetails();
        user_id=profile.get("Userid");



        final ProgressDialog progressDialog = new ProgressDialog(Payments.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<PaymentsResponse> call = apiInterface.paymentresponse(user_id);
        call.enqueue(new Callback<PaymentsResponse>() {
            @Override
            public void onResponse(Call<PaymentsResponse> call, Response<PaymentsResponse> response) {

                if (response.code() == 200) {
                    progressDialog.dismiss();
                    statusBean = response.body() != null ? response.body().getStatus() : null;
                    //Toast.makeText(Login.this, "Volunteer is Exist.", Toast.LENGTH_SHORT).show();
                    List<PaymentsResponse.DataBean> dataBean = response.body().getData();

                    recycler_payments.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerPaymentAdapter = new RecyclerPaymentAdapter(getApplicationContext(),dataBean);
                    recycler_payments.setAdapter(recyclerPaymentAdapter);

                } else if (response.code() != 200) {
                    progressDialog.dismiss();
                    Converter<ResponseBody, APIError> converter =
                            ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                    APIError error;
                    try {
                        error = converter.convert(response.errorBody());
                        APIError.StatusBean status=error.getStatus();
                        Toast.makeText(Payments.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) { e.printStackTrace(); }

                }

            }


            @Override
            public void onFailure(Call<PaymentsResponse> call, Throwable t) {
                progressDialog.dismiss();
                // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Toast toast= Toast.makeText(Payments.this,
                        t.getMessage() , Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                toast.show();


            }
        });

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
}
