package com.igrand.supermenu.Activities;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.UserPasswordResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;

public class ChangePassword extends BaseActivity {

    ImageView backpurple;
    Button submitbutton;
    String password,user_id;
    PrefManager prefManager;
    EditText oldpassword,newpassword,confirmpassword;
    String oldpassword1,newpassword1,confirmpassword1;
    ApiInterface apiInterface;
    UserPasswordResponse.StatusBean statusBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        backpurple=findViewById(R.id.backpurple);
        submitbutton=findViewById(R.id.submitbutton);
        oldpassword=findViewById(R.id.oldpassword);
        newpassword =findViewById(R.id.newpassword);
        confirmpassword=findViewById(R.id.confirmpassword);



        prefManager=new PrefManager(ChangePassword.this);
        HashMap<String, String> profile=prefManager.getUserDetails();
        user_id=profile.get("Userid");
        password=profile.get("password");

        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                oldpassword1=oldpassword.getText().toString();
                newpassword1=newpassword.getText().toString();
                confirmpassword1=confirmpassword.getText().toString();

                if(oldpassword1==null||oldpassword1.equals("")){
                    Toast.makeText(ChangePassword.this, "Please Enter Old Password", Toast.LENGTH_SHORT).show();
                } else if(newpassword1==null||newpassword1.equals("")){
                    Toast.makeText(ChangePassword.this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
                }else if(confirmpassword1==null||newpassword1.equals("")){
                    Toast.makeText(ChangePassword.this, "Please Confirm Password", Toast.LENGTH_SHORT).show();
                }

                else if(newpassword1.equals(confirmpassword1)) {

                    final ProgressDialog progressDialog = new ProgressDialog(ChangePassword.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();


                    apiInterface = ApiClient.getClient().create(ApiInterface.class);
                    Call<UserPasswordResponse> call = apiInterface.userpasswordresponse(user_id,oldpassword1,newpassword1);
                    call.enqueue(new Callback<UserPasswordResponse>() {
                        @Override
                        public void onResponse(Call<UserPasswordResponse> call, Response<UserPasswordResponse> response) {

                            if (response.code() == 200) {
                                progressDialog.dismiss();
                                statusBean = response.body() != null ? response.body().getStatus() : null;

                                Intent intent = new Intent(ChangePassword.this, Login.class);
                                prefManager.clearSession();
                                intent.putExtra("Home",false);
                                intent.putExtra("Type","user");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                                Toast.makeText(ChangePassword.this, "Password Changed Successfully...", Toast.LENGTH_SHORT).show();


                            } else if(response.code()!=200)
                            {
                                progressDialog.dismiss();
                                Converter<ResponseBody, APIErrorChangePassword> converter =
                                        ApiClient.getClient().responseBodyConverter(APIErrorChangePassword.class,new Annotation[0]);
                                APIErrorChangePassword error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    Toast.makeText(ChangePassword.this,""+error.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                            }
                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                        }


                        @Override
                        public void onFailure(Call<UserPasswordResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            Toast toast= Toast.makeText(ChangePassword.this,
                                    t.getMessage() , Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 20, 20);
                            toast.show();


                        }
                    });










                } else {
                    Toast.makeText(ChangePassword.this, "Please check the password...", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
