package com.igrand.supermenu.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Client.ApiClient;
import com.igrand.supermenu.Fragments.OffersFragment;
import com.igrand.supermenu.Interface.ApiInterface;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.AddOfferResponse;
import com.igrand.supermenu.Response.LoginResponse;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class AddOffers extends BaseActivity implements AdapterView.OnItemSelectedListener{

    String[] expiry={"TODAY","TOMORROW"};

        ImageView backpurple,imageupload;
        TextView image0;
    Button loginbutton;
    EditText offer_name,actual_price,offer_price,description;
    String offer_name1,expirytime1,actual_price1,offer_price1,description1,image1,picturePath,restaurent_id,type,type1,imagePic,restaurent_name,type2;
    File image = null;
    Spinner expirytime;
    Bitmap converetdImage;
    ApiInterface apiInterface;
    AddOfferResponse.StatusBean statusBean;
    PrefManagerRestuarent prefManager;
    RadioGroup rg,rg1,radio;
    private Bitmap bitmap;
    RadioButton prepaid,prepaid1,postpaid,postpaid1,radio1,radio2;
    public static final int REQUEST_CODE = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSION_EXTERNAL = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    private ArrayList<Image> images;
    String emptydocument="";
    private final int select_photo = 1;
    private final static int ALL_PERMISSIONS_RESULT = 101;
    Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offers);

        backpurple=findViewById(R.id.backpurple);
        loginbutton=findViewById(R.id.loginbutton);
        offer_name=findViewById(R.id.offer_name);
        expirytime=findViewById(R.id.expirytime);
        actual_price=findViewById(R.id.actual_price);
        offer_price=findViewById(R.id.offer_price);
        description=findViewById(R.id.description);
        image0=findViewById(R.id.image);
        imageupload=findViewById(R.id.imageupload);
        rg=findViewById(R.id.rg);
        rg1=findViewById(R.id.rg1);
        prepaid=findViewById(R.id.prepaid);
        prepaid1=findViewById(R.id.prepaid1);
        postpaid=findViewById(R.id.postpaid);
        postpaid1=findViewById(R.id.postpaid1);
        radio=findViewById(R.id.radio);
        radio1=findViewById(R.id.radio1);
        radio2=findViewById(R.id.radio2);



        expirytime.setOnItemSelectedListener(this);




        prefManager=new PrefManagerRestuarent(AddOffers.this);
        HashMap<String, String> profile=prefManager.getUserDetails();
        restaurent_id=profile.get("Userid");
        restaurent_name=profile.get("Name");


        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid){
                    type="Veg";
                }
                else {
                    type="NonVeg";
                }
            }
        });


        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.prepaid1) {
                    type1 = "Beverages";
                }
                else {
                    type1="Food";
                }
            }
        });

        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                if(checkedId==R.id.radio1) {
                    type2="1";
                } else {
                    type2="0";
                }
            }
        });



        backpurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offer_name1=offer_name.getText().toString();
                actual_price1=actual_price.getText().toString();
                offer_price1=offer_price.getText().toString();
                description1=description.getText().toString();
                image1=image0.getText().toString();

                addoffer(restaurent_id,offer_name1,expirytime1,actual_price1,offer_price1,description1,type,type1,restaurent_name,type2);


            }
        });

        if (!checkPermissions())
        {requestPermissions();}
        image0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

/*
                if (ContextCompat.checkSelfPermission(AddOffers.this,
                        Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddOffers.this,
                            Manifest.permission.CAMERA)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {
                        ActivityCompat.requestPermissions(AddOffers.this,
                                new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA);
                    }
                } else if (ContextCompat.checkSelfPermission(AddOffers.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(AddOffers.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddOffers.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(AddOffers.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        ActivityCompat.requestPermissions(AddOffers.this,
                                PERMISSION_EXTERNAL, REQUEST_EXTERNAL_STORAGE);
                    }
                }



                Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                startActivityForResult(in, select_photo);*/


                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);

            }

        });


        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {

            Picasso.get().load(imagePic).into(imageupload);

            bitmap = ((BitmapDrawable) imageupload.getDrawable().getCurrent()).getBitmap();
            Log.e("bitmap", "" + bitmap);
            converetdImage = getResizedBitmap(bitmap, 500);


        } else {


        }

        ArrayAdapter aa =  new ArrayAdapter<String>(this, R.layout.spinner_item,expiry);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        expirytime.setAdapter(aa);
    }



    //Performing action onItemSelected and onNothing selected
    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,long id) {

        expirytime1 = expirytime.getSelectedItem().toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
// TODO Auto-generated method stub

    }




    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(AddOffers.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                &&                ActivityCompat.checkSelfPermission(AddOffers.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

        )
        {
            return true;
        }
        return false;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                AddOffers.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                ALL_PERMISSIONS_RESULT
        );
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case select_photo:
                if (resultCode == RESULT_OK) {
                    try {

                        Uri imageuri = data.getData();// Get intent
                        // data

                        /*uriPath.setText("URI Path: " + imageuri.toString());
                        // Get real path and show over text view

                        realPath.setText("Real Path: " + real_Path);

                        uriPath.setVisibility(View.VISIBLE);
                        realPath.setVisibility(View.VISIBLE);*/
                        picturePath = getRealPathFromUri(AddOffers.this,
                                imageuri);
                        image = new File(picturePath);
                        Bitmap bitmap = decodeUri(AddOffers.this, imageuri, 300);// call
                        // deocde
                        // uri
                        // method
                        // Check if bitmap is not null then set image else show
                        // toast
                        if (bitmap != null){
                            imageupload.setVisibility(View.VISIBLE);
                            imageupload.setImageBitmap(bitmap);
                        }
                           // Set image over
                            // bitmap
                        else
                            Toast.makeText(AddOffers.this,
                                    "Error while decoding image.",
                                    Toast.LENGTH_SHORT).show();
                    } catch (FileNotFoundException e) {

                        e.printStackTrace();
                        Toast.makeText(AddOffers.this, "File not found.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
        }



       /* if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);

            try {
                if (images != null) {
                    for (int i = 0; i < images.size(); i++) {
                        Uri uri = Uri.fromFile(new File(images.get(0).getPath()));
                        Picasso.get().load(uri).into(imageupload);
//                        Picasso.with(getApplicationContext()).load(uri).into(profile_image);
                        image = null;
                        String filepath = images.get(0).getPath();
                        if (filepath != null && !filepath.equals("")) {
                            image = new File(filepath);
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }*/


        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                imageUri=selectedImage;
                converetdImage = getResizedBitmap(bitmap, 500);
                imageupload.setImageBitmap(converetdImage);
                imageupload.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap decodeUri(Context context, Uri uri,
                                   final int requiredSize) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver()
                .openInputStream(uri), null, o2);
    }

    // Get Original image path
    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void addoffer(String restaurent_id, String offer_name1, String expirytime1, String actual_price1, String offer_price1, String description1, String type, String type1, String restaurent_name, String type2) {


        MultipartBody.Part body = null;
        if (imageUri!=null)
        {
          body= prepareFilePart("image",imageUri);
        }


       /* MultipartBody.Part body = null;
        if (image != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            body = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
        }else {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), emptydocument);
            body = MultipartBody.Part.createFormData("image", emptydocument, requestFile);
        }*/

        if(offer_name1==null||offer_name1.equals("")){
            Toast.makeText(this, "Please Enter OfferName", Toast.LENGTH_SHORT).show();
        }else if(expirytime1==null||expirytime1.equals("")){
            Toast.makeText(this, "Please Enter Expiry Time", Toast.LENGTH_SHORT).show();
        }else if(actual_price1==null||actual_price1.equals("")){
            Toast.makeText(this, "Please Enter ActualPrice", Toast.LENGTH_SHORT).show();
        }else if(offer_price1==null||offer_price1.equals("")){
            Toast.makeText(this, "Please Enter OfferPrice", Toast.LENGTH_SHORT).show();
        }else if(description1==null||description1.equals("")){
            Toast.makeText(this, "Please Enter Description", Toast.LENGTH_SHORT).show();
        }else if(type==null||type.equals("")){
            Toast.makeText(this, "Please Select FoodType", Toast.LENGTH_SHORT).show();
        }else if(type1==null||type1.equals("")){
            Toast.makeText(this, "Please Select FoodBeverages", Toast.LENGTH_SHORT).show();
        }/*else if(type2==null||type2.equals("")){
            Toast.makeText(this, "Please Select OfferType as Primary", Toast.LENGTH_SHORT).show();
        }*//*else if(image==null){
            Toast.makeText(this, "Please Upload image to continue", Toast.LENGTH_SHORT).show();
        }*/else {

            final ProgressDialog progressDialog = new ProgressDialog(AddOffers.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();

            RequestBody OfferName = RequestBody.create(MediaType.parse("multipart/form-data"), offer_name1);
            RequestBody ExpiryTime = RequestBody.create(MediaType.parse("multipart/form-data"), expirytime1);
            RequestBody ActualPrice = RequestBody.create(MediaType.parse("multipart/form-data"), actual_price1);
            RequestBody OfferPrice = RequestBody.create(MediaType.parse("multipart/form-data"), offer_price1);
            RequestBody Description = RequestBody.create(MediaType.parse("multipart/form-data"), description1);
            RequestBody Id = RequestBody.create(MediaType.parse("multipart/form-data"), restaurent_id);
            RequestBody Ftype = RequestBody.create(MediaType.parse("multipart/form-data"), type);
            RequestBody Rname = RequestBody.create(MediaType.parse("multipart/form-data"), restaurent_name);
            RequestBody Mtype = RequestBody.create(MediaType.parse("multipart/form-data"), type1);
//            RequestBody TYPE = RequestBody.create(MediaType.parse("multipart/form-data"), type2);

            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AddOfferResponse> call = apiInterface.addofferresponse(Id, Ftype, OfferName, ExpiryTime, ActualPrice, OfferPrice, Description,Mtype, body);
            call.enqueue(new Callback<AddOfferResponse>() {
                @Override
                public void onResponse(Call<AddOfferResponse> call, Response<AddOfferResponse> response) {

                    if (response.code() == 200) {
                        progressDialog.dismiss();
                        statusBean = response.body() != null ? response.body().getStatus() : null;
                        List<AddOfferResponse.DataBean> dataBean = response.body().getData();
                        Intent intent = new Intent(AddOffers.this, BottomNavigation.class);
                        startActivity(intent);
                        Toast.makeText(AddOffers.this, "Offer Added Successfully...", Toast.LENGTH_SHORT).show();
                    }  else if(response.code()!=200)
                    {
                        progressDialog.dismiss();
                        Converter<ResponseBody, APIError> converter =
                                ApiClient.getClient().responseBodyConverter(APIError.class,new Annotation[0]);
                        APIError error;
                        try {
                            error = converter.convert(response.errorBody());
                            APIError.StatusBean status=error.getStatus();
                            Toast.makeText(AddOffers.this,""+status.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) { e.printStackTrace(); }

                                       /* progressDialog.dismiss();
                                        Toast.makeText(Login.this, statusBean.getMessage(), Toast.LENGTH_SHORT).show();*/

                    }


                }


                @Override
                public void onFailure(Call<AddOfferResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    // Toast.makeText(CustomDialogActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Toast toast = Toast.makeText(AddOffers.this,
                            t.getMessage(), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 20, 20);
                    toast.show();


                }
            });
        }



    }

    private MultipartBody.Part prepareFilePart(String image, Uri imageUri) {
        String path = RealPathUtil.getRealPath(AddOffers.this,imageUri); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(image, file.getName(), requestFile);
    }

}
