package com.igrand.supermenu.Response;

import java.util.List;

public class OrderdetailsResponse {


    /**
     * status : {"code":200,"message":"Order Details"}
     * data : [{"offerid":"31","restaurant_id":"1","restaurant_name":"Bawarchi","usermail":"Shiva.chaitanya61@gmail.com","offer_price":"9","coupon_number":"530173","item_count":"1","delivery_type":"Dinein","description":"TEST","order_status":"1","order_date":"2020-07-20 02:37:41","coupon_success_time":"20202020-0707-2020 02:39:02"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Order Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * offerid : 31
         * restaurant_id : 1
         * restaurant_name : Bawarchi
         * usermail : Shiva.chaitanya61@gmail.com
         * offer_price : 9
         * coupon_number : 530173
         * item_count : 1
         * delivery_type : Dinein
         * description : TEST
         * order_status : 1
         * order_date : 2020-07-20 02:37:41
         * coupon_success_time : 20202020-0707-2020 02:39:02
         */

        private String offerid;
        private String restaurant_id;
        private String restaurant_name;
        private String usermail;
        private String offer_price;
        private String coupon_number;
        private String item_count;
        private String delivery_type;
        private String description;
        private String order_status;
        private String order_date;
        private String coupon_success_time;

        public String getOfferid() {
            return offerid;
        }

        public void setOfferid(String offerid) {
            this.offerid = offerid;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getUsermail() {
            return usermail;
        }

        public void setUsermail(String usermail) {
            this.usermail = usermail;
        }

        public String getOffer_price() {
            return offer_price;
        }

        public void setOffer_price(String offer_price) {
            this.offer_price = offer_price;
        }

        public String getCoupon_number() {
            return coupon_number;
        }

        public void setCoupon_number(String coupon_number) {
            this.coupon_number = coupon_number;
        }

        public String getItem_count() {
            return item_count;
        }

        public void setItem_count(String item_count) {
            this.item_count = item_count;
        }

        public String getDelivery_type() {
            return delivery_type;
        }

        public void setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrder_status() {
            return order_status;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public String getCoupon_success_time() {
            return coupon_success_time;
        }

        public void setCoupon_success_time(String coupon_success_time) {
            this.coupon_success_time = coupon_success_time;
        }
    }
}
