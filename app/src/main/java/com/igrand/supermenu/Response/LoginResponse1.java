package com.igrand.supermenu.Response;

import java.util.List;

public class LoginResponse1 {


    /**
     * status : {"code":200,"message":"Restaurant Data"}
     * data : [{"Restaurant_id":"33","restaurant_name":"JustZoom","fname":"Pawan ","lname":"raj","email":"pawan.raj713@gmail.com","phone":"9576026987","address":"","restaurant_image":"http://igranddeveloper.live/super-menu/admin_assets/uploads/restaurants/"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Restaurant Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * Restaurant_id : 33
         * restaurant_name : JustZoom
         * fname : Pawan
         * lname : raj
         * email : pawan.raj713@gmail.com
         * phone : 9576026987
         * address :
         * restaurant_image : http://igranddeveloper.live/super-menu/admin_assets/uploads/restaurants/
         */

        private String Restaurant_id;
        private String restaurant_name;
        private String fname;
        private String lname;
        private String email;
        private String phone;
        private String address;
        private String restaurant_image;

        public String getRestaurant_id() {
            return Restaurant_id;
        }

        public void setRestaurant_id(String Restaurant_id) {
            this.Restaurant_id = Restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getRestaurant_image() {
            return restaurant_image;
        }

        public void setRestaurant_image(String restaurant_image) {
            this.restaurant_image = restaurant_image;
        }
    }
}
