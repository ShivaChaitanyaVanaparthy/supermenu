package com.igrand.supermenu.Response;

import java.util.List;

public class RatingResponse {


    /**
     * status : {"code":200,"message":"Ratings of a  Restaurants of a particular city"}
     * data : [{"restaurant_id":"16","average_rating":"0"},{"restaurant_id":"42","average_rating":3.9},{"restaurant_id":"45","average_rating":"0"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Ratings of a  Restaurants of a particular city
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * restaurant_id : 16
         * average_rating : 0
         */

        private String restaurant_id;
        private String average_rating;

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getAverage_rating() {
            return average_rating;
        }

        public void setAverage_rating(String average_rating) {
            this.average_rating = average_rating;
        }
    }
}
