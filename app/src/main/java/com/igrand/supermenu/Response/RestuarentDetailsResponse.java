package com.igrand.supermenu.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestuarentDetailsResponse {


    /**
     * status : {"code":200,"message":"Near By Restaurants"}
     * data : [{"email":"Shiva.chaitanya62@gmail.com","phone":"7286882456","website":"test","percentage":"","restaurant_details(cuisine_type)":"Singapore\tNorth Korean\t","businessdoc":"http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG652217767745167916322.jpg","googleloc":"5700 S Cicero Ave, Chicago, IL 60638, USA","status":"1","offer_details":[{"offer_id":"10","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/13.jpg","offer_name":"Test","offer_description":"Test","actual_price":"100","discount_price":"10","food_type":"Veg"},{"offer_id":"11","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG65221776774516791633.jpg","offer_name":"test","offer_description":"test","actual_price":"10","discount_price":"5","food_type":"Veg"},{"offer_id":"12","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG65221776774516791634.jpg","offer_name":"test","offer_description":"ytewy","actual_price":"60","discount_price":"55","food_type":"Veg"}]}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Near By Restaurants
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * email : Shiva.chaitanya62@gmail.com
         * phone : 7286882456
         * website : test
         * percentage :
         * restaurant_details(cuisine_type) : Singapore	North Korean
         * businessdoc : http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG652217767745167916322.jpg
         * googleloc : 5700 S Cicero Ave, Chicago, IL 60638, USA
         * status : 1
         * offer_details : [{"offer_id":"10","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/13.jpg","offer_name":"Test","offer_description":"Test","actual_price":"100","discount_price":"10","food_type":"Veg"},{"offer_id":"11","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG65221776774516791633.jpg","offer_name":"test","offer_description":"test","actual_price":"10","discount_price":"5","food_type":"Veg"},{"offer_id":"12","restaurant_id":"42","restaurant_name":"bawarchi","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG65221776774516791634.jpg","offer_name":"test","offer_description":"ytewy","actual_price":"60","discount_price":"55","food_type":"Veg"}]
         */

        private String email;
        private String phone;
        private String website;
        private String percentage;
        @SerializedName("restaurant_details(cuisine_type)")
        private String _$Restaurant_detailsCuisine_type187; // FIXME check this code
        private String businessdoc;
        private String googleloc;
        private String status;
        private List<OfferDetailsBean> offer_details;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String get_$Restaurant_detailsCuisine_type187() {
            return _$Restaurant_detailsCuisine_type187;
        }

        public void set_$Restaurant_detailsCuisine_type187(String _$Restaurant_detailsCuisine_type187) {
            this._$Restaurant_detailsCuisine_type187 = _$Restaurant_detailsCuisine_type187;
        }

        public String getBusinessdoc() {
            return businessdoc;
        }

        public void setBusinessdoc(String businessdoc) {
            this.businessdoc = businessdoc;
        }

        public String getGoogleloc() {
            return googleloc;
        }

        public void setGoogleloc(String googleloc) {
            this.googleloc = googleloc;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<OfferDetailsBean> getOffer_details() {
            return offer_details;
        }

        public void setOffer_details(List<OfferDetailsBean> offer_details) {
            this.offer_details = offer_details;
        }

        public static class OfferDetailsBean {
            /**
             * offer_id : 10
             * restaurant_id : 42
             * restaurant_name : bawarchi
             * offer_image : http://www.chefdeals.co/admin_assets/uploads/offers/13.jpg
             * offer_name : Test
             * offer_description : Test
             * actual_price : 100
             * discount_price : 10
             * food_type : Veg
             */

            private String offer_id;
            private String restaurant_id;
            private String restaurant_name;
            private String offer_image;
            private String offer_name;
            private String offer_description;
            private String actual_price;
            private String discount_price;
            private String food_type;

            public String getOffer_id() {
                return offer_id;
            }

            public void setOffer_id(String offer_id) {
                this.offer_id = offer_id;
            }

            public String getRestaurant_id() {
                return restaurant_id;
            }

            public void setRestaurant_id(String restaurant_id) {
                this.restaurant_id = restaurant_id;
            }

            public String getRestaurant_name() {
                return restaurant_name;
            }

            public void setRestaurant_name(String restaurant_name) {
                this.restaurant_name = restaurant_name;
            }

            public String getOffer_image() {
                return offer_image;
            }

            public void setOffer_image(String offer_image) {
                this.offer_image = offer_image;
            }

            public String getOffer_name() {
                return offer_name;
            }

            public void setOffer_name(String offer_name) {
                this.offer_name = offer_name;
            }

            public String getOffer_description() {
                return offer_description;
            }

            public void setOffer_description(String offer_description) {
                this.offer_description = offer_description;
            }

            public String getActual_price() {
                return actual_price;
            }

            public void setActual_price(String actual_price) {
                this.actual_price = actual_price;
            }

            public String getDiscount_price() {
                return discount_price;
            }

            public void setDiscount_price(String discount_price) {
                this.discount_price = discount_price;
            }

            public String getFood_type() {
                return food_type;
            }

            public void setFood_type(String food_type) {
                this.food_type = food_type;
            }
        }
    }
}
