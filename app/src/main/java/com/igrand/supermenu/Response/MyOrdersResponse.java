package com.igrand.supermenu.Response;

import java.util.List;

public class MyOrdersResponse {


    /**
     * status : {"code":200,"message":"Orders"}
     * data : [{"order_id":"18","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"oiuytre","discount_price":"12","coupon_code":"109920","item_count":"1","status":"1","order_ratings":"","order_comment":"","rating_status":"0","time":"2020-07-23 23:57:20"},{"order_id":"17","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"BIRYANI","discount_price":"190","coupon_code":"103742","item_count":"2","status":"1","order_ratings":"2.0","order_comment":"","time":"2020-07-23 23:25:10"},{"order_id":"16","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TESTOFFER2","discount_price":"100","coupon_code":"464246","item_count":"3","status":"1","order_ratings":"5.0","order_comment":"","time":"2020-07-23 23:11:59"},{"order_id":"15","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TESTTODAY@","discount_price":"90","coupon_code":"131477","item_count":"3","status":"1","order_ratings":"","order_comment":"","time":"2020-07-23 22:51:24"},{"order_id":"14","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TESTTODAY1","discount_price":"50","coupon_code":"972650","item_count":"3","status":"1","order_ratings":"","order_comment":"","time":"2020-07-23 22:46:11"},{"order_id":"13","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TESTTODAY","discount_price":"8","coupon_code":"993165","item_count":"4","status":"1","order_ratings":"","order_comment":"","time":"2020-07-23 22:39:42"},{"order_id":"12","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TEST3","discount_price":"11","coupon_code":"626397","item_count":"1","status":"1","order_ratings":"4.0","order_comment":"BAD","time":"2020-07-23 07:59:50"},{"order_id":"11","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"Test2","discount_price":"9","coupon_code":"560270","item_count":"2","status":"1","order_ratings":"3.0","order_comment":"NOT BAD","time":"2020-07-23 04:55:41"},{"order_id":"10","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TEST1","discount_price":"9","coupon_code":"726452","item_count":"1","status":"1","order_ratings":"3.0","order_comment":"EXCELLENT","time":"2020-07-23 04:35:46"},{"order_id":"9","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TOMORROW","discount_price":"10","coupon_code":"776232","item_count":"1","status":"2","order_ratings":"5.0","order_comment":"NICE","time":"2020-07-21 21:22:46"},{"order_id":"8","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"2","restaurant_name":"swagath","offer_name":"TESTTTT","discount_price":"12","coupon_code":"262958","item_count":"1","status":"2","order_ratings":"","order_comment":"","time":"2020-07-20 07:39:05"},{"order_id":"7","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"2","restaurant_name":"swagath","offer_name":"test5","discount_price":"0","coupon_code":"173024","item_count":"1","status":"2","order_ratings":"3.0","order_comment":"","time":"2020-07-20 07:30:34"},{"order_id":"6","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"2","restaurant_name":"swagath","offer_name":"test6","discount_price":"90","coupon_code":"359734","item_count":"1","status":"2","order_ratings":"4.0","order_comment":"","time":"2020-07-20 07:30:12"},{"order_id":"5","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"2","restaurant_name":"swagath","offer_name":"Test1","discount_price":"9","coupon_code":"292015","item_count":"1","status":"2","order_ratings":"5.0","order_comment":"","time":"2020-07-20 07:18:50"},{"order_id":"4","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"2","restaurant_name":"swagath","offer_name":"Test2","discount_price":"9","coupon_code":"157098","item_count":"1","status":"2","order_ratings":"5.0","order_comment":"","time":"2020-07-20 07:18:31"},{"order_id":"3","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"Testingoffer2","discount_price":"9","coupon_code":"990505","item_count":"1","status":"2","order_ratings":"5.0","order_comment":"","time":"2020-07-20 07:11:31"},{"order_id":"1","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","offer_name":"TestingOffer1","discount_price":"15","coupon_code":"720549","item_count":"1","status":"2","order_ratings":"5.0","order_comment":"Good Taste","time":"2020-07-20 04:43:21"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Orders
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * order_id : 18
         * user_first_name : shiva
         * user_last_name : vanaparthy
         * user_mail : Shiva.chaitanya61@gmail.com
         * user_mobile : 7286882456
         * restaurant_id : 1
         * restaurant_name : bawarchi
         * offer_name : oiuytre
         * discount_price : 12
         * coupon_code : 109920
         * item_count : 1
         * status : 1
         * order_ratings :
         * order_comment :
         * rating_status : 0
         * time : 2020-07-23 23:57:20
         */

        private String order_id;
        private String user_first_name;
        private String user_last_name;
        private String user_mail;
        private String user_mobile;
        private String restaurant_id;
        private String restaurant_name;
        private String offer_name;
        private String discount_price;
        private String coupon_code;
        private String item_count;
        private String status;
        private String order_ratings;
        private String order_comment;
        private String rating_status;
        private String time;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getUser_first_name() {
            return user_first_name;
        }

        public void setUser_first_name(String user_first_name) {
            this.user_first_name = user_first_name;
        }

        public String getUser_last_name() {
            return user_last_name;
        }

        public void setUser_last_name(String user_last_name) {
            this.user_last_name = user_last_name;
        }

        public String getUser_mail() {
            return user_mail;
        }

        public void setUser_mail(String user_mail) {
            this.user_mail = user_mail;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public void setCoupon_code(String coupon_code) {
            this.coupon_code = coupon_code;
        }

        public String getItem_count() {
            return item_count;
        }

        public void setItem_count(String item_count) {
            this.item_count = item_count;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrder_ratings() {
            return order_ratings;
        }

        public void setOrder_ratings(String order_ratings) {
            this.order_ratings = order_ratings;
        }

        public String getOrder_comment() {
            return order_comment;
        }

        public void setOrder_comment(String order_comment) {
            this.order_comment = order_comment;
        }

        public String getRating_status() {
            return rating_status;
        }

        public void setRating_status(String rating_status) {
            this.rating_status = rating_status;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
