package com.igrand.supermenu.Response;

import java.util.List;

public class EditOfferResponse {


    /**
     * status : {"code":200,"message":"Offers Details For Update"}
     * data : [{"menuitem":"ln2a","offercost":"5432","actualoffercost":"0987","ftype":"Veg","mtype":"Food","valid":"Tomorrow","description":"In publishing and graphic design,&nbsp;lorem ipsum&nbsp;is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying,'hai'\r\n","status":"1","image":"http://igranddeveloper.live/super-menu/admin_assets/uploads/offers/zomato.jpg"}]
     */

    public StatusBean status;
    public List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Offers Details For Update
         */

        public int code;
        public String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * menuitem : ln2a
         * offercost : 5432
         * actualoffercost : 0987
         * ftype : Veg
         * mtype : Food
         * valid : Tomorrow
         * description : In publishing and graphic design,&nbsp;lorem ipsum&nbsp;is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying,'hai'
         * status : 1
         * image : http://igranddeveloper.live/super-menu/admin_assets/uploads/offers/zomato.jpg
         */

        public String menuitem;
        public String offercost;
        public String actualoffercost;
        public String ftype;
        public String mtype;
        public String valid;
        public String description;
        public String status;
        public String image;

        public String getMenuitem() {
            return menuitem;
        }

        public void setMenuitem(String menuitem) {
            this.menuitem = menuitem;
        }

        public String getOffercost() {
            return offercost;
        }

        public void setOffercost(String offercost) {
            this.offercost = offercost;
        }

        public String getActualoffercost() {
            return actualoffercost;
        }

        public void setActualoffercost(String actualoffercost) {
            this.actualoffercost = actualoffercost;
        }

        public String getFtype() {
            return ftype;
        }

        public void setFtype(String ftype) {
            this.ftype = ftype;
        }

        public String getMtype() {
            return mtype;
        }

        public void setMtype(String mtype) {
            this.mtype = mtype;
        }

        public String getValid() {
            return valid;
        }

        public void setValid(String valid) {
            this.valid = valid;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
