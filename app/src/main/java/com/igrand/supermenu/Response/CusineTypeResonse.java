package com.igrand.supermenu.Response;

import java.util.List;

public class CusineTypeResonse {


    /**
     * status : {"code":200,"message":"Cuisine Types List"}
     * data : [{"id":"16","cuisinetype":"Malasian","dateandtime":"2020-05-27 00:39:33","status":"1"},{"id":"15","cuisinetype":"Singapore","dateandtime":"2020-05-27 00:39:24","status":"1"},{"id":"14","cuisinetype":"North Korean","dateandtime":"2020-05-27 00:39:12","status":"1"},{"id":"13","cuisinetype":"South Korean","dateandtime":"2020-05-27 00:38:58","status":"1"},{"id":"12","cuisinetype":"Russian","dateandtime":"2020-05-27 00:38:42","status":"1"},{"id":"11","cuisinetype":"German","dateandtime":"2020-05-27 00:38:32","status":"1"},{"id":"10","cuisinetype":"French","dateandtime":"2020-05-27 00:38:22","status":"1"},{"id":"9","cuisinetype":"Italian","dateandtime":"2020-05-27 00:38:09","status":"1"},{"id":"8","cuisinetype":"South India","dateandtime":"2020-05-27 00:37:57","status":"1"},{"id":"7","cuisinetype":"South America","dateandtime":"2020-05-27 00:37:44","status":"1"},{"id":"6","cuisinetype":"Japanese","dateandtime":"2020-02-28 11:27:35","status":"1"},{"id":"5","cuisinetype":"Chinese","dateandtime":"2020-02-28 11:27:25","status":"1"},{"id":"3","cuisinetype":"North America","dateandtime":"2020-02-28 11:27:08","status":"1"},{"id":"1","cuisinetype":"North Indian","dateandtime":"2020-02-28 11:26:15","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Cuisine Types List
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * id : 16
         * cuisinetype : Malasian
         * dateandtime : 2020-05-27 00:39:33
         * status : 1
         */

        private String id;
        private String cuisinetype;
        private String dateandtime;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCuisinetype() {
            return cuisinetype;
        }

        public void setCuisinetype(String cuisinetype) {
            this.cuisinetype = cuisinetype;
        }

        public String getDateandtime() {
            return dateandtime;
        }

        public void setDateandtime(String dateandtime) {
            this.dateandtime = dateandtime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
