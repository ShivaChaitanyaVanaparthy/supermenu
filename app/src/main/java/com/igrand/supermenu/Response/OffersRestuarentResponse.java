package com.igrand.supermenu.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersRestuarentResponse {


    /**
     * status : {"code":200,"message":"Near By Restaurants"}
     * data : [{"restaurant-name":"Suprabath","offer_id":"1","offer_name":"rk2","offer_cost":"12345","valid_status":"Today","valid_date":"26-09-2019","date":"2019-09-26 17:52:21","status":"1","order_count":4}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Near By Restaurants
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * restaurant-name : Suprabath
         * offer_id : 1
         * offer_name : rk2
         * offer_cost : 12345
         * valid_status : Today
         * valid_date : 26-09-2019
         * date : 2019-09-26 17:52:21
         * status : 1
         * order_count : 4
         */

        @SerializedName("restaurant-name")
        private String restaurantname;
        private String offer_id;
        private String offer_name;
        private String offer_cost;
        private String valid_status;
        private String valid_date;
        private String date;
        private String status;
        private int order_count;

        public String getRestaurantname() {
            return restaurantname;
        }

        public void setRestaurantname(String restaurantname) {
            this.restaurantname = restaurantname;
        }

        public String getOffer_id() {
            return offer_id;
        }

        public void setOffer_id(String offer_id) {
            this.offer_id = offer_id;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getOffer_cost() {
            return offer_cost;
        }

        public void setOffer_cost(String offer_cost) {
            this.offer_cost = offer_cost;
        }

        public String getValid_status() {
            return valid_status;
        }

        public void setValid_status(String valid_status) {
            this.valid_status = valid_status;
        }

        public String getValid_date() {
            return valid_date;
        }

        public void setValid_date(String valid_date) {
            this.valid_date = valid_date;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getOrder_count() {
            return order_count;
        }

        public void setOrder_count(int order_count) {
            this.order_count = order_count;
        }
    }
}
