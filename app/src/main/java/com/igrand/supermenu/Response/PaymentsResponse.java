package com.igrand.supermenu.Response;

import java.util.List;

public class PaymentsResponse {


    /**
     * status : {"code":200,"message":"Restaurant Payments"}
     * data : [{"payments_id":"1","payable":"1000","status":"1","date":"2019-11-04 15:08:42","month_name":"November","year":"2019"},{"payments_id":"2","payable":"2000","status":"1","date":"2019-11-04 15:08:42","month_name":"November","year":"2019"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Restaurant Payments
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * payments_id : 1
         * payable : 1000
         * status : 1
         * date : 2019-11-04 15:08:42
         * month_name : November
         * year : 2019
         */

        private String payments_id;
        private String payable;
        private String status;
        private String date;
        private String month_name;
        private String year;

        public String getPayments_id() {
            return payments_id;
        }

        public void setPayments_id(String payments_id) {
            this.payments_id = payments_id;
        }

        public String getPayable() {
            return payable;
        }

        public void setPayable(String payable) {
            this.payable = payable;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getMonth_name() {
            return month_name;
        }

        public void setMonth_name(String month_name) {
            this.month_name = month_name;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }
    }
}
