package com.igrand.supermenu.Response;


import java.util.List;

public class LoginResponse {


    /**
     * status : {"code":200,"message":"User Data"}
     * data : [{"user_id":"1","first_name":"chaitanya","last_name":"vanaparthy","email":"shiva.chaitanya62@gmail.com","phone":"7286882452","user_type":"user","city":"","user_image":"http://www.chefdeals.co/admin_assets/uploads/users/IMG_20200405_210923.jpg","Restaurant_id":"","restaurant_name":"","fname":"","lname":"","website":"","restaurant_image":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_id : 1
         * first_name : chaitanya
         * last_name : vanaparthy
         * email : shiva.chaitanya62@gmail.com
         * phone : 7286882452
         * user_type : user
         * city :
         * user_image : http://www.chefdeals.co/admin_assets/uploads/users/IMG_20200405_210923.jpg
         * Restaurant_id :
         * restaurant_name :
         * fname :
         * lname :
         * website :
         * restaurant_image :
         */

        private String user_id;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String user_type;
        private String city;
        private String user_image;
        private String Restaurant_id;
        private String restaurant_name;
        private String fname;
        private String lname;
        private String website;
        private String restaurant_image;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getRestaurant_id() {
            return Restaurant_id;
        }

        public void setRestaurant_id(String Restaurant_id) {
            this.Restaurant_id = Restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getRestaurant_image() {
            return restaurant_image;
        }

        public void setRestaurant_image(String restaurant_image) {
            this.restaurant_image = restaurant_image;
        }
    }
}