package com.igrand.supermenu.Response;

import java.util.List;

public class UserHomeResponse {


    /**
     * status : {"code":200,"message":"Near By Restaurants"}
     * data : [{"offer_id":"25","restaurant_id":"42","restaurant_name":"bawarchi","restaurant_address":"5700 S Cicero Ave, Chicago, IL 60638, USA","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG652217767745167916313.jpg","offer_name":"newTest2","actual_price":"30","discount_price":"22","food_type":"Veg","ratings":"3.8571428571429"},{"offer_id":"24","restaurant_id":"42","restaurant_name":"bawarchi","restaurant_address":"5700 S Cicero Ave, Chicago, IL 60638, USA","offer_image":"http://www.chefdeals.co/admin_assets/uploads/offers/IMG652217767745167916312.jpg","offer_name":"newTest1","actual_price":"20","discount_price":"12","food_type":"Veg","ratings":"3.8571428571429"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Near By Restaurants
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * offer_id : 25
         * restaurant_id : 42
         * restaurant_name : bawarchi
         * restaurant_address : 5700 S Cicero Ave, Chicago, IL 60638, USA
         * offer_image : http://www.chefdeals.co/admin_assets/uploads/offers/IMG652217767745167916313.jpg
         * offer_name : newTest2
         * actual_price : 30
         * discount_price : 22
         * food_type : Veg
         * ratings : 3.8571428571429
         */

        private String offer_id;
        private String restaurant_id;
        private String restaurant_name;
        private String restaurant_address;
        private String offer_image;
        private String offer_name;
        private String actual_price;
        private String discount_price;
        private String food_type;
        private String ratings;

        public String getOffer_id() {
            return offer_id;
        }

        public void setOffer_id(String offer_id) {
            this.offer_id = offer_id;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getRestaurant_address() {
            return restaurant_address;
        }

        public void setRestaurant_address(String restaurant_address) {
            this.restaurant_address = restaurant_address;
        }

        public String getOffer_image() {
            return offer_image;
        }

        public void setOffer_image(String offer_image) {
            this.offer_image = offer_image;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getActual_price() {
            return actual_price;
        }

        public void setActual_price(String actual_price) {
            this.actual_price = actual_price;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getFood_type() {
            return food_type;
        }

        public void setFood_type(String food_type) {
            this.food_type = food_type;
        }

        public String getRatings() {
            return ratings;
        }

        public void setRatings(String ratings) {
            this.ratings = ratings;
        }
    }
}

