package com.igrand.supermenu.Response;

import java.util.List;

public class EditProfileRestuarentResponse {


    /**
     * status : {"code":200,"message":"Restaurant Details For Update"}
     * data : [{"fname":"rama","lname":"krishna","phone":"1234567890","logo":"http://igranddeveloper.live/super-menu/admin_assets/uploads/restaurants/restaurant2.jpg"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Restaurant Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * fname : rama
         * lname : krishna
         * phone : 1234567890
         * logo : http://igranddeveloper.live/super-menu/admin_assets/uploads/restaurants/restaurant2.jpg
         */

        private String fname;
        private String lname;
        private String phone;
        private String logo;

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }
    }
}
