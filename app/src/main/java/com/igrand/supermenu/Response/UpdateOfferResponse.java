package com.igrand.supermenu.Response;

public class UpdateOfferResponse {


    /**
     * status : {"code":200,"message":"Offers Data Updated Successfully"}
     */

    public StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Offers Data Updated Successfully
         */

        public int code;
        public String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
