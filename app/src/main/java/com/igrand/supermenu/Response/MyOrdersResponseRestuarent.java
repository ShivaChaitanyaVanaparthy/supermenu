package com.igrand.supermenu.Response;

import java.util.List;

public class MyOrdersResponseRestuarent {


    /**
     * status : {"code":200,"message":"Orders"}
     * data : [{"order_id":"9","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","discount_price":"10","offer_name":"TOMORROW","coupon_code":"776232","delivery_type":"Dinein","item_count":"1","restaurant_city":"Fremont","status":"2","order_ratings":"5.0","order_comment":"NICE","time":"2020-07-21 21:22:46"},{"order_id":"3","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","discount_price":"9","offer_name":"Testingoffer2","coupon_code":"990505","delivery_type":"Parcel","item_count":"1","restaurant_city":"Fremont","status":"2","order_ratings":"5.0","order_comment":"","time":"2020-07-20 07:11:31"},{"order_id":"2","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","discount_price":"19","offer_name":"TestingOffer3","coupon_code":"681499","delivery_type":"Dinein","item_count":"1","restaurant_city":"Fremont","status":"2","order_ratings":"5.0","order_comment":"","time":"2020-07-20 07:11:14"},{"order_id":"1","user_first_name":"shiva","user_last_name":"vanaparthy","user_mail":"Shiva.chaitanya61@gmail.com","user_mobile":"7286882456","restaurant_id":"1","restaurant_name":"bawarchi","discount_price":"15","offer_name":"TestingOffer1","coupon_code":"720549","delivery_type":"Dinein","item_count":"1","restaurant_city":"Fremont","status":"2","order_ratings":"5.0","order_comment":"Good Taste","time":"2020-07-20 04:43:21"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Orders
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * order_id : 9
         * user_first_name : shiva
         * user_last_name : vanaparthy
         * user_mail : Shiva.chaitanya61@gmail.com
         * user_mobile : 7286882456
         * restaurant_id : 1
         * restaurant_name : bawarchi
         * discount_price : 10
         * offer_name : TOMORROW
         * coupon_code : 776232
         * delivery_type : Dinein
         * item_count : 1
         * restaurant_city : Fremont
         * status : 2
         * order_ratings : 5.0
         * order_comment : NICE
         * time : 2020-07-21 21:22:46
         */

        private String order_id;
        private String user_first_name;
        private String user_last_name;
        private String user_mail;
        private String user_mobile;
        private String restaurant_id;
        private String restaurant_name;
        private String discount_price;
        private String offer_name;
        private String coupon_code;
        private String delivery_type;
        private String item_count;
        private String restaurant_city;
        private String status;
        private String order_ratings;
        private String order_comment;
        private String time;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getUser_first_name() {
            return user_first_name;
        }

        public void setUser_first_name(String user_first_name) {
            this.user_first_name = user_first_name;
        }

        public String getUser_last_name() {
            return user_last_name;
        }

        public void setUser_last_name(String user_last_name) {
            this.user_last_name = user_last_name;
        }

        public String getUser_mail() {
            return user_mail;
        }

        public void setUser_mail(String user_mail) {
            this.user_mail = user_mail;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public void setCoupon_code(String coupon_code) {
            this.coupon_code = coupon_code;
        }

        public String getDelivery_type() {
            return delivery_type;
        }

        public void setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
        }

        public String getItem_count() {
            return item_count;
        }

        public void setItem_count(String item_count) {
            this.item_count = item_count;
        }

        public String getRestaurant_city() {
            return restaurant_city;
        }

        public void setRestaurant_city(String restaurant_city) {
            this.restaurant_city = restaurant_city;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrder_ratings() {
            return order_ratings;
        }

        public void setOrder_ratings(String order_ratings) {
            this.order_ratings = order_ratings;
        }

        public String getOrder_comment() {
            return order_comment;
        }

        public void setOrder_comment(String order_comment) {
            this.order_comment = order_comment;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }
}
