package com.igrand.supermenu.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RestuarentProfileResponse {


    /**
     * status : {"code":200,"message":"Near By Restaurants"}
     * data : [{"restaurant_name":"Bawarchi","restaurant_image":"http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG_20191119_12495717.jpg","cuisine_type":"South India\t","email":"shiva.chaitanya62@gmail.com","phone":"7286882459","website":"www.bawarchi.com","percentage":"","restaurant_details(cuisine_type)":"South India\t","businessdoc":"http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG_20191119_12495714.jpg","googleloc":"4287 Central Ave, Fremont, CA 94536, USA","count_of_offers/notifications":20,"status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Near By Restaurants
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * restaurant_name : Bawarchi
         * restaurant_image : http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG_20191119_12495717.jpg
         * cuisine_type : South India
         * email : shiva.chaitanya62@gmail.com
         * phone : 7286882459
         * website : www.bawarchi.com
         * percentage :
         * restaurant_details(cuisine_type) : South India
         * businessdoc : http://www.chefdeals.co/admin_assets/uploads/restaurants/IMG_20191119_12495714.jpg
         * googleloc : 4287 Central Ave, Fremont, CA 94536, USA
         * count_of_offers/notifications : 20
         * status : 1
         */

        private String restaurant_name;
        private String restaurant_image;
        private String cuisine_type;
        private String email;
        private String phone;
        private String website;
        private String percentage;
        @SerializedName("restaurant_details(cuisine_type)")
        private String _$Restaurant_detailsCuisine_type229; // FIXME check this code
        private String businessdoc;
        private String googleloc;
        @SerializedName("count_of_offers/notifications")
        private int _$Count_of_offersNotifications328; // FIXME check this code
        private String status;

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getRestaurant_image() {
            return restaurant_image;
        }

        public void setRestaurant_image(String restaurant_image) {
            this.restaurant_image = restaurant_image;
        }

        public String getCuisine_type() {
            return cuisine_type;
        }

        public void setCuisine_type(String cuisine_type) {
            this.cuisine_type = cuisine_type;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String get_$Restaurant_detailsCuisine_type229() {
            return _$Restaurant_detailsCuisine_type229;
        }

        public void set_$Restaurant_detailsCuisine_type229(String _$Restaurant_detailsCuisine_type229) {
            this._$Restaurant_detailsCuisine_type229 = _$Restaurant_detailsCuisine_type229;
        }

        public String getBusinessdoc() {
            return businessdoc;
        }

        public void setBusinessdoc(String businessdoc) {
            this.businessdoc = businessdoc;
        }

        public String getGoogleloc() {
            return googleloc;
        }

        public void setGoogleloc(String googleloc) {
            this.googleloc = googleloc;
        }

        public int get_$Count_of_offersNotifications328() {
            return _$Count_of_offersNotifications328;
        }

        public void set_$Count_of_offersNotifications328(int _$Count_of_offersNotifications328) {
            this._$Count_of_offersNotifications328 = _$Count_of_offersNotifications328;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
