package com.igrand.supermenu.Response;

import java.util.List;

public class EditProfileUserResponse1 {


    /**
     * status : {"code":200,"message":"Users Details For Update"}
     * data : [{"first_name":"shiva","last_name":"chaitanya vanaparthy","email":"shiva.chaitanya62@gmail.com","phone":"7286882452","profile":"http://igranddeveloper.live/super-menu/admin_assets/uploads/users/1.jpg"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Users Details For Update
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * first_name : shiva
         * last_name : chaitanya vanaparthy
         * email : shiva.chaitanya62@gmail.com
         * phone : 7286882452
         * profile : http://igranddeveloper.live/super-menu/admin_assets/uploads/users/1.jpg
         */

        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String profile;

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }
    }
}
