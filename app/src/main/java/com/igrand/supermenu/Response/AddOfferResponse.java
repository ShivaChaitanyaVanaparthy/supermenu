package com.igrand.supermenu.Response;

import java.util.List;

public class AddOfferResponse {


    /**
     * status : {"code":200,"message":"User Data"}
     * data : [{"menu_item":"abcd","food_type":"VEG","description":"Nothing"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Data
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * menu_item : abcd
         * food_type : VEG
         * description : Nothing
         */

        private String menu_item;
        private String food_type;
        private String description;

        public String getMenu_item() {
            return menu_item;
        }

        public void setMenu_item(String menu_item) {
            this.menu_item = menu_item;
        }

        public String getFood_type() {
            return food_type;
        }

        public void setFood_type(String food_type) {
            this.food_type = food_type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
