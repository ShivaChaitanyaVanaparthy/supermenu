package com.igrand.supermenu.Response;

import com.google.gson.annotations.SerializedName;

public class OfferClaimResponse {


    /**
     * status : {"code":200,"message":"Success"}
     * coupon code : 968640
     */

    private StatusBean status;
    @SerializedName("coupon code")
    private int _$CouponCode28; // FIXME check this code

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public int get_$CouponCode28() {
        return _$CouponCode28;
    }

    public void set_$CouponCode28(int _$CouponCode28) {
        this._$CouponCode28 = _$CouponCode28;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Success
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
