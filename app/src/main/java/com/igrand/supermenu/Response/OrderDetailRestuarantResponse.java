package com.igrand.supermenu.Response;

import java.util.List;

public class OrderDetailRestuarantResponse {


    /**
     * status : {"code":200,"message":"One Offer Order Details"}
     * data : [{"user_email":"pawan.raj713@gmail.com","user_mobile":"+919576026","offer_cost":"150","restaurant":"just ","offer_name":"Pizza ","delivery_type":"Dinein","total_item":"1","datetime":"2020-01-21 18:09:13","order_id":"1","status":"1"},{"user_email":"ramalingesh.putta@gmail.com ","user_mobile":"9052560563","offer_cost":"150","restaurant":"just ","offer_name":"Pizza ","delivery_type":"Dinein","total_item":"1","datetime":"2020-01-21 23:18:30","order_id":"2","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : One Offer Order Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_email : pawan.raj713@gmail.com
         * user_mobile : +919576026
         * offer_cost : 150
         * restaurant : just
         * offer_name : Pizza
         * delivery_type : Dinein
         * total_item : 1
         * datetime : 2020-01-21 18:09:13
         * order_id : 1
         * status : 1
         */

        private String user_email;
        private String user_mobile;
        private String offer_cost;
        private String restaurant;
        private String offer_name;
        private String delivery_type;
        private String total_item;
        private String datetime;
        private String order_id;
        private String status;

        public String getUser_email() {
            return user_email;
        }

        public void setUser_email(String user_email) {
            this.user_email = user_email;
        }

        public String getUser_mobile() {
            return user_mobile;
        }

        public void setUser_mobile(String user_mobile) {
            this.user_mobile = user_mobile;
        }

        public String getOffer_cost() {
            return offer_cost;
        }

        public void setOffer_cost(String offer_cost) {
            this.offer_cost = offer_cost;
        }

        public String getRestaurant() {
            return restaurant;
        }

        public void setRestaurant(String restaurant) {
            this.restaurant = restaurant;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getDelivery_type() {
            return delivery_type;
        }

        public void setDelivery_type(String delivery_type) {
            this.delivery_type = delivery_type;
        }

        public String getTotal_item() {
            return total_item;
        }

        public void setTotal_item(String total_item) {
            this.total_item = total_item;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
