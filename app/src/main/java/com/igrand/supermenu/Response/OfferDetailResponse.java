package com.igrand.supermenu.Response;

import java.util.List;

public class OfferDetailResponse {


    /**
     * status : {"code":200,"message":"Offer Details"}
     * data : [{"city":"Hyderabad","cuisine_type":"","offer_id":"34","restaurant_id":"33","restaurant_name":"justzoom","offer_image":"http://igranddeveloper.live/super-menu/admin_assets/uploads/offers/IMG_20191119_1249572.jpg","offer_details":"","offer_name":"","actual_price":"","discount_price":"","food_type":"","item_type":""}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Offer Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * city : Hyderabad
         * cuisine_type :
         * offer_id : 34
         * restaurant_id : 33
         * restaurant_name : justzoom
         * offer_image : http://igranddeveloper.live/super-menu/admin_assets/uploads/offers/IMG_20191119_1249572.jpg
         * offer_details :
         * offer_name :
         * actual_price :
         * discount_price :
         * food_type :
         * item_type :
         */

        private String city;
        private String cuisine_type;
        private String offer_id;
        private String restaurant_id;
        private String restaurant_name;
        private String offer_image;
        private String offer_details;
        private String offer_name;
        private String actual_price;
        private String discount_price;
        private String food_type;
        private String item_type;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCuisine_type() {
            return cuisine_type;
        }

        public void setCuisine_type(String cuisine_type) {
            this.cuisine_type = cuisine_type;
        }

        public String getOffer_id() {
            return offer_id;
        }

        public void setOffer_id(String offer_id) {
            this.offer_id = offer_id;
        }

        public String getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(String restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getRestaurant_name() {
            return restaurant_name;
        }

        public void setRestaurant_name(String restaurant_name) {
            this.restaurant_name = restaurant_name;
        }

        public String getOffer_image() {
            return offer_image;
        }

        public void setOffer_image(String offer_image) {
            this.offer_image = offer_image;
        }

        public String getOffer_details() {
            return offer_details;
        }

        public void setOffer_details(String offer_details) {
            this.offer_details = offer_details;
        }

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getActual_price() {
            return actual_price;
        }

        public void setActual_price(String actual_price) {
            this.actual_price = actual_price;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getFood_type() {
            return food_type;
        }

        public void setFood_type(String food_type) {
            this.food_type = food_type;
        }

        public String getItem_type() {
            return item_type;
        }

        public void setItem_type(String item_type) {
            this.item_type = item_type;
        }
    }
}
