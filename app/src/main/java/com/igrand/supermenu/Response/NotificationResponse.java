package com.igrand.supermenu.Response;

import java.util.List;

public class NotificationResponse {


    /**
     * status : {"code":200,"message":"Notification Details"}
     * data : [{"offer_name":"ln2a","description":"In publishing and graphic design,&nbsp;lorem ipsum&nbsp;is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying,'hai'\r\n"},{"offer_name":"zxc","description":"Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book\r\n"},{"offer_name":"vbn","description":"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters\r\n"},{"offer_name":"abc","description":"No Words"},{"offer_name":"qwert","description":"No Sentences"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Notification Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * offer_name : ln2a
         * description : In publishing and graphic design,&nbsp;lorem ipsum&nbsp;is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying,'hai'

         */

        private String offer_name;
        private String description;

        public String getOffer_name() {
            return offer_name;
        }

        public void setOffer_name(String offer_name) {
            this.offer_name = offer_name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
