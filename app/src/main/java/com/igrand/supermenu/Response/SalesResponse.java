package com.igrand.supermenu.Response;

import java.util.List;

public class SalesResponse {


    /**
     * status : {"code":200,"message":"Count"}
     * data : [{"day_count":0,"hour_count":0}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : Count
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * day_count : 0
         * hour_count : 0
         */

        private int day_count;
        private int hour_count;

        public int getDay_count() {
            return day_count;
        }

        public void setDay_count(int day_count) {
            this.day_count = day_count;
        }

        public int getHour_count() {
            return hour_count;
        }

        public void setHour_count(int hour_count) {
            this.hour_count = hour_count;
        }
    }
}
