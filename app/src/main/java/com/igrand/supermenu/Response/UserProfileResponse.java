package com.igrand.supermenu.Response;

import java.util.List;

public class UserProfileResponse {


    /**
     * status : {"code":200,"message":"User Details"}
     * data : [{"user_image":"http://igranddeveloper.live/super-menu/admin_assets/uploads/users/siva.jpg","first_name":"Siva","last_name":"Krishna","email":"sivakrishna@gmail.com","phone":"1212121212","orders_count":6}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : User Details
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean {
        /**
         * user_image : http://igranddeveloper.live/super-menu/admin_assets/uploads/users/siva.jpg
         * first_name : Siva
         * last_name : Krishna
         * email : sivakrishna@gmail.com
         * phone : 1212121212
         * orders_count : 6
         */

        private String user_image;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private int orders_count;

        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getOrders_count() {
            return orders_count;
        }

        public void setOrders_count(int orders_count) {
            this.orders_count = orders_count;
        }
    }
}
