package com.igrand.supermenu.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.PaymentsResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerPaymentAdapter extends RecyclerView.Adapter<RecyclerPaymentAdapter.Holder> {

    Context context;
    List<PaymentsResponse.DataBean> dataBean;
    public RecyclerPaymentAdapter(Context applicationContext, List<PaymentsResponse.DataBean> dataBean) {

        this.context=applicationContext;
        this.dataBean=dataBean;
    }

    @NonNull
    @Override
    public RecyclerPaymentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payments, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerPaymentAdapter.Holder holder, int position) {


        //holder.id.setText(dataBean.get(position).getPayments_id());
        //holder.totalsales.setText(dataBean.get(position).s());
        holder.payable.setText(dataBean.get(position).getPayable());






        holder.date.setText(dataBean.get(position).getDate());
        holder.month1.setText(dataBean.get(position).getYear());
        holder.month.setText(dataBean.get(position).getMonth_name());

        if(dataBean.get(position).getStatus().equals("1")) {

            holder.status.setText("PAID");
            holder.status.setTextColor(Color.parseColor("#FF6584"));

        } else if(dataBean.get(position).getStatus().equals("0")) {

            holder.status.setText("NOT-PAID");
            holder.status.setTextColor(Color.parseColor("#FF6584"));
        }


    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date,id,totalsales,payable,status,month,month1;
        public Holder(@NonNull View itemView) {
            super(itemView);
            date=itemView.findViewById(R.id.date);
            totalsales=itemView.findViewById(R.id.totalsales);
            payable=itemView.findViewById(R.id.payable);
            status=itemView.findViewById(R.id.status);
            month=itemView.findViewById(R.id.month);
            month1=itemView.findViewById(R.id.month1);

        }
    }
}
