package com.igrand.supermenu.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.NotificationResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerNotificationsAdapter extends RecyclerView.Adapter<RecyclerNotificationsAdapter.Holder> {

    Context context;
    List<NotificationResponse.DataBean> dataBean;
    public RecyclerNotificationsAdapter(Context applicationContext, List<NotificationResponse.DataBean> dataBean) {

        this.context=applicationContext;
        this.dataBean=dataBean;
    }

    @NonNull
    @Override
    public RecyclerNotificationsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerNotificationsAdapter.Holder holder, int position) {


        holder.offer_name.setText(dataBean.get(position).getOffer_name());
        holder.description.setText(dataBean.get(position).getDescription());



    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView offer_name,description;
        public Holder(@NonNull View itemView) {
            super(itemView);

            offer_name=itemView.findViewById(R.id.offer_name);
            description=itemView.findViewById(R.id.description);
        }


    }
}
