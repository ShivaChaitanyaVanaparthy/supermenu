package com.igrand.supermenu.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.supermenu.Activities.ViewDetails;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.OfferDetailRestuarantResponse;
import com.igrand.supermenu.Response.OrderDetailRestuarantResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerOfferDetailsAdapter extends RecyclerView.Adapter<RecyclerOfferDetailsAdapter.Holder> {

    Context context;
    List<OrderDetailRestuarantResponse.DataBean> dataBean;
    String month,time1;


    public RecyclerOfferDetailsAdapter(Context applicationContext, List<OrderDetailRestuarantResponse.DataBean> dataBean) {

        this.context=applicationContext;
        this.dataBean=dataBean;
    }

    @NonNull
    @Override
    public RecyclerOfferDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offerderails, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerOfferDetailsAdapter.Holder holder, int i) {

//        final Activity activity = (Activity) context;


        if(dataBean.get(i).getStatus().equals("0")) {

            holder.date.setText(dataBean.get(i).getDatetime());
             holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant());
            holder.status.setText("CLAIMED");
            //holder.status.setBackgroundColor(Color.parseColor("#0E0067"));
            holder.status.setBackgroundResource(R.drawable.textbackgroundpurple);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());


            String time = dataBean.get(i).getDatetime();
            String[] separated = time.split(" ");
            month = separated[0];
            time1 = separated[1];

        } else if(dataBean.get(i).getStatus().equals("1")){

            holder.date.setText(dataBean.get(i).getDatetime());
             holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant());
            holder.status.setText("SUCCESS");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textgreenbackground);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());


        } else if(dataBean.get(i).getStatus().equals("2")) {


            holder.date.setText(dataBean.get(i).getDatetime());
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant());
            holder.status.setText("EXPIRED");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textviewpink);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());
        }


    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView date,order_id,restuarent_name,status,discount_price,couponcode,code,location,offer_name;
        LinearLayout linear;
        public Holder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.date);
            order_id=itemView.findViewById(R.id.order_id);
            restuarent_name=itemView.findViewById(R.id.restuarent_name);
            status=itemView.findViewById(R.id.status);
            linear=itemView.findViewById(R.id.linear);
            location=itemView.findViewById(R.id.location);
            offer_name=itemView.findViewById(R.id.offer_name);
        }
    }
}
