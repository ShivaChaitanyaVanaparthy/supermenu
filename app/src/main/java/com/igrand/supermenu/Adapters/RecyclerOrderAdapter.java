package com.igrand.supermenu.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.Orderdetails;
import com.igrand.supermenu.Activities.RateIt;
import com.igrand.supermenu.Activities.ViewDetails;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.MyOrdersResponse;
import com.igrand.supermenu.Response.MyOrdersResponseRestuarent;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerOrderAdapter extends RecyclerView.Adapter<RecyclerOrderAdapter.Holder> {

    Context context;
    List<MyOrdersResponseRestuarent.DataBean> dataBean;
    String month,time1;
    public RecyclerOrderAdapter(Context context, List<MyOrdersResponseRestuarent.DataBean> dataBean) {
        this.context=context;
        this.dataBean=dataBean;

    }



    @NonNull
    @Override
    public RecyclerOrderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_orders, parent, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerOrderAdapter.Holder holder, final int i) {
        final Activity activity = (Activity) context;


        if(dataBean.get(i).getStatus().equals("0")) {

            holder.date.setText(dataBean.get(i).getTime());
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getUser_first_name());
            holder.status.setText("CLAIMED");
            //holder.status.setBackgroundColor(Color.parseColor("#0E0067"));
            holder.status.setBackgroundResource(R.drawable.textbackgroundpurple);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());

            holder.location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri u = Uri.parse("tel:" + dataBean.get(i).getUser_mobile());

                    // Create the intent and set the data for the
                    // intent as the phone number.
                    Intent i = new Intent(Intent.ACTION_DIAL, u);

                    try
                    {
                        // Launch the Phone app's dialer with a phone
                        // number to dial a call.
                        context.startActivity(i);
                    }
                    catch (SecurityException s)
                    {
                        // show() method display the toast with
                        // exception message.
                    }
                }
            });

            String time = dataBean.get(i).getTime();
            String[] separated = time.split(" ");
            month=separated[0];
            time1=separated[1];


            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ViewDetails.class);
                    intent.putExtra("Time",dataBean.get(i).getTime());
                    intent.putExtra("Order_Id",dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName",dataBean.get(i).getUser_first_name());
                    intent.putExtra("Status","CLAIMED");
                    intent.putExtra("Value",dataBean.get(i).getDiscount_price());
                    intent.putExtra("Location",dataBean.get(i).getUser_mobile());
                    intent.putExtra("Month",month);
                    intent.putExtra("Time1",dataBean.get(i).getTime());
                    intent.putExtra("OfferName",dataBean.get(i).getOffer_name());
                    intent.putExtra("Delivery_type",dataBean.get(i).getDelivery_type());
                    intent.putExtra("email",dataBean.get(i).getUser_mail());
                    intent.putExtra("count",dataBean.get(i).getItem_count());

                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });


        } else if(dataBean.get(i).getStatus().equals("1")) {


            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ViewDetails.class);
                    intent.putExtra("Time",dataBean.get(i).getTime());
                    intent.putExtra("Order_Id",dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName",dataBean.get(i).getUser_first_name());
                    intent.putExtra("Status0","SUCCESS");
                    intent.putExtra("Value",dataBean.get(i).getDiscount_price());
                    intent.putExtra("Location",dataBean.get(i).getUser_mobile());
                    intent.putExtra("Month",month);
                    intent.putExtra("Time1",dataBean.get(i).getTime());
                    intent.putExtra("OfferName",dataBean.get(i).getOffer_name());
                    intent.putExtra("Delivery_type",dataBean.get(i).getDelivery_type());
                    intent.putExtra("email",dataBean.get(i).getUser_mail());
                    intent.putExtra("count",dataBean.get(i).getItem_count());
                    intent.putExtra("Reviews",dataBean.get(i).getOrder_comment());
                    intent.putExtra("Ratings",dataBean.get(i).getOrder_ratings());


                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });



            holder.date.setText(dataBean.get(i).getTime());
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getUser_first_name());
            holder.status.setText("SUCCESS");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textgreenbackground);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());

            holder.location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri u = Uri.parse("tel:" + dataBean.get(i).getUser_mobile());

                    // Create the intent and set the data for the
                    // intent as the phone number.
                    Intent i = new Intent(Intent.ACTION_DIAL, u);

                    try
                    {
                        // Launch the Phone app's dialer with a phone
                        // number to dial a call.
                        context.startActivity(i);
                    }
                    catch (SecurityException s)
                    {
                        // show() method display the toast with
                        // exception message.
                    }
                }
            });



        } else if(dataBean.get(i).getStatus().equals("2")) {


            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ViewDetails.class);
                    intent.putExtra("Time",dataBean.get(i).getTime());
                    intent.putExtra("Order_Id",dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName",dataBean.get(i).getUser_first_name());
                    intent.putExtra("Status1","EXPIRED");
                    intent.putExtra("Value",dataBean.get(i).getDiscount_price());
                    intent.putExtra("Location",dataBean.get(i).getUser_mobile());
                    intent.putExtra("Month",month);
                    intent.putExtra("Time1",dataBean.get(i).getTime());
                    intent.putExtra("OfferName",dataBean.get(i).getOffer_name());
                    intent.putExtra("Delivery_type",dataBean.get(i).getDelivery_type());
                    intent.putExtra("email",dataBean.get(i).getUser_mail());
                    intent.putExtra("count",dataBean.get(i).getItem_count());

                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });


            holder.date.setText(dataBean.get(i).getTime());
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getUser_first_name());
            holder.status.setText("EXPIRED");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textviewpink);
            holder.location.setText(dataBean.get(i).getUser_mobile());
            holder.offer_name.setText(dataBean.get(i).getOffer_name());

            holder.location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri u = Uri.parse("tel:" + dataBean.get(i).getUser_mobile());

                    // Create the intent and set the data for the
                    // intent as the phone number.
                    Intent i = new Intent(Intent.ACTION_DIAL, u);

                    try
                    {
                        // Launch the Phone app's dialer with a phone
                        // number to dial a call.
                        context.startActivity(i);
                    }
                    catch (SecurityException s)
                    {
                        // show() method display the toast with
                        // exception message.
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView date,order_id,restuarent_name,status,discount_price,couponcode,code,location,offer_name;
        LinearLayout linear,viewdetails;
        public Holder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.date);
            order_id=itemView.findViewById(R.id.order_id);
            restuarent_name=itemView.findViewById(R.id.restuarent_name);
            status=itemView.findViewById(R.id.status);
            linear=itemView.findViewById(R.id.linear);
            viewdetails=itemView.findViewById(R.id.viewdetails);
            location=itemView.findViewById(R.id.location);
            offer_name=itemView.findViewById(R.id.offer_name);
        }
    }
}
