package com.igrand.supermenu.Adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.RestuarentDetailsResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerUserOffer1Adapter extends RecyclerView.Adapter<RecyclerUserOffer1Adapter.Holder> {
    String offer_details,offer_name,actual_price,discount_price;
    Integer size;
    List<RestuarentDetailsResponse.DataBean> dataBean;
    List<RestuarentDetailsResponse.DataBean.OfferDetailsBean> offer_details2;
    String cuisine;

    Context context;
    public RecyclerUserOffer1Adapter(Context applicationContext, String offer_details, String offer_name, Integer size, String actual_price, String discount_price, List<RestuarentDetailsResponse.DataBean> dataBean, List<RestuarentDetailsResponse.DataBean.OfferDetailsBean> offer_details2, String cuisine_type) {

        this.context=applicationContext;
        this.offer_details=offer_details;
        this.offer_name=offer_name;
        this.size=size;
        this.actual_price=actual_price;
        this.discount_price=discount_price;
        this.dataBean=dataBean;
        this.offer_details2=offer_details2;
        this.cuisine=cuisine_type;
    }

    @NonNull
    @Override
    public RecyclerUserOffer1Adapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offers1, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerUserOffer1Adapter.Holder holder, int i) {


        //String pic=offer_details2.get(i).getOffer_image();
       // Picasso.get().load(pic).into(holder.image);


        holder.offer_details.setText(offer_details2.get(i).getOffer_description());
        holder.offer_name.setText(offer_details2.get(i).getOffer_name());
        holder.discount_price1.setText(offer_details2.get(i).getDiscount_price());
        holder.actual_price1.setText(offer_details2.get(i).getActual_price());
        holder.actual_price1.setPaintFlags(holder.actual_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);



    }

    @Override
    public int getItemCount() {
        return offer_details2.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView offer_name,offer_details,discount_price1,actual_price1,offer_name1;
        ImageView image;
        public Holder(@NonNull View itemView) {
            super(itemView);
            offer_details=itemView.findViewById(R.id.offer_details);
            offer_name=itemView.findViewById(R.id.offer_name);
            discount_price1=itemView.findViewById(R.id.discount_price);
            actual_price1=itemView.findViewById(R.id.actual_price);
            image=itemView.findViewById(R.id.image);


        }

    }
}
