package com.igrand.supermenu.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import com.igrand.supermenu.Activities.Registration1;
import com.igrand.supermenu.Activities.RegistrationRestaurantt;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.CusineTypeResonse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapterCusineTypeList extends RecyclerView.Adapter<RecyclerAdapterCusineTypeList.ViewHolder>{

Context context;
    List<CusineTypeResonse.DataBean> dataBeans;
    private RadioButton lastCheckedRB = null;
    public int mSelectedItem = -1;
    TextView cityname;
    Dialog dialog;
    Button add;
    String selectedwork,selectedworkid;
    Registration1 addProjectEstimation1;
    RegistrationRestaurantt registrationRestaurantt1;
    ArrayList<String> list=new ArrayList<String>();
    boolean[] checkBoxState;




    public RecyclerAdapterCusineTypeList(Registration1 addProjectEstimation, List<CusineTypeResonse.DataBean> dataBeans, Registration1 addProjectEstimation1, TextView cuisinetype, Dialog dialog, Button add) {

        this.context=addProjectEstimation;
        this.dataBeans=dataBeans;
        this.cityname=cuisinetype;
        this.dialog= dialog;
        this.addProjectEstimation1=addProjectEstimation1;
        this.add=add;
    }

    public RecyclerAdapterCusineTypeList(RegistrationRestaurantt registrationRestaurantt, List<CusineTypeResonse.DataBean> dataBeans, RegistrationRestaurantt registrationRestaurantt1, TextView cuisinetype, Dialog dialog, Button add) {
        this.context=registrationRestaurantt;
        this.dataBeans=dataBeans;
        this.cityname=cuisinetype;
        this.dialog= dialog;
        this.registrationRestaurantt1=registrationRestaurantt1;
        this.add=add;
    }


    @NonNull
    @Override
    public RecyclerAdapterCusineTypeList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_card_checkbox, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerAdapterCusineTypeList.ViewHolder holder, final int position) {


        add=dialog.findViewById(R.id.add);

        holder.radio.setText(dataBeans.get(position).getCuisinetype());
        //selectedwork=dataBeans.get(position).getCuisinetype();



                holder.radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                        if(isChecked==true){
                            list.add(dataBeans.get(position).getCuisinetype());
                        } else {
                            list.remove(dataBeans.get(position).getCuisinetype());
                        }


                        if(addProjectEstimation1!=null){
                            add.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    addProjectEstimation1.getcusine(list);
                                    dialog.dismiss();

                                }
                            });
                        }
                        if(registrationRestaurantt1!=null){

                            add.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    registrationRestaurantt1.getcusine(list);
                                    dialog.dismiss();

                                }
                            });
                        }





              /*  if(holder.radio.isChecked()){

                    list.add(dataBeans.get(position).getCuisinetype());
                }

                    addProjectEstimation1.getcusine(list);


                dialog.dismiss();

*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox radio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            radio=itemView.findViewById(R.id.radio);

           /* View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    if(dataBeans!=null){
                        selectedwork=dataBeans.get(mSelectedItem).getCuisinetype();
                        selectedworkid=dataBeans.get(mSelectedItem).getId();

                       // addProjectEstimation1.getId0(selectedworkid);
                    }

                }
            };

            itemView.setOnClickListener(clickListener);
            radio.setOnClickListener(clickListener);*/

        }
    }
}
