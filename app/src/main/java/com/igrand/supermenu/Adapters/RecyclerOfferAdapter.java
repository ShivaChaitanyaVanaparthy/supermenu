package com.igrand.supermenu.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igrand.supermenu.Activities.OfferDetails;
import com.igrand.supermenu.Activities.OfferEdit;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.OffersRestuarentResponse;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerOfferAdapter extends RecyclerView.Adapter<RecyclerOfferAdapter.Holder> {

    Context context;
    List<OffersRestuarentResponse.DataBean> dataBean;
    public RecyclerOfferAdapter(Context context, List<OffersRestuarentResponse.DataBean> dataBean) {
        this.context=context;
        this.dataBean=dataBean;
    }


    @NonNull
    @Override
    public RecyclerOfferAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_offers, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerOfferAdapter.Holder holder, final int position) {

        final Activity activity = (Activity) context;

        holder.date.setText(dataBean.get(position).getDate());
        holder.offer_id.setText(dataBean.get(position).getOffer_id());
        holder.offer_name.setText(dataBean.get(position).getOffer_name());
        int ordercount=dataBean.get(position).getOrder_count();
        holder.totalorders.setText(String.valueOf(ordercount));
        holder.day.setText(dataBean.get(position).getValid_date());

        String date=dataBean.get(position).getValid_date();


        if(dataBean.get(position).getStatus().equals("1"))  {

            holder.status.setText("ACTIVE");

        } else if(dataBean.get(position).getStatus().equals("0")) {

            holder.status.setText("INACTIVE");
        }

        //holder.totalorders.setText(dataBean.get(position).getOrder_count());



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, OfferDetails.class);
                intent.putExtra("OFFERID",dataBean.get(position).getOffer_id());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, OfferEdit.class);
                intent.putExtra("OFFERID",dataBean.get(position).getOffer_id());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView edit;
        TextView date,offer_id,offer_name,status,totalorders,day;
        public Holder(@NonNull View itemView) {
            super(itemView);

            edit=itemView.findViewById(R.id.edit);

            date=itemView.findViewById(R.id.date);
            offer_id=itemView.findViewById(R.id.offer_id);
            offer_name=itemView.findViewById(R.id.offer_name);
            status=itemView.findViewById(R.id.status);
            totalorders=itemView.findViewById(R.id.totalorders);
            day=itemView.findViewById(R.id.day);

        }
    }
}
