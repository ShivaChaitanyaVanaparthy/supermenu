package com.igrand.supermenu.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.igrand.supermenu.Activities.RateIt;
import com.igrand.supermenu.Activities.ViewDetails;
import com.igrand.supermenu.Activities.ViewDetailsOrder;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.MyOrdersResponse;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerUserOrdersAdapter extends RecyclerView.Adapter<RecyclerUserOrdersAdapter.Holder> {

    List<MyOrdersResponse.DataBean> dataBean;
    Context context;
    String month,time1;


    public RecyclerUserOrdersAdapter(FragmentActivity activity, List<MyOrdersResponse.DataBean> dataBean) {

        this.context = activity;
        this.dataBean = dataBean;
    }


    @NonNull
    @Override
    public RecyclerUserOrdersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ordersuser, parent, false);
        return new Holder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerUserOrdersAdapter.Holder holder, final int i) {

        final Activity activity = (Activity) context;
        if (dataBean.get(i).getStatus().equals("0")) {
            holder.date.setText(dataBean.get(i).getTime());
            holder.viewdetails.setVisibility(View.GONE);
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant_name());
            holder.status.setText("CLAIMED");
            //holder.status.setBackgroundColor(Color.parseColor("#0E0067"));
            holder.status.setBackgroundResource(R.drawable.textbackgroundpurple);
            holder.discount_price.setText(dataBean.get(i).getDiscount_price());
            holder.couponcodelinear.setVisibility(View.VISIBLE);
            holder.couponcode.setText(dataBean.get(i).getCoupon_code());
            holder.itemcount.setText(dataBean.get(i).getItem_count());
            holder.offername1.setText(dataBean.get(i).getOffer_name());
            String time = dataBean.get(i).getTime();
            String[] separated = time.split(" ");
            month = separated[0];
            time1 = separated[1];
            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ViewDetailsOrder.class);
                    intent.putExtra("Time", dataBean.get(i).getTime());
                    intent.putExtra("Order_Id", dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName", dataBean.get(i).getRestaurant_name());
                    intent.putExtra("Status", "CLAIMED");
                    intent.putExtra("Value", dataBean.get(i).getDiscount_price());
                    //intent.putExtra("Location", dataBean.get(i).getUser_city());
                    intent.putExtra("Month", month);
                    intent.putExtra("Time1", time1);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });

        }
        else if (dataBean.get(i).getStatus().equals("1")) {
             holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ViewDetailsOrder.class);
                    intent.putExtra("Time", dataBean.get(i).getTime());
                    intent.putExtra("Order_Id", dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName", dataBean.get(i).getRestaurant_name());
                    intent.putExtra("Status0", "SUCCESS");
                    intent.putExtra("Value", dataBean.get(i).getDiscount_price());
                    //intent.putExtra("Location", dataBean.get(i).getUser_city());
                    intent.putExtra("OfferName", dataBean.get(i).getOffer_name());
                    intent.putExtra("Reviews", dataBean.get(i).getOrder_comment());
                    intent.putExtra("Ratings", dataBean.get(i).getOrder_ratings());
                    intent.putExtra("Month", month);
                    intent.putExtra("Time1", time1);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });


            holder.date.setText(dataBean.get(i).getTime());
            holder.viewdetails.setVisibility(View.VISIBLE);
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant_name());
            holder.status.setText("SUCCESS");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textgreenbackground);
            holder.discount_price.setText(dataBean.get(i).getDiscount_price());
            holder.couponcodelinear.setVisibility(View.GONE);
//            holder.location.setText(dataBean.get(i).getUser_city());
            holder.itemcount.setText(dataBean.get(i).getItem_count());
           // holder.item_count.setVisibility(View.VISIBLE);
            holder.offername1.setText(dataBean.get(i).getOffer_name());
            /*holder.couponcode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, RateIt.class);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });*/


        }
        else if (dataBean.get(i).getStatus().equals("2")) {

            holder.viewdetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ViewDetailsOrder.class);
                    intent.putExtra("Time", dataBean.get(i).getTime());
                    intent.putExtra("Order_Id", dataBean.get(i).getOrder_id());
                    intent.putExtra("RestuarentName", dataBean.get(i).getRestaurant_name());
                    intent.putExtra("Status1", "EXPIRED");
                    intent.putExtra("Value", dataBean.get(i).getDiscount_price());
                    //intent.putExtra("Location", dataBean.get(i).getUser_city());
                    intent.putExtra("Month", month);
                    intent.putExtra("Time1", time1);
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });

            holder.date.setText(dataBean.get(i).getTime());
            holder.order_id.setText(dataBean.get(i).getOrder_id());
            holder.viewdetails.setVisibility(View.GONE);
            holder.restuarent_name.setText(dataBean.get(i).getRestaurant_name());
            holder.status.setText("EXPIRED");
            // holder.status.setBackgroundColor(Color.parseColor("#16C100"));
            holder.status.setBackgroundResource(R.drawable.textviewpink);
            holder.discount_price.setText(dataBean.get(i).getDiscount_price());
            holder.couponcodelinear.setVisibility(View.GONE);
           // holder.location.setText(dataBean.get(i).getUser_city());
            holder.itemcount.setText(dataBean.get(i).getItem_count());
            holder.offername1.setText(dataBean.get(i).getOffer_name());
        }

    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView date, order_id, viewdetails,restuarent_name, status, discount_price, couponcode, code,offername,itemcount,offername1;
        LinearLayout linear,item_count,couponcodelinear;

        public Holder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            order_id = itemView.findViewById(R.id.order_id);
            restuarent_name = itemView.findViewById(R.id.restuarent_name);
            status = itemView.findViewById(R.id.status);
            discount_price = itemView.findViewById(R.id.discount_price);
            couponcode = itemView.findViewById(R.id.couponcode);
            linear = itemView.findViewById(R.id.linear);
            code = itemView.findViewById(R.id.code);
            viewdetails = itemView.findViewById(R.id.viewdetails);
            //location = itemView.findViewById(R.id.location);
            offername = itemView.findViewById(R.id.offername);
            itemcount = itemView.findViewById(R.id.itemcount);
            item_count = itemView.findViewById(R.id.item_count);
            offername1 = itemView.findViewById(R.id.offername1);
            couponcodelinear = itemView.findViewById(R.id.couponcodelinear);

        }
    }
}
