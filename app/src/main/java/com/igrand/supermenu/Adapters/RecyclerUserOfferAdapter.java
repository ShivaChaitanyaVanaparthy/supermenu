package com.igrand.supermenu.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.igrand.supermenu.Activities.OfferDetailsUser;
import com.igrand.supermenu.R;
import com.igrand.supermenu.Response.RatingResponse;
import com.igrand.supermenu.Response.UserHomeResponse;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerUserOfferAdapter extends RecyclerView.Adapter<RecyclerUserOfferAdapter.Holder> {
    Context context;
    List<UserHomeResponse.DataBean> dataBean;
    String offer_id, actual_price, discount_price;
    String RestaurantRating;



    public RecyclerUserOfferAdapter(FragmentActivity activity, List<UserHomeResponse.DataBean> dataBean) {

        this.context = activity;
        this.dataBean = dataBean;

    }


    @NonNull
    @Override
    public RecyclerUserOfferAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_useroffers, parent, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerUserOfferAdapter.Holder holder, final int i) {
        final Activity activity = (Activity) context;


        holder.actual_price.setText(dataBean.get(i).getActual_price());
        holder.restaurant_name.setText(dataBean.get(i).getRestaurant_name());
        Picasso.get().load(dataBean.get(i).getOffer_image()).placeholder(R.drawable.loading).error(R.drawable.logoo).into(holder.offer_image);
        holder.discount_price.setText(dataBean.get(i).getDiscount_price());
        holder.food_type.setText(dataBean.get(i).getFood_type());
        holder.name.setText(dataBean.get(i).getOffer_name());
        holder.address.setText(dataBean.get(i).getRestaurant_address());
        offer_id = dataBean.get(i).getOffer_id();
        actual_price = dataBean.get(i).getActual_price();
        discount_price = dataBean.get(i).getDiscount_price();


        holder.actual_price.setPaintFlags(holder.actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);



        if(dataBean.get(i).getRatings().equals("")){

        } else {
            double number = Double.valueOf(dataBean.get(i).getRatings()) ;
            double roundedNumber = DecimalUtils.round(number, 1);
            RestaurantRating=String.valueOf(roundedNumber);
            holder.rating.setText(RestaurantRating);

        }





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OfferDetailsUser.class);
                intent.putExtra("Offer_Id", dataBean.get(i).getOffer_id());
                intent.putExtra("Actual_price", actual_price);
                intent.putExtra("Discount_price", discount_price);
                intent.putExtra("CuisineType", dataBean.get(i).getFood_type());
                intent.putExtra("Rating", dataBean.get(i).getRatings());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

            }
        });

        holder.actual_price.setPaintFlags(holder.actual_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView actual_price, restaurant_name, discount_price, food_type, name,address,rating;
        ImageView offer_image;

        public Holder(@NonNull View itemView) {

            super(itemView);
            actual_price = itemView.findViewById(R.id.actual_price);
            restaurant_name = itemView.findViewById(R.id.restuarent_name);
            offer_image = itemView.findViewById(R.id.offer_image);
            discount_price = itemView.findViewById(R.id.discount_price);
            food_type = itemView.findViewById(R.id.food_type);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            rating = itemView.findViewById(R.id.rating);

        }


    }

    public static class DecimalUtils {

        public static double round(double value, int numberOfDigitsAfterDecimalPoint) {
            BigDecimal bigDecimal = new BigDecimal(value);
            bigDecimal = bigDecimal.setScale(numberOfDigitsAfterDecimalPoint,
                    BigDecimal.ROUND_HALF_UP);
            return bigDecimal.doubleValue();
        }
    }
}

