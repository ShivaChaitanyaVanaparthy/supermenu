package com.igrand.supermenu.Interface;

import com.igrand.supermenu.Response.AddOfferResponse;
import com.igrand.supermenu.Response.CopounResponse;
import com.igrand.supermenu.Response.CusineTypeResonse;
import com.igrand.supermenu.Response.EditOfferResponse;
import com.igrand.supermenu.Response.EditProfileRestuarentResponse;
import com.igrand.supermenu.Response.EditProfileUserResponse;
import com.igrand.supermenu.Response.EditProfileUserResponse1;
import com.igrand.supermenu.Response.ForgetPasswordResponse;
import com.igrand.supermenu.Response.ForgetPasswordUpdateResponse;
import com.igrand.supermenu.Response.LoginResponse1;
import com.igrand.supermenu.Response.MyOrdersResponse;
import com.igrand.supermenu.Response.MyOrdersResponseRestuarent;
import com.igrand.supermenu.Response.NotificationResponse;
import com.igrand.supermenu.Response.OfferClaimResponse;
import com.igrand.supermenu.Response.OfferDetailResponse;
import com.igrand.supermenu.Response.LoginResponse;
import com.igrand.supermenu.Response.OfferDetailRestuarantResponse;
import com.igrand.supermenu.Response.OffersRestuarentResponse;
import com.igrand.supermenu.Response.OrderDetailRestuarantResponse;
import com.igrand.supermenu.Response.OrderdetailsResponse;
import com.igrand.supermenu.Response.PaymentsResponse;
import com.igrand.supermenu.Response.RatingResponse;
import com.igrand.supermenu.Response.RegisterResponse;
import com.igrand.supermenu.Response.RegistrationRestResponse;
import com.igrand.supermenu.Response.RestuarentDetailsResponse;
import com.igrand.supermenu.Response.RestuarentProfileResponse;
import com.igrand.supermenu.Response.ReviewCancel;
import com.igrand.supermenu.Response.ReviewsResponse;
import com.igrand.supermenu.Response.SalesResponse;
import com.igrand.supermenu.Response.UpdateOfferResponse;
import com.igrand.supermenu.Response.UpdateProfileRestuarentResponse;
import com.igrand.supermenu.Response.UserHomeResponse;
import com.igrand.supermenu.Response.UserPasswordResponse;
import com.igrand.supermenu.Response.UserProfileResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("home")
    Call<UserHomeResponse>userhomeresponse(@Field("city_name") String city_name);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("ratings")
    Call<RatingResponse>reviews(@Field("city_name") String city_name);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("reviews")
    Call<ReviewsResponse>reviewresponse(@Field("order_id") String order_id,
                                        @Field("ratings") String ratings,
                                        @Field("comments") String comments);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("cancel-review")
    Call<ReviewCancel>reviewCancelresponse(@Field("order_id") String order_id,
                                     @Field("cancel_button") String cancel_button);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("register")
    Call<RegisterResponse>registerresponse(@Field("user_type") String user_type,@Field("first_name") String first_name,
                                            @Field("last_name") String last_name,@Field("phone") String phone,
                                            @Field("email") String email,@Field("password") String password);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("login")
    Call<LoginResponse>loginresponse(@Field("email") String email, @Field("password") String password);
                                   //  @Field("user_type") String user_type);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("login")
    Call<LoginResponse1>loginresponse1(@Field("email") String email, @Field("password") String password, @Field("user_type") String user_type);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("user-profile")
    Call<UserProfileResponse>userprofileresponse(@Field("user_id") String user_id);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("offer-details")
    Call<OfferDetailResponse>offetdetailresponse(@Field("offer_id") String offer_id);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-details")
    Call<RestuarentDetailsResponse>restuarentdetailresponse(@Field("restaurant_id") String restaurant_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("myorders")
    Call<MyOrdersResponse>myorderresponse(@Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("order-details")
    Call<OrderdetailsResponse>viewDetails(@Field("order_id") String order_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("offerclaim")
    Call<OfferClaimResponse>offerclaimresponse(@Field("offer_id") String offer_id,@Field("item_count") String item_count,@Field("delivery_type") String delivery_type,@Field("user_id") String user_id);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("user-changepassword")
    Call<UserPasswordResponse>userpasswordresponse(@Field("user_id") String user_id, @Field("old_password") String old_password, @Field("new_password") String new_password);



    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-changepassword")
    Call<UserPasswordResponse>restuarantpasswordresponse(@Field("restaurant_id") String restaurant_id, @Field("old_password") String old_password, @Field("new_password") String new_password);



    @Multipart
    @Headers("x-api-key:super-menu@123")
    @POST("user-profile-update")
    Call<EditProfileUserResponse>editprofileuserrrsponse(@Part("user_id") RequestBody user_id,
                                                         @Part("first_name") RequestBody first_name,
                                                         @Part("last_name") RequestBody last_name,
                                                         @Part("phone") RequestBody phone,
                                                         @Part("email") RequestBody email,
                                                         @Part MultipartBody.Part file);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-profile-edit")
    Call<EditProfileRestuarentResponse>editprofilerestuarentresponse(@Field("restaurant_id") String restaurant_id);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("notifications")
    Call<NotificationResponse>notificationsresponse(@Field("restaurant_id") String restaurant_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-orders")
    Call<MyOrdersResponseRestuarent>myorderresponserestuarent(@Field("restaurant_id") String restaurant_id);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("payments")
    Call<PaymentsResponse>paymentresponse(@Field("restaurant_id") String restaurant_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-profile")
    Call<RestuarentProfileResponse>restuarentprofileresponse(@Field("restaurant_id") String restaurant_id);


    @Multipart
    @Headers("x-api-key:super-menu@123")
    @POST("restaurant-profile-update")
    Call<UpdateProfileRestuarentResponse>updateprofilerestuarentresponse(@Part("restaurant_id") RequestBody restaurant_id,
                                                                         @Part("fname") RequestBody fname,
                                                                         @Part("lname") RequestBody lname,
                                                                         @Part("phone") RequestBody phone,
                                                                         @Part MultipartBody.Part file);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("user-profile-edit")
    Call<EditProfileUserResponse1>editprofileuserrrsponse1(@Field("user_id") String user_id);




    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("offers")
    Call<OffersRestuarentResponse>offerrestuarentresponse(@Field("restaurant_id") String restaurant_id);



    @Multipart
    @Headers("x-api-key:super-menu@123")
    @POST("offers-add")
    Call<AddOfferResponse>addofferresponse(@Part("restaurant_id") RequestBody restaurant_id,
                                           @Part("ftype") RequestBody ftype,
                                           @Part("offer_name") RequestBody  offer_name,
                                           @Part("valid") RequestBody valid,
                                           @Part("actualoffercost") RequestBody actualoffercost,
                                           @Part("offercost") RequestBody offercost,
                                           @Part("description") RequestBody description,
                                          // @Part("restaurantname") RequestBody restaurantname,
                                           @Part("menu_type") RequestBody menu_type,
                                          // @Part("offer_primary") RequestBody offer_primary,
                                           @Part MultipartBody.Part file);



    @Multipart
    @Headers("x-api-key:super-menu@123")
    @POST("offers-update")
    Call<UpdateOfferResponse>offersupdate(@Part("offer_id") RequestBody offer_id,
                                          @Part("offer_name") RequestBody offer_name,
                                          @Part("ftype") RequestBody  ftype,
                                          @Part("valid") RequestBody valid,
                                          @Part("actualoffercost") RequestBody actualoffercost,
                                          @Part("offercost") RequestBody offercost,
                                          @Part("description") RequestBody description,
                                          @Part("mtype") RequestBody mtype,
                                          @Part("status") RequestBody status,
                                          @Part MultipartBody.Part file);





    @Multipart
    @Headers("x-api-key:super-menu@123")
    @POST("register")
    Call<RegistrationRestResponse>registerrestresponse(@Part("user_type") RequestBody user_type,
                                                       @Part("fname") RequestBody fname,
                                                        @Part("lname") RequestBody lname,
                                                       @Part("phone") RequestBody phone,
                                                        @Part("email") RequestBody email,
                                                       @Part("password") RequestBody password,
                                                        @Part("restaurant_name") RequestBody restaurant_name,
                                                       //@Part("cuisine_type") RequestBody cuisine_type,
                                                        @Part List<MultipartBody.Part> cuisine_type,
                                                        @Part("city_name") RequestBody city_name,
                                                        @Part("website") RequestBody website,
                                                        @Part("googleloc") RequestBody googleloc,
                                                        @Part  MultipartBody.Part file);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("offer-details")
    Call<OfferDetailRestuarantResponse>restuarantofferresponse(@Field("offer_id") String offer_id);



    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("offers-edit")
    Call<EditOfferResponse>editofferresponse(@Field("offer_id") String offer_id);



    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("forgot-password")
    Call<ForgetPasswordResponse>forgetpasswordresponse(@Field("email") String email);



    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("forgot-password-update")
    Call<ForgetPasswordUpdateResponse>restuarantpasswordupdateresponse(@Field("email") String email, @Field("user_type") String user_type,@Field("password") String password,@Field("confirmpassword") String confirmpassword);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("couponverify")
    Call<CopounResponse>copoun(@Field("order_id") String order_id,
                               @Field("coupan_code") String coupan_code);


    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("one_offer_order")
    Call<OrderDetailRestuarantResponse>restuarantorderresponse(@Field("offer_id") String offer_id);

    @FormUrlEncoded
    @Headers("x-api-key:super-menu@123")
    @POST("sales")
    Call<SalesResponse>salesresponse(@Field("restaurant_id") String restaurant_id);



    @Headers("x-api-key:super-menu@123")
    @GET("cuisine-types")
    Call<CusineTypeResonse>admincusineList();







}
